# docker镜像瘦身记录

**导入OpenCloudOS官方提供的docker镜像**

```bash
wget https://mirrors.opencloudos.tech/opencloudos-stream/releases/2301/images/OpenCloudOS-Stream-2301-x86_64.tar

docker import opencloudos OpenCloudOS-Stream-2301-x86_64.tar
```

通过docker images查看当前docker镜像大小为**1.57GB**

![image-20230525191908260](README.assets/image-20230525191908260-16850135525231.png)

**启动容器，开始进行镜像瘦身**

```bash
docker run -itd opencloudos bash
docker exec -it xxx bash
# 拷贝dockerThin.py至容器内部 ...
python3 dockerThin.py
rm dockerThin.py
```

![image-20230525192151724](README.assets/image-20230525192151724-16850137128263.png)

**保存瘦身后的容器为本地镜像，同时再导入该镜像查看docker镜像大小**

```
docker export xxx > new_os.tar
docker import - new_cloudos < ./new_os.tar
```

![image-20230525192344740](README.assets/image-20230525192344740-16850138262815.png)

可以看到，经过dockerThin.py对容器内与dnf没有软件依赖关系的软件包进行卸载后，重新导出的镜像大小为393MB，docker导入后的大小为382MB，是瘦身之前的25%。

