import subprocess

def check_installed_packages(packages):
    # 执行 dnf list installed 命令，获取已安装软件包列表
    command = "dnf list installed | awk '{print $1}'"
    result = subprocess.run(command, shell=True, capture_output=True, text=True)
    installed_packages = result.stdout.splitlines()

    # 检查命令执行是否成功
    if result.returncode == 0:
        for line in installed_packages:
            if any(package in line for package in packages) == False:
                print(f"软件包 {line} 与dnf依赖的软件包无关")
                # 执行卸载命令
                uninstall_command = f"dnf remove -y {line}"
                uninstall_result = subprocess.run(uninstall_command, shell=True, capture_output=True, text=True)
                if uninstall_result.returncode == 0:
                    print(f"已卸载软件包 {line}")
                else:
                    print(f"卸载软件包 {line} 失败")
    else:
        # 打印错误信息
        print("命令执行失败")
        return []


# 要检查的软件包列表(dns依赖的软件包)
packages_to_check = ['dnf', 'mock', 'policycoreutils', 'yum']   #  the information is from 'dnf repoquery --whatrequires dnf'

# 调用函数进行检查
check_installed_packages(packages_to_check)
