# 部署Kerberos认证服务

## 1. 环境配置

| 主机名 | 内网IP         | 角色            |
| ------ | -------------- | --------------- |
| OC01   | 192.168.54.147 | Master KDC      |
| OC02   | 192.168.54.146 | Kerberos Client |

在两台主机的hosts文件中配置如下，确保主机名能够被解析。

```
192.168.54.144	OC01
192.168.54.142	OC02
```

## 2. 配置 Kerberos 服务端

### 2.1 安装kerberos

使用yum命令安装kerberos，命令如下：

```shell
yum install krb5-server krb5-libs krb5-client -y
```

安装完成如下图：

![image-20230525162139726](assets/image-20230525162139726.png)

### 2.2 配置krb5.conf文件

文件默认路径为：`/etc/krb5.conf`，该文件为kerberos的基本配置信息，需要所有使用kerberos的主机上的文件都同步。

文件内容配置如下：

```
[logging]
    default = FILE:/var/log/krb5libs.log
    kdc = FILE:/var/log/krb5kdc.log
    admin_server = FILE:/var/log/kadmind.log

[libdefaults]
    dns_lookup_realm = false
    ticket_lifetime = 24h
    renew_lifetime = 7d
    forwardable = true
    rdns = false
    pkinit_anchors = FILE:/etc/pki/tls/certs/ca-bundle.crt
    spake_preauth_groups = edwards25519
    dns_canonicalize_hostname = fallback
    qualify_shortname = ""
    default_realm = OPENCLOUDOS.COM

[realms]
OPENCLOUDOS.COM = {
    kdc = OC01
    admin_server = OC01
}

[domain_realm]
# .example.com = EXAMPLE.COM
# example.com = EXAMPLE.COM
```

**说明：**

- **[logging]：**表示server端的日志的打印位置

- **[libdefaults]：**每种连接的默认配置，其中各项说明如下：
  - default_realm：标识客户端的默认 Kerberos 域。将其值设置为您的 Kerberos 域，如果未设定，则在调用kinit之类的程序时，必须为每个Kerberos主体指定一个域。
  - dns_lookup_realm：用于指定Kerberos客户端在尝试进行身份验证时是否使用DNS来查找域名的领域。
  - ticket_lifetime：指定Kerberos票据的有效期，默认为1天（24小时）。
  - renew_lifetime：指定可以使用`kinit -R`命令续订票据的最长时间，默认值为7天。
  - forwardable：指定是否允许票据被代理转发。
  - rdns：指定Kerberos客户端在进行反向DNS查找时是否使用IP地址。
  - pkinit_anchors：指定PKINIT（公钥基础设施）证书颁发机构的根证书文件的路径。
  - spake_preauth_groups：指定SPAKE（安全密码身份验证）预认证所使用的密码组。
  - qualify_shortname：指定Kerberos客户端在解析短用户名时是否应该自动添加默认域名或领域名。
- **[realms]：**指定kerberos域的配置信息
  - kdc：指定该领域的KDC服务器的主机名或IP地址。
  - admin_server：指定该领域的管理员服务器的主机名或IP地址。管理员服务器通常用于管理Kerberos数据库。
- **[domain_realm]：**用于指定域名和Kerberos领域之间的映射关系。

### 2.3 配置kdc.conf文件

文件默认路径为：`/var/kerberos/krb5kdc/kdc.conf`

文件内容配置如下：

```
[kdcdefaults]
    kdc_ports = 88
    kdc_tcp_ports = 88
    spake_preauth_kdc_challenge = edwards25519

[realms]
OPENCLOUDOS.COM = {
     #master_key_type = aes256-cts
     acl_file = /var/kerberos/krb5kdc/kadm5.acl
     dict_file = /usr/share/dict/words
     default_principal_flags = +preauth
     admin_keytab = /var/kerberos/krb5kdc/kadm5.keytab
     supported_enctypes = aes256-cts:normal aes128-cts:normal arcfour-hmac:normal camellia256-cts:normal camellia128-cts:normal
}
```

**说明：**

- **[kdcdefaults]：**用于指定KDC服务器全局默认设置。
  - kdc_ports，kdc_tcp_ports：指定KDC服务器要监听的UDP和TCP端口号，默认值为88。
  - spake_preauth_kdc_challenge：指定KDC服务器在使用SPAKE预认证时使用的随机挑战字节长度。
- **[realms]：**用于指定KDC服务器上Kerberos域的配置信息。
  - master_key_type：指定主密钥的密钥类型。
  - acl_file：指定访问授权列表文件的路径。访问授权列表文件定义了谁可以进行Kerberos请求的身份验证以及他们可以访问的资源。默认情况下，KDC服务器使用`/var/kerberos/krb5kdc/kadm5.acl`文件来进行访问授权。
  - dict_file：指定密码策略字典文件的路径。密码策略字典文件包含了一些常用单词和短语，用于检查用户密码的强度。默认情况下，KDC服务器使用`/usr/share/dict/words`文件作为密码策略字典文件。
  - default_principal_flags：指定域中默认的Kerberos主体标志
  - admin_keytab：指定KDC管理员的Kerberos keytab的路径，用来管理KDC数据库。
  - supported_enctypes：指定KDC服务器支持的加密类型。

### 2.4 配置kadm5.acl文件

文件默认路径为：`/var/kerberos/krb5kdc/kadm5.acl`，该配置文件用于控制Kerberos数据库的管理权限。该文件定义了哪些用户或组可以访问Kerberos数据库和执行哪些操作。

文件内容配置如下：

```
*/admin@OPENCLOUDOS.COM *
```

其中`*/admin`是Kerberos中的账户形式，这个账户属于OPENCLOUDOS.COM这个域，最后的*表示符合`*/admin`的账户拥有所有权限。

### 2.5 创建/初始化kerberos数据库

要初始化Kerberos数据库，可采用下面的命令：

```
kdb5_util create -s -r OPENCLOUDOS.COM
```

![image-20230525164729067](assets/image-20230525164729067.png)

数据库创建成功后，会在目录`/var/kerberos/krb5kdc/`下生成新文件，如下所示：

![image-20230525170024025](assets/image-20230525170024025.png)

### 2.6 在数据库中创建新主体

在KDC终端中创建OPENCLOUDOS.COM域内的管理员主题，执行如下命令：

```
kadmin.local -q "addprinc root/admin"
```

![image-20230525165346760](assets/image-20230525165346760.png)

使用`kadmin.local`命令进入kerberos的admin命令行界面，通过命令行来创建一个测试管理员用户，如下图所示：

![image-20230525182049735](assets/image-20230525182049735.png)

使用`listprincs`命令来查看所有用户：

![image-20230525182147884](assets/image-20230525182147884.png)



### 2.7启动krb5kdc和kadmin服务

要启动krb5kdc和kadmin服务，可以使用以下命令：

```
systemctl start krb5kdc.service
systemctl start kadmin.service
```

要设置服务开机自启动，可以使用以下命令：

```
systemctl enable krb5kdc.service
systemctl enable kadmin.service
```

启动服务后，KDC已经在工作了，这两个守护进程会在后台运行，可以查看他们的日志文件（`/var/log/krb5kdc.log` 和`/var/log/kadmind.log`），也可以通过命令`kinit`来检查守护进程是否正常工作。

### 2.8 配置防火墙

采用以下命令进行防火墙的相关设置：

```
firewall-cmd --add-service=kerberos --permanent
firewall-cmd --add-service=kadmin --permanent
firewall-cmd --add-service=kpasswd --permanent
firewall-cmd --reload
```

到这里Kerberos的服务端就配置完成了。



## 3. 配置 kerberos 客户端

### 3.1 安装kerberos

使用yum命令安装kerberos客户端，命令如下：

```
yum install krb5-client krb5-libs -y
```

安装完成后如下图所示：

![image-20230525171155883](assets/image-20230525171155883.png)

### 3.2 配置krb5.conf文件

需要与KDC中krb5.conf文件保持一致，文件配置内容如下：

```
[logging]
    default = FILE:/var/log/krb5libs.log
    kdc = FILE:/var/log/krb5kdc.log
    admin_server = FILE:/var/log/kadmind.log

[libdefaults]
    dns_lookup_realm = false
    ticket_lifetime = 24h
    renew_lifetime = 7d
    forwardable = true
    rdns = false
    pkinit_anchors = FILE:/etc/pki/tls/certs/ca-bundle.crt
    spake_preauth_groups = edwards25519
    dns_canonicalize_hostname = fallback
    qualify_shortname = ""
    default_realm = OPENCLOUDOS.COM

[realms]
OPENCLOUDOS.COM = {
    kdc = OC01
    admin_server = OC01
}

[domain_realm]
# .example.com = EXAMPLE.COM
# example.com = EXAMPLE.COM
```

### 3.3 验证

**验证KDC颁发tickets**

首先运行`kinit`命令获取ticket，并将其存储在凭证缓存文件中：

```
kinit kebtest/admin
```

输入密码，命令执行成功如下：

![image-20230525184133972](assets/image-20230525184133972.png)

接下来，通过`klist`命令来查看当前缓存中的凭证列表：

![image-20230525184654083](assets/image-20230525184654083.png)

使用`kdestroy`命令来销毁缓存及其包含的凭证：

![image-20230525184756908](assets/image-20230525184756908.png)


