# Redis安装部署及使用

## 实验环境

虚拟机版本：VMWare Workstation Pro 17

操作系统 ：OpenCloudOS Stream

## 安装部署

##### 前期准备

1.[官网链接 ](https://redis.io/)

2.版本选择(当前yum下载默认版本为5.0.7)

```bash
2.x   #非常老
3.x   #较主流  redis-cluster
4.x   #混合持久化
5.x   #最新稳定版  新增加了流处理类型
```

3.yum下载默认的目录设置

```bash
/etc/redis/redis.conf           #默认conf文件路径
/var/lib/redis                  #rdb文件默认路径
```

##### 下载安装

```bash
yum install redis -y
```

<img src="redis_img/yum_install.png" title="" alt="yum下载安装" width="651">

安装成功，安装版本为7.0.5

![](redis_img/yum_install_secceed.png)

##### 测试

1.启动并连接redis

```bash
systemctl start redis   #启动服务器
redis-cli               #启动客户端
set k1 1
get K1
```

![](redis_img/connect_test.png)

2.检查进程和端口号

```bash
ps -ef|grep redis             #查看进程
netstat  -lntup|grep  6379    #查看端口
```

![](redis_img/port&ps.png)

3.关闭方法

方法1

```bash
redis-cli
SHUTDOWN           #进入客户端后执行shutdown
```

方法2

```bash
redis-cli shutdown  
```

![](redis_img/shutdown.png)

通过查看进程可以看出，两种方法都能够成功关闭。

## Redis使用

#### 简单命令的使用

1.查看所有的key  

```sql
keys *
```

2.set、get

```sql
set  k1  v1         #关键字k1的值设置为v1
get  k1             #查询k1的值
```

#### 持久化

###### 1.RDB持久化

当前内存里的数据状态持久化到硬盘，该种方式的有点是存储数据为压缩格式，恢复速度快；缺点是不是实时的，可能会存在丢失数据。

手动持久化

```bash
SAVE
BGSAVE
```

SAVE和BGSAVE均可以生成RDB文件，但是SAVE命令会组测Redis服务器进程，直到RDB文件创建完毕为止，在此期间，服务器不能处理任何请求；BGSAVE会创建一个子进程，由子进程来创建RDB文件。

a.执行BGSAVE

执行BGSAVE在/var/lib/redis目录下生成了rdb文件，此时重启redis，数据仍然存在。

![](redis_img/RDB1.png)

b.执行shundown、restart等命令时，也会自动生成RDB文件

```bash
cd /var/lib/redis
ls
systemctl restart redis
ls
rm dump.rdb
redis-cli
ls
redis cli shutdown
ls
```

![](redis_img/RDB2.png)

###### 2.AOF持久化方式

开启AOF持久化后每执行一条会更改Redis中的数据的命令，Redis就会将该命令写入硬盘中的AOF文件。AOF文件的保存位置和RDB文件的位置相同。

1.修改配置文件

redis中默认的持久化方式为RDB持久化方式，这里我们将持久化方式修改为AOF方式

```bash
cd /etc/redis
vim redis.conf 
```

RDB的配置文件将1379行no 修改为yes

![](redis_img/appendonly.png)

执行以下命令

```bash
rm dump.rdb
ls
systemctl start redis
redis-cli
> keys *
> set k1 2
> set k2 1
> keys *
ls
cd appendonlydir
```

![](redis_img/AOE1.png)

此时由于我们删除了rdb文件，所以第一次keys *结果为空，经过set操作后，再次查看目录下文件，发现生成了AOE的相关文件，通过这些文件，重启后也能够成功恢复数据。

![](redis_img/AOE2.png)

#### 主从复制

redis默认端口为6379，我们在6380端口再开启一个实例作为从数据库。

```bash
redis-server --port 6380 --slaveof 127.0.0.1 6379  &
ps -ef|grep redis-
redis-cli                          #连接到主数据库
> get K1> set master 6379
> keys *redis-cli -p 6380         #连接到从数据库
> keys *   
> set slave 6380
```

![](redis_img/copy.png)

可以看到从数据库成功同步主数据库的更新，并且只被允许读。
