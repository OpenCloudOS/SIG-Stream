## 部署sendmail并输出文档

## 安装

1. 安装 Sendmail。使用命令行终端进入下载目录，并使用适当的包管理工具（如 `yum`）进行安装。以下是安装 Sendmail 的示例命令：

   ```
   yum install sendmail
   ```

![image-20230527173235062](C:\Users\liyilin\Desktop\临时工作\pr\SIG-Stream\img\image-20230527173235062.png)

2. 安装m4，`m4` 是一个宏处理器工具，用于处理 `sendmail.mc` 配置文件并生成 `sendmail.cf` 配置文件。

   ![image-20230527173404780](C:\Users\liyilin\Desktop\临时工作\pr\SIG-Stream\img\image-20230527173404780.png)

## 配置

1. 配置 Sendmail。Sendmail 的配置文件位于 `/etc/mail/sendmail.mc`。可以编辑此文件来配置 Sendmail 的设置，例如域名、邮件转发、认证等。在编辑完成后，需要重新生成配置文件。执行以下命令生成配置文件：

   ```
   m4 /etc/mail/sendmail.mc > /etc/mail/sendmail.cf
   ```

执行 `m4 /etc/mail/sendmail.mc > /etc/mail/sendmail.cf` 命令时遇到错误消息 `m4: /etc/mail/sendmail.mc:10: cannot open '/usr/share/sendmail-cf/m4/cf.m4': No such file or directory`，这意味着在系统上缺少 `cf.m4` 文件。

原因是未安装 sendmail-cf 软件包，请执行以下命令安装：

```
yum install sendmail-cf
```

![image-20230527173526032](C:\Users\liyilin\Desktop\临时工作\pr\SIG-Stream\img\image-20230527173526032.png)

检查 `/usr/share/sendmail-cf/m4/cf.m4` 文件是否存在。执行以下命令来验证文件是否存在：

```
ls /usr/share/sendmail-cf/m4/cf.m4
```

![image-20230527173544107](C:\Users\liyilin\Desktop\临时工作\pr\SIG-Stream\img\image-20230527173544107.png)

2. 继续配置启动 Sendmail 服务。执行以下命令来启动 Sendmail 服务：

```
systemctl start sendmail
```

如果希望 Sendmail 服务在系统启动时自动启动，可以执行以下命令启用自动启动：

```
systemctl enable sendmail
```

## 测试

测试 Sendmail。使用以下命令来测试 Sendmail 是否正常工作：

```
echo "This is a test email." | mail -s "Test Email" recipient@example.com
```

若报错，则安装mail服务：

```
yum install mailx
```



![image-20230527173737439](C:\Users\liyilin\Desktop\临时工作\pr\SIG-Stream\img\image-20230527173737439.png)

执行

![image-20230527173840093](C:\Users\liyilin\Desktop\临时工作\pr\SIG-Stream\img\image-20230527173840093.png)

至此安装部署测试完成