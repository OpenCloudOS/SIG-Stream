在 OpenCloudOS Stream 2301 上部署 gdbm 并使用该库创建和管理数据库的步骤如下：

1.安装 gdbm

打开终端并使用以下命令安装 gdbm：

```
sudo dnf install gdbm-devel
```

2.创建数据库

在你的C程序中，首先需要包含头文件 `gdbm.h`，然后使用以下代码创建一个 GDBM 数据库

```c
GDBM_FILE db;
db = gdbm_open("example.db", 0, GDBM_WRCREAT, 0666, NULL);
if (!db) {
    printf("Failed to open database\n");
}
```

在上面的代码中，我们创建了一个名为 "example.db" 的数据库，并指定了权限为 0666。GDBM_WRCREAT 被设置为警告 gdbm 库如果数据库不存在则创建它。

3.存储数据

可以使用以下代码向数据库中存储数据：

```c
char *key1 = "mykey1";
char *value1 = "myvalue1";
datum db_key1, db_value1;
db_key1.dptr = key1;
db_key1.dsize = strlen(key1) + 1;
db_value1.dptr = value1;
db_value1.dsize = strlen(value1) + 1;

int ret = gdbm_store(db, db_key1, db_value1, GDBM_INSERT);
if (ret == 0) {
   	printf("Data stored successfully!\n");
} else {
   	printf("Failed to store data\n");
}
```

在上面的代码中，我们定义了一个 key 和 value 变量来存储数据，并将它们打包到 GDBM_DATUM 结构体中。然后使用 gdbm_store 将数据插入到 "example.db" 数据库中。

4.检索数据

以下代码演示了如何从数据库中检索数据：

```c
char *key = "mykey1";
datum db_key, db_value;
db_key.dptr = key;
db_key.dsize = strlen(key) + 1;

db_value = gdbm_fetch(db, db_key);
if (db_value.dptr) {
    printf("Retrieved value: %s\n", (char *) db_value.dptr);
} else {
    printf("Data not found\n");
}

```

在上面的代码中，我们首先指定要检索的键，然后使用 gdbm_fetch 从数据库中检索值。如果成功检索，则打印值；否则打印“Data not found”。

5.关闭数据库

在完成对数据库的操作后，请确保使用 gdbm_close 函数关闭数据库：

```c
gdbm_close(db);
```

6.编写测试程序

可以将上述代码放在一个 C 文件中，并编译成测试程序。例如，在名为 "test_gdbm.c" 的文件中编写以上代码

完整代码如下：

```c
#include <stdio.h>
#include <gdbm.h>

int main(int argc,char* argv[])
{
	//创建数据库 
	GDBM_FILE db;
	db = gdbm_open("example.db", 0, GDBM_WRCREAT, 0666, NULL);
	if (!db) {
    	printf("Failed to open database\n");
	}
	//存储数据 
	char *key1 = "mykey1";
	char *value1 = "myvalue1";
	datum db_key1, db_value1;
	db_key1.dptr = key1;
	db_key1.dsize = strlen(key1) + 1;
	db_value1.dptr = value1;
	db_value1.dsize = strlen(value1) + 1;

	int ret = gdbm_store(db, db_key1, db_value1, GDBM_INSERT);
	if (ret == 0) {
    	printf("Data stored successfully!\n");
	} else {
    	printf("Failed to store data\n");
	}
	//检索数据 
	char *key = "mykey1";
	datum db_key, db_value;
	db_key.dptr = key;
	db_key.dsize = strlen(key) + 1;

	db_value = gdbm_fetch(db, db_key);
	if (db_value.dptr) {
    	printf("Retrieved value: %s\n", (char *) db_value.dptr);
	} else {
    	printf("Data not found\n");
	}
	//关闭数据库 
	gdbm_close(db);
 
 	return 0;
}

```

并使用以下命令进行编译：

```
gcc -o test_gdbm test_gdbm.c -lgdbm
```

其中 -lgdbm 标志告诉编译器链接 gdbm 库。然后使用以下命令运行测试程序：

```
./test_gdbm
```

测试程序会打印出“Data stored successfully！”和“Retrieved value: myvalue1”信息来验证 gdbm 数据库的存储和检索功能。

以上是在 OpenCloudOS Stream 2301 上部署 gdbm 并创建和管理数据库的基本步骤。

