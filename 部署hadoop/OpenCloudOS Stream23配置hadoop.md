﻿# OpenCloudOS stream配置hadoop

一、虚拟机安装OpenCloudOS stream

镜像下载链接：[ISO - OpenCloudOS社区官网](https://www.opencloudos.org/iso)

根据需要自行选择，我这里选择的是DVD

安装过程参照[OC V9 下载及安装 - OpenCloudOS Documentation](https://docs.opencloudos.org/quickstart/V9_install/)【4.安装指导】

二、Hadoop安装流程

（一）前期准备

1、关闭防火墙，禁用SELINUX

![输入图片说明](Image/1.jpg)

输入`vi /etc/selinux/config`，将SELINUX的值改为disabled，保存并退出。

![输入图片说明](Image/2.jpg)

输入reboot命令重启系统，使操作生效。

2、配置host与ip之间对应关系

使用ifconfig查看ip地址

![输入图片说明](Image/3.jpg)

输入输入`vi /etc/hosts`，在最后一行加入192.168.158.128  localhost（此处我的主机名是localhost）

![输入图片说明](Image/4.jpg)

3、创建hadoop用户和用户组，为新用户设置密码（命令：passwd ）

输入`useradd –m hadoop`创建hadoop用户。

![输入图片说明](Image/5.jpg)

4、安装JDK

（1）安装Xshell，使用Xshell连接虚拟机

（2）使用命令`yum -y install lrzsz`，下载rz

![输入图片说明](Image/6.jpg)

（3）在hadoop目录下使用mkdir新建文件夹，命名为app

![输入图片说明](Image/7.jpg)

6、使用rz命令上传JDK安装包，将hadoop-2.6.0.tar.gz、jdk-7u79-linux-x64.tar.gz文件上传到app文件中

![输入图片说明](Image/8.jpg)

7、解压jdk文件：输入命令`tar -xvf jdk-7u79-linux-x64.tar.gz`

8、配置JDK环境变量：

（1）建立软链接：ln -s jdk1.7.0_79 jdk

![输入图片说明](Image/9.jpg)

（2）输入vi ~/.bashrc命令修改文件

![输入图片说明](Image/10.jpg)


```
	JAVA_HOME=/home/hadoop/app/jdk
	CLASSPATH=:$JAVA_HOME/lib/dt,jar:$JAVA_HOME/lib/tools.jar
	PATH=$PATH:$JAVA_HOME/bin
	export JAVA_HOME_CLASSPATH_PATH
```


（3）使用`source ~/.bashrc`命令使环境变量生效

（4）查看JDK版本，确保安装成功

![输入图片说明](Image/11.jpg)

（二）hadoop伪分布式集群安装配置

1、解压hadoop安装包：`tar -zxvf hadoop-2.6.0.tar.gz`
2、进入hadoop配置目录

![输入图片说明](Image/12.jpg)

3、使用命令`vi core-site.xml`修改core-site.xml配置文件

![输入图片说明](Image/13.jpg)

```
<property>
                <name>fs.defaultFS</name>
                <value>hdfs://localhost:9000</value>
<\property>
<property>
                <name>hadoop.tmp.dir</name>
                <value>file:/home/hadoop/data/tmp</value>
<\property>
<property>
                <name>hadoop.proxyuser,hadoop.hosts</name>
                <value>*</value>
<\property>
<property>
                <name>hadoop.proxyuser,hadoop.group</name>
                <value>*</value>
<\property>
```


4、修改hdfs-site.xml配置文件

![输入图片说明](Image/14.jpg)

```
<property>
	<name>dfs.namenode.data.dir</name>
	<value>/home/hadoop/data/dfs/name</value>
	<final>true</final>
</property>
<property>
	<name>dfs.datanode.data.dir</name>
	<value>/home/hadoop/data/dfs/data</value>
	<final>true</final>
</property>
<property>
	<name>dfs.replication</name>
	<value>1</value>
</property>
<property>
	<name>dfs.permissions</name>
	<value>false</value>
</property>
```


5、修改hadoop-env.sh配置文件

![输入图片说明](Image/15.jpg)

6、修改mapred-site.xml文件

![输入图片说明](Image/16.jpg)

```
<configuration>
                <property>
                        <name>mapreduce.framework.name</name>
                        <value>yarn</value>
                </property>
</configuration>
```

7、修改yarn-site.xml文件

![输入图片说明](Image/17.jpg)

```
        <property>
                <name>yarn.nodemanager.aux-services</name>
                <value>mapreduce_shuffle</value>
        </property>
```


8、修改slaves配置文件

![输入图片说明](Image/18.jpg)


9、创建hadoop 2.6.0软链接：`ln -s hadoop-2.6.0 hadoop`

![输入图片说明](Image/19.jpg)


10、配置hadoop环境变量并使之生效（部分环境变量之前已配置完成）

![输入图片说明](Image/20.jpg)

```
JAVA_HOME=/home/hadoop/app/jdk
HADOOP_HOME=/home/hadoop/app/hadoop
CLASSPATH=:$JAVA_HOME/lib/dt,jar:$JAVA_HOME/lib/tools.jar
PATH=$PATH:$JAVA_HOME/bin:HADOOP_HOME/bin:$PATH
export JAVA_HOME CLASSPATH_PATH PATH HADOOP_HOME
```

11、创建Hadoop相关数据目录

![输入图片说明](Image/21.jpg)

12、进入hadoop\bin目录下，使用`./hadoop namenode -format`命令格式化NameNode

![输入图片说明](Image/22.jpg)

13、使用命令行`sbin/start-all.sh`启动hadoop伪分布式集群，通过`jps`命令查看Hadoop启动进程，使用命令行`sbin/stop-all.sh`关闭hadoop伪分布式集群

![输入图片说明](Image/23.jpg)

14、至此，hadoop配置结束
