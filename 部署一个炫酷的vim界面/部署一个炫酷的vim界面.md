# 部署步骤

要在 CentOS 8 上安装和配置一个炫酷的 Vim 界面，可以按照以下步骤进行操作：

步骤 1: 安装 Vim 首先，确保您已在 CentOS 8 上安装了 Vim。如果尚未安装，请打开终端并运行以下命令：

```
sudo dnf install vim
```

![image-20230528214546243](assets\image-20230528214546243.png)

步骤 2: 安装插件管理器 为了使 Vim 更加强大和可定制化，我们将使用一个插件管理器。在终端中运行以下命令来安装 Vundle 插件管理器：

```
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

步骤 3: 创建 Vim 配置文件 现在，我们需要创建一个用于配置 Vim 的文件。在终端中运行以下命令：

```
vim ~/.vimrc
```

这将打开一个新的 Vim 编辑器窗口，并创建一个名为 `.vimrc` 的文件。

步骤 4: 配置 Vim 在 `.vimrc` 文件中，您可以添加各种配置选项和插件。以下是一些示例配置，您可以根据自己的喜好进行修改：

```
" 启用语法高亮
syntax enable

" 启用行号显示
set number

" 设置配色方案（可根据自己的喜好选择）
colorscheme desert

" 设置缩进为 4 个空格
set tabstop=4
set shiftwidth=4
set expandtab

" 启用鼠标支持
set mouse=a

" 启用 Vundle 插件管理器
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" 在此处添加您想要安装的插件，例如：
Plugin 'vim-airline/vim-airline'
Plugin 'scrooloose/nerdtree'

call vundle#end()
filetype plugin indent on

```

您可以根据自己的需求和喜好添加其他配置选项和插件。

![image-20230528214808570](assets\image-20230528214808570.png)

步骤 5: 安装插件 保存并关闭 `.vimrc` 文件后，在终端中运行以下命令来安装配置文件中指定的插件：

```
vim +PluginInstall +qall
```

![image-20230528214849422](assets\image-20230528214849422.png)

这将自动下载和安装所需的插件。

步骤 6: 完成 重新启动 Vim，您将看到新的炫酷界面和配置生效。根据您选择的配色方案和插件，界面可能会有所不同。

1. 保存并关闭当前打开的 Vim 编辑器窗口。可以按下键盘上的 Esc 键，然后输入 `:q` 命令，最后按下回车键。
2. 打开一个新的终端窗口或选项卡。
3. 输入 `vim` 命令，然后按下回车键。这将启动一个新的 Vim 编辑器会话。
4. 现在，您应该看到重新启动后的 Vim 界面和配置已经生效了。

请注意，重新启动 Vim 只会重新加载配置文件和插件，而不会影响您在当前会话中打开的文件。如果您对配置文件进行了更改，并希望这些更改立即生效，您可以重新启动 Vim。

步骤 7: 可选 - 添加自定义 Logo 如果您想在 Vim 启动时显示自定义 Logo，您可以按照以下步骤操作：

- 将您的自定义 Logo 图片（例如 `logo.png`）放置在一个您方便访问的位置。

- 在 `.vimrc` 文件中添加以下配置：

  ```
  " 设置启动时显示自定义 Logo
  let g:vim_startify_custom
  
  ```

# 效果展示

![image-20230528215041397](assets\image-20230528215041397.png)