# 部署LMDB服务并编写测试程序

要在Open Cloud OS上部署LMDB并创建数据库，并编写测试程序对存储和检索功能进行验证，可以按照以下步骤进行操作：

## 步骤1：安装LMDB库

### 1. yum直接安装

直接采用yum安装lmdb，命令如下：

```
sudo yum install lmdb
```

### 2. 采用源码编译安装

1. 安装GCC编译器和Git，运行以下命令安装它们：

```
sudo yum install gcc git -y
```

2. 从LMDB的GitHub存储库中获取源代码。您可以使用以下命令克隆存储库：

```
git clone https://github.com/LMDB/lmdb.git
```

3. 进入克隆的LMDB目录：

```
cd lmdb/libraries/liblmdb
```

4. 使用make命令编译LMDB库：

```
make
```

5. 安装LMDB库：

```
sudo make install
```

## 步骤2：编写测试程序

1. 创建测试使用数据库database，命令如下：

   ```shell
   mkdir ~/database
   ```

2. OpenCloudOS已经内置python3，但需要升级`lmdb`依赖包，命令如下：

   首先安装python3-pip：

   ```shell
   sudo yum install python3-pip -y
   ```

   升级`lmdb`依赖包：

   ```shell
   pip3 install --upgrade lmdb
   ```

3. 编写测试存储功能的Python代码示例，例如`lmdb_test_store.py`，内容如下：

   ```python
   import lmdb
   # 打开数据库环境
   env = lmdb.open('/home/opencloudos/database', map_size=10485760)
   
   # 打开一个事务
   with env.begin(write=True) as txn:
   
       # 在数据库中存储数据
       txn.put(b'key1', b'value1')
       txn.put(b'key2', b'value2')
       txn.put(b'key3', b'value3')
   
   # 关闭数据库环境
   env.close()
   print("存储了以下值：")
   print("key1: value1")
   print("key2: value2")
   print("key3: value3")
   ```

4. 编写测试检索功能的Python代码示例，例如`lmdb_test_retrieve.py`，内容如下：

   ```python
   import lmdb
   # 打开数据库环境
   env = lmdb.open('/home/opencloudos/database', readonly=True)
   
   # 打开一个只读事务
   with env.begin() as txn:
       # 打开数据库
       # 从数据库中检索数据
       value1 = txn.get(b'key1')
       value2 = txn.get(b'key2')
       value3 = txn.get(b'key3')
   
       # 打印检索到的值
       print("检索到以下值：")
       if value1:
           print("key1:", value1.decode())
       if value2:
           print("key2:", value2.decode())
       if value3:
           print("key3:", value3.decode())
   
   # 关闭数据库环境
   env.close()
   print("检索完成！")
   ```

## 步骤3：运行测试程序

1. 在终端中，导航到包含测试程序的目录。

2. 运行以下命令以运行测试存储功能程序：

   ```
   python ./lmdb_test_store.py
   ```

   执行结果如图：

   ![image-20230527235632734](assets/image-20230527235632734.png)

3. 运行以下命令以运行测试检索功能程序：

   ```
   python ./lmdb_test_retrieve.py
   ```

   执行结果如图：

   ![image-20230527235816838](assets/image-20230527235816838.png)

4. 检查输出，确保存储和检索功能正常工作。

这是一个基本的LMDB部署和测试示例。您可以根据自己的需求进行修改和扩展，还可以在LMDB文档中找到更多的操作和配置选项，以满足您的具体需求。