# 1. 将BIND配置为缓存DNS服务器

## 流程

- root权限下，安装```bind```和```bind-utils```软件包：

  ```shell
  # dnf install bind bind-utils
  ```

  中间选择y确认安装，最后出现```Complete!```即完成安装。

- 编辑```/etc/named.conf```文件，并在```options```语句中进行更改：

  - 使用nano打开此配置文件：

    ```shell
    # nano /etc/named.conf
    ```

  - 更新```listen-on```和```listen-on-v6```语句，以指定BIND应该侦听的IPv4和IPv6接口：

    ```shell
    listen-on port 53 { 127.0.0.1; 192.0.2.1; };
    listen-on-v6 port 53 { ::1; 2002:db8:1::1; };
    ```
  
    **注:配置时，应将其中的接口替换为您主机对应的接口。下同。**

  - 更新```allow-query```语句，以配置哪些IP地址和范围客户端可以查询此DNS服务器：

    ```shell
    allow-query { localhost; 192.0.2.0/24; 2001:db8:1::/64; };
    ```
  
  - 添加```allow-recursion```语句，以定义BIND接受递归查询的IP地址和范围：

    ```shell
    allow-recursion { localhost; 192.0.2.0/24; 2001:db8:1::/64; };
    ```
  
    **注意：不要在公共IP地址中递归。否则，服务器可能会成为大规模DNS扩大攻击的一部分。**

  - 默认情况下，BIND 通过将从根服务器递归查询到权威 DNS 服务器来解析查询。或者，您可以将 BIND 配置为将查询转发到其他 DNS 服务器，比如您的供应商之一。在这种情况下，添加一个带有 BIND 应该转发查询的 DNS 服务器的 IP 地址列表的```forwarders```语句：

    ```shell
    forwarders { 198.51.100.1; 203.0.113.5; };
    ```
  
    作为回退行为，如果转发服务器没有响应，BIND会以递归方式解析查询。要禁用此行为，请添加```forward only;```语句。

- 验证```/etc/named.conf```文件的语法：

  ```shell
  # named-checkconf
  ```

  如果命令没有显示输出，则语法是正确的。

- 更新```firewalld```规则，以允许传入的DNS流量：

  ```shell
  # firewall-cmd --permanent --add-service=dns
  # firewall-cmd --reload
  ```

- 编辑```/etc/sysconfig/named```文件：

  ```shell
  # nano /etc/sysconfig/named
  ```

  添加语句：

  ```shell
  DISABLE_ZONE_CHECKING="yes"
  ```

- 启动并启用BIND：

  ```shell
  # systemctl enable --now named
  ```

## 验证

+ 使用新设置DNS服务器解析域：

  ```shell
  # dig @localhost www.example.org
  ...
  www.example.org.	86400	IN	A	93.184.216.34
  
  ;; Query time: 445msec
  ...
  ```

  本例假定BIND在同意主机上运行并响应```localhost```接口上的查询。

  在第一次查询记录后，BIND会将条目添加到其缓存中。

+ 重复前面的查询：

  ```shell
  # dig @localhost www.example.org
  ...
  www.example.org.	86381	IN	A	93.184.216.34
  
  ;; Query time: 1msec
  ...
  ```

  由于对条目进行了缓存，进一步对相同记录的请求会非常快，直到条目过期为止。

# 2. 在BIND DNS服务器中配置日志记录

默认```/etc/named.conf```文件中的配置（如```bind```软件包提供）使用```default_debug```通道，并将消息记录到```/var/named/data/named.run```中。```default_debug```频道仅在服务器的debug级别为零时记录条目。

使用不同的频道和类别，可以将BIND配置为将具有定义的严重性的不同事件写入单独的文件。

## 前提条件

+ 已配置BIND，例如作为缓存名称服务器。
+ ```named```服务正在运行。

## 流程

+ 编辑 `/etc/named.conf` 文件，并将 `category` 和 `channel` 添加到 `logging` 语句中，例如：

  ```shell
  logging {
      ...
  
      category notify { zone_transfer_log; };
      category xfer-in { zone_transfer_log; };
      category xfer-out { zone_transfer_log; };
      channel zone_transfer_log {
          file "/var/named/log/transfer.log" versions 10 size 50m;
          print-time yes;
          print-category yes;
          print-severity yes;
          severity info;
       };
  
       ...
  };
  ```

  使用这个示例配置，BIND 会记录与区域传送相关的消息，到 `/var/named/log/transfer.log`。BIND 创建最多 `10` 个日志文件版本，如果它们达到 `50` MB 的最大大小，则轮转它们。

  `category` 定义了 BIND 向哪些频道发送类别信息。

  `channel` 定义了日志消息的目的地，包括版本数量、最大文件大小以及 BIND 应记录到频道的严重性等级。其他设置（如启用日志的时间戳、类别和严重性）是可选的，但可用于调试目的。

+ 如果不存在，创建日志目录，并为 `named` 用户授予对这个目录的写权限：

  ```shell
  # mkdir /var/named/log/
  # chown named:named /var/named/log/
  # chmod 700 /var/named/log/
  ```

+ 验证 `/etc/named.conf` 文件的语法：

  ```shell
  # named-checkconf
  ```

  如果命令没有显示输出，则语法为正确的。

+ 重启BIND：

  ```shell
  # systemctl restart named
  ```

## 验证

+ 显示日志文件内容：

  ```shell
  # cat /var/named/log/transfer.log
  ```

# 3. 编写BIND ACL

控制 BIND 的某些功能的访问可以防止未经授权的访问和攻击，如拒绝服务 (DoS)。BIND 访问控制列表 (`acl`) 语句是 IP 地址和范围的列表。每个 ACL 都有一个别名，可以在几个语句中使用，如 `allow-query` 来引用指定的 IP 地址和范围。

**```注意：BIND 仅在 ACL 中使用第一个匹配条目。例如，如果您定义了 ACL { 192.0.2/24; !192.0.2.1; } 以及带有 192.0.2.1 IP 地址的主机的连接，即使第二个条目排除这个地址，也会授予访问权限。```**

BIND有以下内置ACL：

+ `none`：不匹配主机。
+ `any`: 匹配所有主机。
+ `localhost` ：匹配回环地址 `127.0.0.1` 和 `::1`，以及服务器上运行 BIND 的服务器上的所有接口的 IP 地址。
+ `localnets` ：匹配回环地址 `127.0.0.1` 和 `::1`，以及运行 BIND 的服务器都直接连接到的所有子网。

## 前提条件

+ 已配置了 BIND，例如作为缓存名称服务器。
+ ```named```服务正在运行。

## 流程

+ 编辑 `/etc/named.conf` 文件并进行以下更改：

  + 将 `acl` 语句添加到文件中。例如，要为 `127.0.0.1`、`192.0.2.0/24`  创建名为 `internal-networks` 的 ACL，请输入：

    ```shell
    acl internal-networks { 127.0.0.1; 192.0.2.0/24; };
    ```

  + 在支持它们的声明中使用 ACL 的别名，例如：

    ```shell
    allow-query { internal-networks; };
    allow-recursion { internal-networks; };
    ```

+ 验证 `/etc/named.conf` 文件的语法：

  ```shell
  # named-checkconf
  ```

  如果命令没有显示输出，则语法为正确的。

+ 重新载入 BIND：

  ```shell
  # systemctl reload named
  ```

## 验证

+ 执行操作，以触发使用配置的 ACL 的功能。例如，此流程中的 ACL 只允许来自定义的 IP 地址的递归查询。在这种情况下，在不属于 ACL 定义的主机上输入以下命令来尝试解析外部域：

  ```shell
  # dig +short @192.0.2.1 www.example.com
  ```

  如果命令没有返回任何输出，BIND 拒绝访问，且 ACL 可以正常工作。有关客户端的详细输出，请使用不带 `+short` 选项的命令：

  ```shell
  # dig @192.0.2.1 www.example.com
  ...
  ;; WARNING: recursion requested but not available
  ...
  ```

# 4. 在BIND DNS服务器中配置区

DNS 区域是包含域空间中特定子树的资源记录的数据库。例如，如果您负责 `example.com` 域，可以在 BIND 中为它设置一个区。因此，客户端可将 `www.example.com` 解析为在这个区中配置的 IP 地址。

## 4.1. 区域文件中的 SOA 记录

SOA（start of authority）记录在一个 DNS 区中是必必需的记录。例如，如果多个 DNS 服务器对某个区域具有权威，那么此记录非常重要，但也指向 DNS 解析器。

BIND 中的 SOA 记录具有以下语法：

```shell
name class type mname rname serial refresh retry expire minimum
```

为提高可读性，管理员通常将区域文件中的记录分成多行，其中包含以分号(`;`)开头的注释。请注意，如果您分割 SOA 记录，圆括号将记录保留在一起：

```shell
@ IN SOA ns1.example.com. hostmaster.example.com. (
                          2022070601 ; serial number
                          1d         ; refresh period
                          3h         ; retry period
                          3d         ; expire time
                          3h )       ; minimum TTL
```

**注意： 请注意完全限定域名 (FQDN) 末尾的结尾点。FQDN 包含多个域标签，它们用点分开。由于 DNS root 有一个空标签，所以 FQDN 以点结尾。因此，BIND 在没有结尾点的情况下将区域名称附加到名称中。不含尾部点的主机名（例如 `ns1.example.com`）会被扩展为 `ns1.example.com.example.com.`，对于主域名服务器，这不是正确的地址。**

以下是 SOA 记录中的字段：

- `name` ：区域的名称，即所谓的 `源（origin）`。如果将此字段设置为 `@`，BIND 会将其扩展为 `/etc/named.conf` 中定义的区域名称。

- `class` ：在 SOA 记录中，必须将此字段始终设置为 Internet (`IN`)。

- `type` ：在 SOA 记录中，必须将此字段始终设置为 `SOA`。

- `mname` （主名称）：此区域的主域名服务器的主机名。

- `rname` (负责名称): 负责此区域的电子邮件地址。请注意，格式不同。您必须将 at 符号 (`@`) 替换为点 (`.`)。

- `serial` ：此区域文件的版本号。次要域名服务器仅在主服务器上的序列号较高时更新其区域副本。

  格式可以是任意数字值。通常的格式是 `<year><month><day><two-digit-number>`。使用这种格式，在理论上可以每天修改区域文件上百次。

- `refresh`：在检查主服务器更新时，次要服务器应等待的时间。

- `retry` ：当次要服务器在尝试失败后重试查询主服务器的时间长度。

- `expire` ：当所有之前的尝试失败时，次要服务器停止查询主服务器的时间长度。

- `minimum` ：RFC 2308 将此字段的含义改为负缓存时间。兼容解析器使用它来确定缓存 `NXDOMAIN` 名称错误的时间。

**注意：`refresh`, `retry`, `expire`, 和 `minimum` 项中的值定义了一个时间（以秒为单位）。但是，为了提高可读性，请使用时间后缀，如 `m` 表示分钟、`h` 表示小时，以及 `d` 表示天。例如，`3h` 代表 3 小时。 **

## 4.2. 在 BIND 主服务器上设置转发区

转发区域将名称映射到 IP 地址和其他信息。例如，如果您负责域 `example.com`，您可以在 BIND 中设置转发区来解析名称，如 `www.example.com`。

### 前提条件

- 已配置了 BIND，例如作为缓存名称服务器。
- `named` 服务正在运行。

### 流程

+ 在 `/etc/named.conf` 文件中添加区定义：

  ```shell
  zone "example.com" {
      type master;
      file "example.com.zone";
      allow-query { any; };
      allow-transfer { none; };
  };
  ```

  这些设置定义：

  - 此服务器作为 `example.com` 区域的主服务器 (`类型 master`)。
  - `/var/named/example.com.zone` 文件是区域文件。如果您设置了相对路径，如本例中所示，这个路径相对于您在 `options` 语句中的目录中创建的 `directory` 相对。
  - 任何主机都可以查询此区域。另外，还可指定 IP 范围或 BIND 访问控制列表 (ACL) 别名来限制访问。
  - 没有主机可以传输区域。仅在设置次要服务器并且仅为次要服务器的 IP 地址时才允许区域传送。

+ 验证 `/etc/named.conf` 文件的语法：

  ```shell
  # named-checkconf
  ```

  如果命令没有显示输出，则语法为正确的。

+ 使用以下内容创建 `/var/named/example.com.zone` 文件：

  ```shell
  $TTL 8h
  @ IN SOA ns1.example.com. hostmaster.example.com. (
                            2022070601 ; serial number
                            1d         ; refresh period
                            3h         ; retry period
                            3d         ; expire time
                            3h )       ; minimum TTL
  
                    IN NS   ns1.example.com.
                    IN MX   10 mail.example.com.
  
  www               IN A    192.0.2.30
  www               IN AAAA 2001:db8:1::30
  ns1               IN A    192.0.2.1
  ns1               IN AAAA 2001:db8:1::1
  mail              IN A    192.0.2.20
  mail              IN AAAA 2001:db8:1::20
  ```

  这个区域文件：

  - 将资源记录的默认生存时间 (TTL) 值设置为 8 小时。如果没有时间后缀（例如没有使用 `h` 指定小时），BIND 会将该值解析为秒。
  - 包含所需的 SOA 资源记录，以及有关该区域的详细信息。
  - 将 `ns1.example.com` 设置为此区域的权威 DNS 服务器。要正常工作，区域需要至少一个域名服务器 (`NS`) 记录。但是，若要与 RFC 1912 兼容，您需要至少有两个域名服务器。
  - 将 `mail.example.com` 设置为 `example.com` 域的邮件交换器 (`MX`)。主机名前面的数字值是记录的优先级。较低值的条目具有更高的优先级。
  - 设置 `www.example.com` 的 IPv4 和 IPv6 地址、`mail.example.com` 和 `ns1.example.com`。

+ 在区域文件上设置安全权限，仅允许 `named` 组读取它：

  ```shell
  # chown root:named /var/named/example.com.zone
  # chmod 640 /var/named/example.com.zone
  ```

+ 重新载入 BIND：

  ```shell
  # systemctl reload named
  ```

### 验证

+ 从 `example.com` 区域查询不同的记录，并验证输出是否与您在区域文件中配置的记录匹配：

  ```shell
  # dig +short @localhost AAAA www.example.com
  2001:db8:1::30
  
  # dig +short @localhost NS example.com
  ns1.example.com.
  
  # dig +short @localhost A ns1.example.com
  192.0.2.1
  ```

  本例假定 BIND 在同一主机上运行并响应 `localhost` 接口上的查询。

## 4.3. 在 BIND 主服务器中设置反向区

反向区域将 IP 地址映射到名称。例如，如果您负责 IP 范围 `192.0.2.0/24`，您可以在 BIND 中设置反向区域，以将 IP 地址从这个范围内的 IP 地址解析为主机名。

**注意： 如果您为整个类网络创建一个反向区域，请相应地命名区域。例如，对于 C network `192.0.2.0/24`，区域的名称是 `2.0.192.in-addr.arpa`。如果要为不同的网络大小创建反向区域，如 `192.0.2.0/28`，区的名称为 `28-2.0.192.in-addr.arpa`。**

### 前提条件

- 已配置了 BIND，例如作为缓存名称服务器。
- `named` 服务正在运行。

### 流程

+ 在 `/etc/named.conf` 文件中添加区定义：

  

  ```shell
  zone "2.0.192.in-addr.arpa" {
      type master;
      file "2.0.192.in-addr.arpa.zone";
      allow-query { any; };
      allow-transfer { none; };
  };
  ```

  这些设置定义：

  - 此服务器作为 `2.0.192.in-addr.arpa` 反向区域的主服务器(`type master`)。
  - `/var/named/2.0.192.in-addr.arpa.zone` 文件是区域文件。如果您设置了相对路径，如本例中所示，这个路径相对于您在 `options` 语句中的目录中创建的 `directory` 相对。
  - 任何主机都可以查询此区域。另外，还可指定 IP 范围或 BIND 访问控制列表 (ACL) 别名来限制访问。
  - 没有主机可以传输区域。仅在设置次要服务器并且仅为次要服务器的 IP 地址时才允许区域传送。

+ 验证 `/etc/named.conf` 文件的语法：

  ```shell
  # named-checkconf
  ```

  如果命令没有显示输出，则语法为正确的。

+ 使用以下内容创建 `/var/named/2.0.192.in-addr.arpa.zone` 文件：

  ```shell
  $TTL 8h
  @ IN SOA ns1.example.com. hostmaster.example.com. (
                            2022070601 ; serial number
                            1d         ; refresh period
                            3h         ; retry period
                            3d         ; expire time
                            3h )       ; minimum TTL
  
                    IN NS   ns1.example.com.
  
  1                 IN PTR  ns1.example.com.
  30                IN PTR  www.example.com.
  ```

  这个区域文件：

  - 将资源记录的默认生存时间 (TTL) 值设置为 8 小时。如果没有时间后缀（例如没有使用 `h` 指定小时），BIND 会将该值解析为秒。
  - 包含所需的 SOA 资源记录，以及有关该区域的详细信息。
  - 将 `ns1.example.com` 设置为此反向区域的权威 DNS 服务器。要正常工作，区域需要至少一个域名服务器 (`NS`) 记录。但是，若要与 RFC 1912 兼容，您需要至少有两个域名服务器。
  - 设置 `192.0.2.1` 和 `192.0.2.30` 地址的指针(`PTR`)记录。

+ 在区域文件上设置安全权限，仅允许 `named` 组读取它：

  ```shell
  # chown root:named /var/named/2.0.192.in-addr.arpa.zone
  # chmod 640 /var/named/2.0.192.in-addr.arpa.zone
  ```

+ 验证 `/var/named/2.0.192.in-addr.arpa.zone` 文件的语法：

  ```shell
  # named-checkzone 2.0.192.in-addr.arpa /var/named/2.0.192.in-addr.arpa.zone
  zone 2.0.192.in-addr.arpa/IN: loaded serial 2022070601
  OK
  ```

+ 重新载入 BIND：

  ```shell
  # systemctl reload named
  ```

### 验证

+ 从反向区查询不同的记录，并验证输出是否与您在区域文件中配置的记录匹配：

  ```shell
  # dig +short @localhost -x 192.0.2.1
  ns1.example.com.
  
  # dig +short @localhost -x 192.0.2.30
  www.example.com.
  ```

  本例假定 BIND 在同一主机上运行并响应 `localhost` 接口上的查询。

## 4.4. 更新BIND区文件

在某些情况下，例如，如果服务器的 IP 地址有变化，您必须更新区域文件。如果多个 DNS 服务器负责某个区，则仅在主服务器中执行这个步骤。存储区域副本的其他 DNS 服务器将通过区域传送接收更新。

### 前提条件

- zone 被配置。
- `named` 服务正在运行。

### 流程

+ 可选：识别 `/etc/named.conf` 文件中的区文件的路径：

  ```shell
  options {
      ...
      directory       "/var/named";
  }
  
  zone "example.com" {
      ...
      file "example.com.zone";
  };
  ```

  您可以在区域定义的 `file` 指令中找到到区域文件的路径。相对路径相对于 `options` 语句中的 `directory` 设置的相对路径。

+ 编辑区域文件：

  + 进行必要的更改。
  + 在 SOA 记录中递增序列号。

  **注意：如果序列号等于或低于先前值，次要服务器不会更新其区域的副本。 **

+ 验证区文件的语法：

  ```shell
  # named-checkzone example.com /var/named/example.com.zone
  zone example.com/IN: loaded serial 2022062802
  OK
  ```

+ 重新载入 BIND：

  ```shell
  # systemctl reload named
  ```

### 验证

+ 查询您添加、修改或删除的记录，例如：

  ```shell
  # dig +short @localhost A ns2.example.com
  192.0.2.2
  ```

  本例假定 BIND 在同一主机上运行并响应 `localhost` 接口上的查询。

## 4.5. 使用自动密钥生成和区维护功能进行 DNSSEC 区域签名

您可以使用域名系统安全扩展 (DNSSEC) 为区域签名，以确保身份验证和数据完整性。此类区域包含额外的资源记录。客户端可以使用它们来验证区域信息的真实性。

如果您为区启用 DNSSEC 策略功能，BIND 会自动执行以下操作：

- 创建密钥
- 为区域签名
- 维护区域，包括重新签名并定期替换密钥。

**注意：要启用外部 DNS 服务器以验证区的真实性，您必须在父区中添加该区域的公钥。请联系您的域供应商或 registry，以了解更多有关如何完成此操作的详细信息。 **

此流程使用 BIND 中的内置 `default` DNSSEC 策略。这个策略使用单一 `ECDSAP256SHA` 密钥签名。另外，还可创建自己的策略来使用自定义密钥、算法和计时。

### 前提条件

- 配置您要启用 DNSSEC 的区域。
- `named` 服务正在运行。
- 服务器可将时间与时间服务器同步。对于 DNSSEC 验证，系统时间准确非常重要。

### 流程

+ 首先，安装dnssec工具：

  ```shell
  # dnf install bind-dnssec-utils.x86_64
  ```

  若无对应的软件包，可先执行：

  ```shell
  # dnf search dnssec
  ```

  后，获得包列表后选择对应的进行安装操作。

+ 编辑 `/etc/named.conf` 文件，并将 `dnssec-policy default;` 添加到您要启用 DNSSEC 的区域：

  ```shell
  zone "example.com" {
      ...
      dnssec-policy default;
  };
  ```

+ 重新载入 BIND：

  ```shell
  # systemctl reload named
  ```

+ BIND 将公钥存储在 `/var/named/K<zone_name>.+<algorithm>+<key_ID>.key` 文件中。使用此文件显示区的公钥，格式为父区所需的格式：

  - DS 记录格式：

    ```shell
    # dnssec-dsfromkey /var/named/Kexample.com.+013+61141.key
    example.com. IN DS 61141 13 2 3E184188CF6D2521EDFDC3F07CFEE8D0195AACBD85E68BAE0620F638B4B1B027
    ```

  - DNSKEY 格式：

    ```shell
    # grep DNSKEY /var/named/Kexample.com.+013+61141.key
    example.com. 3600 IN DNSKEY 257 3 13 sjzT3jNEp120aSO4mPEHHSkReHUf7AABNnT8hNRTzD5cKMQSjDJin2I3 5CaKVcWO1pm+HltxUEt+X9dfp8OZkg==
    ```

+ 请求将区域的公钥添加到父区。请联系您的域供应商或 registry，以了解更多有关如何完成此操作的详细信息。

### 验证

+ 从启用了 DNSSEC 签名的区域查询您自己的 DNS 服务器：

  ```shell
  # dig +dnssec +short @localhost A www.example.com
  192.0.2.30
  A 13 3 28800 20220718081258 20220705120353 61141 example.com. e7Cfh6GuOBMAWsgsHSVTPh+JJSOI/Y6zctzIuqIU1JqEgOOAfL/Qz474 M0sgi54m1Kmnr2ANBKJN9uvOs5eXYw==
  ```

  本例假定 BIND 在同一主机上运行并响应 `localhost` 接口上的查询。

+ 在将公钥添加到父区并传播到其他服务器后，验证服务器是否将查询上的身份验证数据(`ad`)标记设置为已签名区域：

  ```shell
  #  dig @localhost example.com +dnssec
  ...
  ;; flags: qr rd ra ad; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1
  ...
  ```

# 5. 在BIND DNS服务器中配置区传输

区域传送可确保所有具有区域副本的 DNS 服务器均使用最新数据。

## 前提条件

- 在未来的主服务器中，已配置要设置区域传送的区域。
- 在未来的次要服务器上，已配置 BIND，例如作为缓存名称服务器。
- 在两个服务器上，`named`  服务正在运行。

## 流程

+ 在现有主服务器中：

  + 创建一个共享密钥，并将其附加到 `/etc/named.conf` 文件中：

    ```shell
    # tsig-keygen example-transfer-key | tee -a /etc/named.conf
    key "example-transfer-key" {
            algorithm hmac-sha256;
            secret "37NeyBjzgaTdzBnv+2I13zSo/LApxbV5tRPXOqECKy8=";
    };
    ```

    这个命令显示 `tsig-keygen` 命令的输出，并自动将其附加到 `/etc/named.conf` 中。

    稍后，在次要服务器上，您还需要命令的输出。

  + 编辑 `/etc/named.conf` 文件中的区定义：

    + 在 `allow-transfer` 语句中，定义服务器必须提供 `example-transfer-key` 语句中指定的密钥来传输区,另外，在 `allow-transfer` 语句中使用 IP地址或者BIND 访问控制列表 (ACL) 别名:

      ```shell
      zone "example.com" {
          ...
          allow-transfer { 192.0.2.2; key example-transfer-key; };
      };
      ```

    + 默认情况下，在更新区域后，BIND 会通知所有在区中有名称服务器 (`NS`) 记录的域名服务器。如果您不计划为二级服务器添加 `NS` 记录，您可以配置 BIND 通知这个服务器。为此，请将这个次要服务器的 IP 地址添加 `also-notify` 声明到区：

      ```shell
      zone "example.com" {
          ...
          also-notify { 192.0.2.2; };
      };
      ```

  + 验证 `/etc/named.conf` 文件的语法：

    ```shell
    # named-checkconf
    ```

  + 重新载入 BIND：

    ```shell
    # systemctl reload named
    ```

+ 在未来的次要服务器中：

  + 按如下方式编辑 `/etc/named.conf` 文件：

    + 添加与主服务器相同的密钥定义：

      ```shell
      key "example-transfer-key" {
              algorithm hmac-sha256;
              secret "37NeyBjzgaTdzBnv+2I13zSo/LApxbV5tRPXOqECKy8=";
      };
      ```

    + 在 `/etc/named.conf` 文件中添加区定义：

      ```shell
      zone "example.com" {
          type slave;
          file "slaves/example.com.zone";
          allow-query { any; };
          allow-transfer { none; };
          masters {
            192.0.2.1 key example-transfer-key;
          };
      };
      ```

      这些设置状态：

      - 此服务器是 `example.com` 区域的次要服务器 (`type slave`)。
      - `/var/named/slaves/example.com.zone` 文件是区域文件。如果您设置了相对路径，如本例中所示，这个路径相对于您在 `options` 语句中的目录中创建的 `directory` 相对。要隔离此服务器从属的区域文件，您可以将它们存储在 `/var/named/slaves/` 目录中。
      - 任何主机都可以查询此区域。另外，还可指定 IP 范围或 ACL 别名来限制访问。
      - 没有主机可以从该服务器传输区域。
      - 此区域的主服务器的 IP 地址是 `192.0.2.1` 。或者，您可以指定 ACL 别名。此次要服务器将使用名为 `example-transfer-key` 的键向主服务器进行身份验证。

  + 验证 `/etc/named.conf` 文件的语法：

    ```shell
    # named-checkconf
    ```

  + 重新载入 BIND：

    ```shell
    # systemctl reload named
    ```

## 验证

在次要服务器中：

+ 显示 `named` 服务的 `systemd` 日志条目：

```shell
# journalctl -u named
...
May 22 20:54:20 localhost.localdomain named[5172]: zone example.com/IN: Transfer started.
May 22 20:54:20 localhost.localdomain named[5172]: transfer of 'example.com/IN' from 192.0.2.1#53: connected using 192.0.2.1#53 TSIG example-transfer-key
May 22 20:54:20 localhost.localdomain named[5172]: zone example.com/IN: transferred serial 2022070603: TSIG 'example-transfer-key'
May 22 20:54:20 localhost.localdomain named[5172]: transfer of 'example.com/IN' from 192.0.2.1#53: Transfer status: success
May 22 20:54:20 localhost.localdomain named[5172]: transfer of 'example.com/IN' from 192.0.2.1#53: Transfer completed: 1 messages, 11 records, 400 bytes, 0.022 secs (18181 bytes/sec) (serial 2022070603)
```

+ 验证 BIND 创建了区域文件：

  ```shell
  # ls -l /var/named/slaves/
  total 4
  -rw-r--r--. 1 named named 566 May  22 20:54 example.com.zone
  ```

  请注意，默认情况下，次要服务器以二进制原始格式存储区域文件。

+ 从次要服务器查询传输的区的记录：

  ```shell
  # dig +short @192.0.2.2 AAAA www.example.com
  2001:db8:1::30
  ```

  本例假定您在此流程中设置的次要服务器侦听 IP 地址 `192.0.2.2`。

# 6. 在 BIND 中配置响应策略区以覆盖 DNS 记录

使用 DNS 块和过滤，管理员可以重写 DNS 响应来阻止对某些域或主机的访问。在 BIND 中，响应策略区域 (RPZ) 提供此功能。您可以为受阻条目配置不同的操作，如返回 `NXDOMAIN` 错误或不响应查询。

如果您的环境中有多个 DNS 服务器，请使用此流程在主服务器上配置 RPZ，稍后配置区传输以在您的次要服务器上提供 RPZ。

## 前提条件

- 已配置了 BIND，例如作为缓存名称服务器。
- `named` 服务正在运行。

## 流程

+ 编辑 `/etc/named.conf` 文件并进行以下更改：

  + 在 `options` 语句中添加 `response-policy` 定义：

    ```shell
    options {
        ...
    
        response-policy {
            zone "rpz.local";
        };
    
        ...
    }
    ```

    您可以在 `response-policy` 的 `zone` 语句中为 RPZ 设置自定义名称。但是，在下一步中，您必须在区定义中使用相同的名称。

  + 为您在上一步中设置的 RPZ 添加 `zone` 定义：

    ```shell
    zone "rpz.local" {
        type master;
        file "rpz.local";
        allow-query { localhost; 192.0.2.0/24; };
        allow-transfer { none; };
    };
    ```

    这些设置状态：

    - 此服务器是名为 `rpz.local` 的 RPZ 的主服务器 (`type master`)。
    - `/var/named/rpz.local` 文件是区域文件。如果您设置了相对路径，如本例中所示，这个路径相对于您在 `options` 语句中的目录中创建的 `directory` 相对。
    - `allow-query` 中定义的任何主机都可以查询此 RPZ。另外，还可指定 IP 范围或 BIND 访问控制列表 (ACL) 别名来限制访问。
    - 没有主机可以传输区域。仅在设置次要服务器并且仅为次要服务器的 IP 地址时才允许区域传送。

+ 验证 `/etc/named.conf` 文件的语法：

  ```shell
  # named-checkconf
  ```

+ 使用以下内容创建 `/var/named/rpz.local` 文件，例如：

  ```shell
  $TTL 10m
  @ IN SOA ns1.example.com. hostmaster.example.com. (
                            2022070601 ; serial number
                            1h         ; refresh period
                            1m         ; retry period
                            3d         ; expire time
                            1m )       ; minimum TTL
  
                   IN NS    ns1.example.com.
  
  example.org      IN CNAME .
  *.example.org    IN CNAME .
  example.net      IN CNAME rpz-drop.
  *.example.net    IN CNAME rpz-drop.
  ```

  这个区域文件：

  - 将资源记录的默认生存时间 (TTL) 值设置为 10 分钟。如果没有时间后缀（例如没有使用 `h` 指定小时），BIND 会将该值解析为秒。
  - 包含所需的 SOA 资源记录，以及有关该区域的详细信息。
  - 将 `ns1.example.com` 设置为此区域的权威 DNS 服务器。要正常工作，区域需要至少一个域名服务器 (`NS`) 记录。但是，若要与 RFC 1912 兼容，您需要至少有两个域名服务器。
  - 将查询的 `NXDOMAIN` 错误返回给该域中的 `example.org` 和主机。
  - 将查询丢弃至此域中的 `example.net` 和主机。

+ 验证 `/var/named/rpz.local` 文件的语法：

  ```shell
  # named-checkzone rpz.local /var/named/rpz.local
  zone rpz.local/IN: loaded serial 2022070601
  OK
  ```

+ 重新载入 BIND：

  ```shell
  # systemctl reload named
  ```

## 验证

+ 尝试解析 `example.org` 中的主机，该主机在 RPZ 中配置，以返回 `NXDOMAIN` 错误：

  ```shell
  # dig @localhost www.example.org
  ...
  ;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 26026
  ...
  ```

  本例假定 BIND 在同一主机上运行并响应 `localhost` 接口上的查询。

+ 尝试解析 `example.net` 域中的主机，该域在 RPZ 中配置以丢弃查询：

  ```shell
  # dig @localhost www.example.net
  ...
  ;; connection timed out; no servers could be reached
  ...
  ```

