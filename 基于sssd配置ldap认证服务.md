# 基于sssd配置ldap认证服务

## 目录

-   [部署流程](#部署流程)
    -   [1.Vmware 虚拟机安装OpenCloudOS stream](#1Vmware-虚拟机安装OpenCloudOS-stream)
    -   [2.安装SSSD系统安全服务守护进程和openldap服务器](#2安装SSSD系统安全服务守护进程和openldap服务器)
    -   [3.配置SSSD进程](#3配置SSSD进程)
        -   [前提](#前提)
        -   [流程](#流程)
        -   [3.1配置 SSSD 以发现 LDAP 域](#31配置-SSSD-以发现-LDAP-域)
        -   [3.2 为 SSSD 配置文件提供程序](#32-为-SSSD-配置文件提供程序)
        -   [3.3 为 SSSD 配置代理提供程序](#33-为-SSSD-配置代理提供程序)
    -   [4.配置sssd以使用LDAP并需要TLS验证](#4配置sssd以使用LDAP并需要TLS验证)
        -   [4.1 使用 SSSD 的 OpenLDAP 客户端以加密的方式从 LDAP 检索数据](#41-使用-SSSD-的-OpenLDAP-客户端以加密的方式从-LDAP-检索数据)
        -   [4.2 配置 SSSD 以使用 LDAP 并需要 TLS 身份验证](#42-配置-SSSD-以使用-LDAP-并需要-TLS-身份验证)
        -   [前提](#前提)
        -   [流程](#流程)
        -   [验证](#验证)

**目标**：

-   基于stream 2301版本部署sssd和openldap认证服务

## 部署流程

### 1.Vmware 虚拟机安装OpenCloudOS stream

下载 Stream 2301 版本镜像，连接：[https://www.opencloudos.org/iso](https://www.opencloudos.org/iso "https://www.opencloudos.org/iso")

安装过程请参考：

[OC V9 下载及安装 - OpenCloudOS Documentation](https://docs.opencloudos.org/quickstart/V9_install/ "OC V9 下载及安装 - OpenCloudOS Documentation")

### 2.安装SSSD系统安全服务守护进程和openldap服务器

使用dnf安装必要的软件包

```bash
dnf -y install openldap-clients sssd sssd-ldap oddjob-mkhomedir
```

SSSD 的默认配置文件为 `/etc/sssd/sssd.conf`。除了这个文件外，SSSD 还可以从 `/etc/sssd/conf.d/` 目录中的所有 `*.conf` 文件中读取其配置

### 3.配置SSSD进程

#### 前提

已安装SSSD

#### 流程

#### 3.1配置 SSSD 以发现 LDAP 域

```bash
nano /etc/sssd/sssd.conf
```

基本格式如下：

```bash
[domain/LDAP_domain_name]
id_provider = ldap
auth_provider = ldap

```

如果需要显式定义服务器，请使用ldap\_uri选项指定服务器的URI

在\_`ldap_search_base`\_选项中可以指定 LDAP 服务器的搜索库

```bash
[domain/LDAP_domain_name]
id_provider = ldap
auth_provider = ldap

ldap_uri = ldap://ldap.example.com
ldap_search_base = dc=example,dc=com

```

然后指定与 LDAP 服务器建立安全连接的方法，建议的方法是使用 TLS 连接。

请启用\_`ldap_id_use_start_tls`\_选项，并使用以下与 CA 证书相关的选项：

-   \_`ldap_tls_reqcert`\_指定客户端是否请求服务器证书以及对证书执行哪些检查
-   \_`ldap_tls_cacert`\_指定包含证书的文件

```bash
[domain/LDAP_default]
id_provider = ldap
auth_provider = ldap

ldap_uri = ldap://ldap.example.com
ldap_search_base = dc=example,dc=com

ldap_id_use_start_tls = true
ldap_tls_reqcert = demand
ldap_tls_cacert = /etc/pki/tls/certs/ca-bundle.crt
```

#### 3.2 为 SSSD 配置文件提供程序

将以下部分添加到文件`/etc/sssd/sssd.conf`中

```bash
[domain/files]
id_provider = files
```

（可选）将 **sss** 数据库设置为文件中用户和组查找的第一个源

```bash
passwd:     sss files
group:      sss files
```

按照系统引导时服务启动的方式配置系统

```bash
systemctl enable sssd
```

重启sssd服务

```bash
systemctl restart sssd
```

#### 3.3 为 SSSD 配置代理提供程序

```bash
[domain/proxy_name]
```

要指定身份验证提供程序，请执行以下操作：

1.  将\_`auth_provider`\_选项设置为 `proxy`
2.  使用\_`proxy_pam_target`\_选项将 PAM 服务指定为身份验证代理。

例如：

```bash
[domain/proxy_name]
auth_provider = proxy
proxy_pam_target = sssdpamproxy
```

要指定身份提供程序，请执行以下操作：

1.  将\_`id_provider`\_选项设置为 `proxy`
2.  使用 *`proxy_lib_name`* 选项将 NSS 库指定为身份代理。

例如：

```bash
[domain/proxy_name]
id_provider = proxy
proxy_lib_name = nis
```

将新的domain添加到 *`domains`* 选项中，

```bash
domains = LDAP_default, files , proxy_name
```

### 4.配置sssd以使用LDAP并需要TLS验证

#### 4.1 使用 SSSD 的 OpenLDAP 客户端以加密的方式从 LDAP 检索数据

LDAP 对象的验证方法可以是 Kerberos 密码，也可以是 LDAP 密码。

#### 4.2 配置 SSSD 以使用 LDAP 并需要 TLS 身份验证

#### 前提

-   OpenLDAP 服务器安装并配置了用户信息
-   要配置为 LDAP 客户端的主机上具有 root 权限
-   已创建并配置了 `/etc/sssd/sssd.conf` 文件，以将 `ldap` 指定为 `autofs_provider` 和 `id_provider`

有来自发布 OpenLDAP 服务器的证书颁发机构的 root CA 签名证书链的 PEM 格式副本，存储在名为 `core-dirsrv.ca.pem` 的本地文件中

#### 流程

将身份验证供应商切换到 `sssd`

```bash
authselect select sssd with-mkhomedir
```

将包含 root CA 签名证书链的 `core-dirsrv.ca.pem` 文件从颁发 OpenLDAP 服务器的 SSL/TLS 证书的证书颁发机构链复制到 `/etc/openldap/certs` 文件夹

```bash
cp core-dirsrv.ca.pem /etc/openldap/certs
```

然后编写lapd的配置文件

将 LDAP 服务器的 URL 和后缀添加到 `/etc/openldap/ldap.conf` 文件中

```bash
vim /etc/openldap/ldap.conf
```

```bash
URI ldap://ldap-server.example.com/
BASE dc=example,dc=com
```

在 `/etc/openldap/ldap.conf` 文件中，向 `/etc/openldap/certs/core-dirsrv.ca.pem` 添加指向 **TLS\_CACERT** 参数的行

```bash
TLS_CACERT /etc/openldap/certs/core-dirsrv.ca.pem
```

在 `/etc/sssd/sssd.conf` 文件中，将您的环境值添加到 `ldap_uri` 和 `ldap_search_base` 参数中，并将 `ldap_id_use_start_tls` 设置为 `True`

```bash
[domain/default]
id_provider = ldap
autofs_provider = ldap
auth_provider = ldap
chpass_provider = ldap
ldap_uri = ldap://ldap-server.example.com/
ldap_search_base = dc=example,dc=com
ldap_id_use_start_tls = True
cache_credentials = True
ldap_tls_cacertdir = /etc/openldap/certs
ldap_tls_reqcert = allow

[sssd]
services = nss, pam, autofs
domains = default

[nss]
homedir_substring = /home
…
```

在 `/etc/sssd/sssd.conf` 中，修改 `[domain]` 部分中的 `ldap_tls_cacert` 和 `ldap_tls_reqcert` 值来指定 TLS 身份验证要求：

```bash

cache_credentials = True
ldap_tls_cacert = /etc/openldap/certs/core-dirsrv.ca.pem
ldap_tls_reqcert = hard

```

更改 /etc/sssd/sssd.conf 文件的权限

```bash
chmod 600 /etc/sssd/sssd.conf
```

重启并启用 SSSD 服务和 `oddjobd` 守护进程

```bash
systemctl restart sssd oddjobd
systemctl enable sssd oddjobd
```

可选）如果您的 LDAP 服务器使用已弃用的 TLS 1.0 或 TLS 1.1 协议，请将客户端系统上系统范围的加密策略切换到 LEGACY 级别，以允许 RHEL 使用这些协议进行通信：

```bash
update-crypto-policies --set LEGACY
```

#### 验证

验证您可以使用 `id` 命令和指定 LDAP 用户从 LDAP 服务器检索用户数据

系统管理员现在可以使用 `id` 命令从 LDAP 查询用户。该命令返回一个正确的用户 ID 和组群成员。

```bash
id ldap_user
```

返回结果：

```bash
uid=17388(ldap_user) gid=45367(sysadmins) groups=45367(sysadmins),25395(engineers),10(wheel),1202200000(admins)
```
