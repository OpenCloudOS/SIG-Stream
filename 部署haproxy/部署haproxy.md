# 部署haproxy

## 安装haproxy

1. 下载

   本人采用的是1.7.8版本haproxy
   
   ![](.\image\01.png)
```bash
wget  https://src.fedoraproject.org/repo/pkgs/haproxy/haproxy-1.7.8.tar.gz/sha512/e1d65c8a4607c01d52628f36f8c7342096267130a0f949474746f571158e4f795281f78765004c214a0527f74ce180760f9cc910d3650d30026776076d721c0c/haproxy-1.7.8.tar.gz
```

2. 解压压缩包：
```bash
[root@localhost /]# tar -zxvf haproxy-1.7.8.tar.gz
```

3. 编译安装
```bash
[root@localhost /]# cd haproxy-1.7.8/
[root@localhost haproxy-1.7.8]# uname -r    #查看内核版本
3.10.0-1160.el7.x86_64
[root@localhost haproxy-1.7.8]# make TARGET=linux310 ARCH=x86_64          
[root@localhost haproxy-1.7.8]# make install PREFIX=/usr/local/haproxy  #编译安装,路径没有（没有会自己创建）
install -d "/usr/local/haproxy/sbin"
install haproxy  "/usr/local/haproxy/sbin"
install -d "/usr/local/haproxy/share/man"/man1
install -m 644 doc/haproxy.1 "/usr/local/haproxy/share/man"/man1
install -d "/usr/local/haproxy/doc/haproxy"
for x in configuration management architecture cookie-options lua WURFL-device-detection proxy-protocol linux-syn-cookies network-namespaces DeviceAtlas-device-detection 51Degrees-device-detection netscaler-client-ip-insertion-protocol close-options SPOE intro; do \
        install -m 644 doc/$x.txt "/usr/local/haproxy/doc/haproxy" ; \
done
```

## 配置HAProxy

注：1.79及以后的版本解压后文件内就没有haproxy.cfg文件，需要自行添加，添加到安装目录下
```bash
[root@localhost haproxy-1.7.8]# cd /usr/local/haproxy/
[root@localhost haproxy]# ls
doc  sbin  share
[root@localhost haproxy]# vim haproxy.cfg
```

编辑配置文件

HAProxy配置文件通常分为三个部分，即global、defaults和listen。global为全局配置，defaults为默认配置，listen为应用组件配置。

这里我们只需要更改HAPorxy主机的IP地址和Nginx的地址
```bash
global
  #日志
  log 127.0.0.1 local0 info
  #最大连接数
  maxconn 10240
  daemon
 
defaults
  #应用全局的日志配置
  log global
  mode http
  #超时配置
  timeout connect 5000
  timeout client 5000
  timeout server 5000
  timeout check 2000
 
listen http_front #haproxy的客户页面
  bind 192.168.2.212:8888         #HAProxy自己的IP地址
  mode http
  option httplog
  stats uri /haproxy
  stats auth admin:123456          #控制面板账号密码 账号：admin 
  stats refresh 5s
  stats enable
 
#如果没有部署反向代理，下面这一部分可以暂时不要
 
listen webcluster 
       bind 0.0.0.0:80     #这不用管，要打也只能打自己的IP
       option httpchk GET /index.html
       balance roundrobin  # 负载均衡模式轮询
       server inst1 192.168.2.195:80 check inter 2000 fall 3    #提供web服务的真实ip地址
       server inst2 192.168.2.206:80 check inter 2000 fall 3
```

## 启动

用安装包解压后目录里面的haproxy 启动安装目录的haproxy.cfg
```bash
/haproxy-1.7.8/haproxy -f /usr/local/haproxy/haproxy.cfg
```
验证是否成功
```bash
[root@localhost /]# lsof -i:8888
COMMAND  PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
haproxy 4092 root    3u  IPv4  48646      0t0  TCP localhost.localdomain:ddi-tcp-1 (LISTEN)
```

访问控制面板

[http://192.168.2.212:8888/haproxy](http://192.168.72.128:8888/haproxy)  配置文件里都有

![](.\image\02.png)