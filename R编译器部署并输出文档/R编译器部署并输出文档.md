# R编译器部署并输出文档

在OpenCloudOS部署R编译器并进行简单的Demo编译和运行，以下是安装、部署和配置指导文档：

## 安装部署R编译器

1. R编译器源码下载

   OC中好像没有R编译器的包（使用yum未找到），采用源码编译的形式进行安装。

   安装 wget 工具，并使用该工具下载源码，命令如下：

   ```
   sudo yum install wget
   wget https://cloud.r-project.org/src/base/R-4/R-4.3.0.tar.gz
   ```

   ![image-20230527211313975](assets/image-20230527211313975.png)

2. 安装编译时所需要的依赖项：

   ```
   sudo yum install gcc g++ java gfortran bzip2-devel zlib-devel xz-devel pcre2-devel perl readline-devel openssl-devel libcurl-devel libXt-devel -y
   ```

3. 解压缩R源码，编译安装R编译器，命令如下：

   ```
   tar zxvf R-4.3.0.tar.gz
   cd R-4.3.0/
   ./configure
   make
   sudo make install
   ```

4. 配置环境变量：

   在shell的配置文件`~/.bashrc`内添加如下内容：

   ```
   export PATH=/home/opencloudos/R-4.3.0/bin:$PATH
   ```

   其中 `/home/opencloudos/R-4.3.0/bin`是 R 可执行文件的路径。

   要使更改生效，可以执行以下命令使配置文件生效：

   ```
   source ~/.bashrc
   ```

5. 验证安装是否成功：

   - 打开终端并输入 `R` 进入R控制台，如下图所示：

     ![image-20230528104900564](assets/image-20230528104900564.png)

   - 输入`version`命令验证R是否成功安装，如下图：

     ![image-20230528104919778](assets/image-20230528104919778.png)

## 编译和运行简单的R程序

1. 创建一个简单的R程序：

   - 在任意文本编辑器中创建一个名为 `demo.R` 的文件。

   - 添加以下代码到 `demo.R` 文件中：

     ```
     x <- c(1, 2, 3, 4, 5)
     mean_x <- mean(x)
  print(mean_x)
     ```

2. 编译和运行R程序：

   - 打开终端，并进入包含 `demo.R` 文件的目录。

   - 输入以下命令运行R程序：

     ```
     Rscript demo.R
     ```

   - 您将在终端上看到输出结果，显示平均值。
   
     ![image-20230528105040622](assets/image-20230528105040622.png)

这样，您已成功安装、部署和配置R编译器，并编译、运行了一个简单的R程序。