# OpenCloudOS 2301 部署FTP服务

## 背景信息

FTP（File Transfer Protocol）是一种文件传输协议，基于客户端/服务器架构，支持以下两种工作模式：

- 主动模式：客户端向FTP服务器发送端口信息，由服务器主动连接该端口。
- 被动模式：FTP服务器开启并发送端口信息给客户端，由客户端连接该端口，服务器被动接受连接。

**说明** 大多数FTP客户端都在局域网中，没有独立的公网IP地址，且有防火墙阻拦，主动模式下FTP服务器成功连接到客户端比较困难。因此，如无特殊需求，建议您将FTP服务器配置为被动模式。

FTP支持以下三种认证模式：

- 匿名用户模式：任何人无需密码验证就可以直接登录到FTP服务器。这种模式最不安全，一般只用来保存不重要的公开文件，不推荐在生产环境中使用。
- 本地用户模式：通过Linux系统本地账号进行验证的模式，相较于匿名用户模式更安全。
- 虚拟用户模式：FTP服务器的专有用户。虚拟用户只能访问Linux系统为其提供的FTP服务，而不能访问Linux系统的其它资源，进一步增强了FTP服务器的安全性。

本文主要介绍被动模式下，使用本地用户访问FTP服务器的配置方法。



## 步骤一：安装vsftpd

**先决条件**： 要求已以root身份登录

1. 运行以下命令安装vsftpd。

   ```undefined
   # dnf install -y vsftpd
   ```

   出现如下图所示界面时，表示安装成功。

   ![image-20230526185004729](https://s2.loli.net/2023/05/26/xgNwmGP4cUDOAhd.png)

2. 运行以下命令设置FTP服务开机自启动。

   ```
   # systemctl enable vsftpd.service
   ```

3. 运行以下命令启动FTP服务。

   ```
   # systemctl start vsftpd.service
   ```

   **说明** 执行该命令时如果提示错误信息Job for vsftpd.service failed because the control process exited with error code，请排查是否存在下述问题。

   - 网络环境不支持IPv6时，运行命令**vim /etc/vsftpd/vsftpd.conf**将内容`listen_ipv6=YES`修改为`listen_ipv6=NO`。
   - MAC地址不匹配时，运行命令**ifconfig**查看MAC地址，并在/etc/sysconfig/network-scripts/ifcfg-xxx配置文件中新增或修改`HWADDR=<MAC地址>`。

4. 运行以下命令查看FTP服务监听的端口。

   ```undefined
   # netstat -antup | grep ftp
   ```

   出现如下图所示界面，表示FTP服务已启动，监听的端口号为21。
   ![image-20230526185110108](https://s2.loli.net/2023/05/26/scj2XheUqTdVvGI.png)

   此时，vsftpd默认已开启本地用户模式，您还需要继续进行配置才能正常使用FTP服务。

## 步骤二：配置vsftpd

为保证数据安全，本文主要介绍被动模式下，使用本地用户访问FTP服务器的配置方法。

1. 运行以下命令为FTP服务创建一个Linux用户。本示例中，该用户名为**ftptest**。

   ```undefined
   # useradd ftptest
   ```

2. 运行以下命令修改**ftptest**用户的密码。

   ```undefined
   # passwd ftptest
   ```

   运行命令后，根据命令行提示完成FTP用户的密码修改。

   **注意**：测试时发现，OpenCloudOS 2301会提示加载字典错误，可以通过手动关闭字典检查的方式解决

   修改 ` /etc/security/pwquality.conf` 文件，设置 `dictcheck = 0`保存即可。

   ![image-20230526190034565](https://s2.loli.net/2023/05/26/sk2WYrt61GUMNB4.png)

3. 运行以下命令创建一个供FTP服务使用的文件目录。

   ```bash
   mkdir /var/ftp/test
   ```

4. 运行以下命令，创建测试文件。

   该测试文件用于FTP客户端访问FTP服务器时使用。

   ```bash
   touch /var/ftp/test/testfile.txt
   ```

5. 运行以下命令更改/var/ftp/test目录的拥有者为**ftptest**。

   ```bash
   chown -R ftptest:ftptest /var/ftp/test
   ```

6. 修改vsftpd.conf配置文件。

   1. 运行以下命令，打开vsftpd的配置文件。

      如果您在安装vsftpd时，使用的是`apt install vsftpd`安装命令，则配置文件路径为/etc/vsftpd.conf。

      ```bash
      vi /etc/vsftpd/vsftpd.conf
      ```

   2. 按i进入编辑模式。

   3. 配置FTP服务器为被动模式。

      具体的配置参数说明如下：

      **重要** 修改和添加配置文件内的信息时，请注意格式问题。例如，添加多余的空格会造成无法重启服务的结果。

      ```bash
      #除下面提及的参数，其他参数保持默认值即可。
      
      #修改下列参数的值：
      #禁止匿名登录FTP服务器。
      anonymous_enable=NO
      #允许本地用户登录FTP服务器。
      local_enable=YES
      #监听IPv4 sockets。
      listen=YES
      
      #在行首添加#注释掉以下参数：
      #关闭监听IPv6 sockets。
      #listen_ipv6=YES
      
      #在配置文件的末尾添加下列参数：
      #设置本地用户登录后所在目录。
      local_root=/var/ftp/test
      #全部用户被限制在主目录。
      chroot_local_user=YES
      #启用例外用户名单。
      chroot_list_enable=YES
      #指定例外用户列表文件，列表中用户不被锁定在主目录。
      chroot_list_file=/etc/vsftpd/chroot_list
      #开启被动模式。
      pasv_enable=YES
      allow_writeable_chroot=YES
      #本教程中为Linux实例的静态IP。
      pasv_address=<FTP服务器公网IP地址>
      #设置被动模式下，建立数据传输可使用的端口范围的最小值。
      #建议您把端口范围设置在一段比较高的范围内，例如50000~50010，有助于提高访问FTP服务器的安全性。
      pasv_min_port=<port number>
      #设置被动模式下，建立数据传输可使用的端口范围的最大值。
      pasv_max_port=<port number>
      ```

      更多参数的详细信息，请参见[vsftp配置文件及参数说明](#canshu)。

   4. 按Esc退出编辑模式，然后输入:wq并回车以保存并关闭文件。

7. 创建chroot_list文件，并在文件中写入例外用户名单。

   1. 运行以下命令，创建chroot_list文件。

      ```bash
      vi /etc/vsftpd/chroot_list
      ```

   2. 按i进入编辑模式。

   3. 输入例外用户名单。此名单中的用户不会被锁定在主目录，可以访问其他目录。

   4. 按Esc退出编辑模式，然后输入:wq并回车以保存并关闭文件。

   **重要** 没有例外用户时，也必须创建chroot_list文件，内容可为空。

8. 运行以下命令重启vsftpd服务。

   ```undefined
   systemctl restart vsftpd.service
   ```

## 步骤三：开启防火墙指定端口

先查看防火墙是否开启

```
firewall-cmd --state
```

然后设置防火墙开放被动模式对应端口以及21端口，这里是添加自定义的12121-12131端口。

```
firewall-cmd --zone=public --add-port=12121-12131/tcp --permanent
```

```
firewall-cmd --zone=public --add-port=21/tcp --permanent
```

操作成功后重启防火墙

```
systemctl restart firewalld.service
```

如果不放心，可以通过下面命令查看开启的所有端口：

```
firewall-cmd --list-ports
```

## 步骤四：客户端测试

FTP客户端、Windows命令行工具或浏览器均可用来测试FTP服务器。本文以xftp软件作为FTP客户端，介绍FTP服务器的访问步骤。

1. 在本地主机，打开**xftp**。

2. 在地址栏中输入`ftp://<FTP服务器IP地址>:FTP端口`，本文中为Linux实例的静态IP地址。例如：`ftp://192.168.131.1:21`。

3. 在弹出的**登录身份**对话框中，输入已设置的FTP用户名和密码，然后单击**登录**。登录后，您可以查看到FTP服务器指定目录下的文件，例如：测试文件testfile.txt。

4. 可以通过ftp服务上传本地文件进行**备份**操作，或者下载服务器上的文件。

   ![image-20230526193619295](https://s2.loli.net/2023/05/26/HJ3CRLxrQjNBg9S.png)

## <a name="canshu">vsftp配置文件及参数说明</a>

/etc/vsftpd目录下文件说明如下：

- /etc/vsftpd/vsftpd.conf是vsftpd的核心配置文件。
- /etc/vsftpd/ftpusers是黑名单文件，此文件中的用户不允许访问FTP服务器。
- /etc/vsftpd/user_list是白名单文件，此文件中的用户允许访问FTP服务器。

配置文件vsftpd.conf参数说明如下：

- 用户登录控制参数说明如下表所示。

  | 参数                 | 说明                      |
  | :------------------- | :------------------------ |
  | anonymous_enable=YES | 接受匿名用户              |
  | no_anon_password=YES | 匿名用户login时不询问口令 |
  | anon_root=（none）   | 匿名用户主目录            |
  | local_enable=YES     | 接受本地用户              |
  | local_root=（none）  | 本地用户主目录            |

- 用户权限控制参数说明如下表所示。

  | 参数                       | 说明                        |
  | :------------------------- | :-------------------------- |
  | write_enable=YES           | 可以上传文件（全局控制）    |
  | local_umask=022            | 本地用户上传的文件权限      |
  | file_open_mode=0666        | 上传文件的权限配合umask使用 |
  | anon_upload_enable=NO      | 匿名用户可以上传文件        |
  | anon_mkdir_write_enable=NO | 匿名用户可以建目录          |
  | anon_other_write_enable=NO | 匿名用户修改删除            |
  | chown_username=lightwiter  | 匿名上传文件所属用户名      |