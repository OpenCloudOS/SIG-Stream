# 部署 Squid 缓存代理服务器

## 目录

-   [部署流程](#部署流程)
    -   [1.Vmware 虚拟机安装OpenCloudOS stream](#1Vmware-虚拟机安装OpenCloudOS-stream)
    -   [2.安装Squid缓存代理服务器](#2安装Squid缓存代理服务器)
    -   [3.配置Squid缓存代理服务器](#3配置Squid缓存代理服务器)
        -   [3.1将Squid设置为没有身份验证的缓存代理](#31将Squid设置为没有身份验证的缓存代理)
        -   [前提](#前提)
        -   [流程](#流程)
        -   [验证](#验证)
        -   [3.2 将Squid服务配置为在特定的IP地址中监听](#32-将Squid服务配置为在特定的IP地址中监听)
        -   [前提](#前提)
        -   [流程](#流程)
        -   [3.3 在Squid中配置域拒绝列表](#33-在Squid中配置域拒绝列表)
        -   [前提](#前提)
        -   [流程](#流程)
        -   [3.4 将squid设置为LDAP身份验证的缓存代理服务器](#34-将squid设置为LDAP身份验证的缓存代理服务器)
        -   [前提](#前提)
        -   [流程](#流程)
        -   [验证](#验证)
        -   [可能遇到的问题](#可能遇到的问题)
        -   [3.5 将Squid设置为带有Kerberos身份验证的缓存代理](#35-将Squid设置为带有Kerberos身份验证的缓存代理)
        -   [前提](#前提)
        -   [流程](#流程)
        -   [验证](#验证)

**目标**：

-   在 OpenCloudOS Stream 2301 版本上部署 Squid 缓存代理服务器，实现 HTTP、HTTPS 和 FTP 协议的代理等功能

## 部署流程

### 1.Vmware 虚拟机安装OpenCloudOS stream

下载 Stream 2301 版本镜像，连接：[https://www.opencloudos.org/iso](https://www.opencloudos.org/iso "https://www.opencloudos.org/iso")

安装过程请参考：

[OC V9 下载及安装 - OpenCloudOS Documentation](https://docs.opencloudos.org/quickstart/V9_install/ "OC V9 下载及安装 - OpenCloudOS Documentation")

### 2.安装Squid缓存代理服务器

使用yum可直接安装squid软件包：

```bash
yum install squid
```

### 3.配置Squid缓存代理服务器

#### 3.1将Squid设置为没有身份验证的缓存代理

在该配置下以IP范围来限制外来流量对代理的访问

#### 前提

-   `/etc/squid/squid.conf` 文件是由 `squid` 软件包提供的默认版本,如果在之前编辑了这个文件，请删除该文件并重新安装该软件包

#### 流程

1.编辑squid的配置文件

```bash
nano /etc/squid/squid.conf
```

2.编辑并调整localnet的访问控制列表（ACL）

默认的配置文件中已经包含一个ACL列表及其对应的`http_access allow localnet` 规则，

请注意在之中删除所有与自身环境不匹配的现有的 `acl localnet` 条目。

[![p9Hmpr9.png](https://s1.ax1x.com/2023/05/25/p9Hmpr9.png)](https://imgse.com/i/p9Hmpr9)

3.在acl列表之后定义使用HTTPS协议的端口,其基本格式为

```bash
acl SSL_ports port port_number
```

例如：

```bash
acl SSL_ports port 443
```

默认的配置文件中已经包含443端口：

[![p9HmSKJ.png](https://s1.ax1x.com/2023/05/25/p9HmSKJ.png)](https://imgse.com/i/p9HmSKJ)

4.更新`acl Safe_ports` 规则列表

该规则表示Squid可以建立连接的端口，

比如21（FTP）、80（HTTP）、443（HTTPS）

基本格式如上图所示：

```bash
acl Safe_ports port port_number

```

同时默认配置中包含 `http_access deny !Safe_ports` 规则，

该规则定义了拒绝访问`Safe_ports` ACL 中未定义的端口

[![p9Hm9bR.png](https://s1.ax1x.com/2023/05/25/p9Hm9bR.png)](https://imgse.com/i/p9Hm9bR)

5.更新 `cache_dir` 参数以配置缓存类型、缓存目录路径、以及缓存大小

```bash
cache_dir ufs /var/spool/squid 10000 16 256
```

其中`ufs` 是缓存类型，`/var/spool/squid/` 是缓存目录， `10000` MB是缓存大小

后面两个参数则分别表示Squid 在`/var/spool/squid/`目录中创建`16` 个一级子目录，

在每个一级目录中创建 `256`个子目录

> 补充：如果要指定自己的缓存目录，请在命令行中配置缓存目录的权限：

```bash
chown squid:squid path_to_cache_directory
```

6.在防火墙中打开3128端口以测试代理是否正常工作

命令行中输入：

```bash
firewall-cmd --permanent --add-port=3128/tcp
firewall-cmd --reload
```

7.启用并启动 `squid` 服务

```bash
systemctl enable --now squid

```

#### 验证

验证HTTPS代理功能：

在命令行中使用curl发出https请求：

```bash
curl -O -L "https://www.redhat.com/index.html" -x "proxy.example.com:3128"
```

[![p9Hexv4.png](https://s1.ax1x.com/2023/05/25/p9Hexv4.png)](https://imgse.com/i/p9Hexv4)

在当前目录下得到index.html，代理工作正常

同理，代理FTP协议也是类似流程

#### 3.2 将Squid服务配置为在特定的IP地址中监听

#### 前提

-   `squid` 软件包已安装

#### 流程

1.编辑 `/etc/squid/squid.conf` 文件:

-   如果只配置监听端口，在 `http_port` 参数中设置端口号

```bash
http_port 8080
```

-   如果配置服务器监听的IP地址，在 `http_port` 参数中设置 IP 地址和端口号(支持多个地址)

```bash
http_port 192.0.2.1:3128
http_port 192.0.2.1:8080

```

2.防火墙打开相应的端口：

```bash
firewall-cmd --permanent --add-port=8080/tcp
firewall-cmd --reload
```

3.重启squid服务

```bash
systemctl restart squid
```

#### 3.3 在Squid中配置域拒绝列表

如果管理员想要阻止对于某些特定域的访问，则可以配置域拒绝列表

#### 前提

-   squid 已经被配置，用户也可以使用代理

#### 流程

1.编辑 `/etc/squid/squid.conf` 文件，添加以下设置：

```bash
acl domain_deny_list dstdomain "/etc/squid/domain_deny_list.txt"
http_access deny all domain_deny_list
```

2.创建`/etc/squid/domain_deny_list.txt` 文件，在文件中添加要阻止的域

例如，如果要阻止对子域`example.com` 和`example.net` 的访问：

```bash
.example.com
example.net
```

3.重启squid服务：

```bash
systemctl restart squid
```

#### 3.4 将squid设置为LDAP身份验证的缓存代理服务器

该配置下仅有经过身份验证的用户可以使用代理

#### 前提

-   &#x20;`/etc/squid/squid.conf` 文件是由 `squid` 软件包提供的默认版本,如果在之前编辑了这个文件，请删除该文件并重新安装该软件包
-   已经配置好了LDAP的服务器，LDAP 目录中存在一个服务用户：`uid=proxy_user,cn=users,cn=accounts,dc=example,dc=com`

Squid 只使用此帐户搜索验证用户。如果存在身份验证用户，Squid 会以此用户的身份绑定到该目录以验证身份验证。

#### 流程

1.编辑squid的配置文件

```bash
nano /etc/squid/squid.conf
```

2.在 `/etc/squid/squid.conf` 顶部添加以下配置条目

```bash
auth_param basic program /usr/lib64/squid/basic_ldap_auth -b "cn=users,cn=accounts,dc=example,dc=com" -D "uid=proxy_user,cn=users,cn=accounts,dc=example,dc=com" -W /etc/squid/ldap_password -f "(&(objectClass=person)(uid=%s))" -ZZ -H ldap://ldap_server.example.com:389
```

每个选项的解释如下：

**auth\_param basic program**:&#x20;

指定认证程序的路径和名称，这里使用的是 /usr/lib64/squid/basic\_ldap\_auth。

**-b "cn=users,cn=accounts,dc=example,dc=com"**:

指定LDAP搜索的基础DN（Distinguished Name），用于指定搜索用户的起始位置。

**-D "uid=proxy\_user,cn=users,cn=accounts,dc=example,dc=com"**:&#x20;

指定用于绑定LDAP服务器的用户DN（Distinguished Name）。

**-W /etc/squid/ldap\_password:**&#x20;

指定LDAP绑定用户的密码文件路径。

**-f "(&(objectClass=person)(uid=%s))"**:&#x20;

指定LDAP搜索过滤器，用于根据用户提供的用户名进行搜索。

**-ZZ**:&#x20;

启用LDAP的安全传输层（TLS）加密。

**-H ldap\://ldap\_server.example.com:389**:&#x20;

指定LDAP服务器的主机名（或IP地址）和端口号。

3.添加以下 ACL 和规则来配置 Squid 只允许经过身份验证的用户使用代理：

```bash
acl ldap-auth proxy_auth REQUIRED
http_access allow ldap-auth
```

4.删除以下规则，以禁用从 `localnet` ACL 中指定的 IP 范围绕过代理身份验证

```bash
acl ldap-auth proxy_auth REQUIRED
http_access allow ldap-auth
```

> 请在 `http_access deny` 所有规则之前指定这些设置。

剩余的conf文件的配置和3.1节类似

5.将 LDAP 服务用户的密码存储在 `/etc/squid/ldap_password` 文件中，并为该文件设置适当的权限

```bash
echo "password" > /etc/squid/ldap_password
chown root:squid /etc/squid/ldap_password
chmod 640 /etc/squid/ldap_password
```

#### 验证

随后启动`squid` 服务并验证：

```bash
systemctl enable --now squid
```

```bash
curl -O -L "https://www.redhat.com/index.html" -x "user_name:password@proxy.example.com:3128"
```

可以缓存相应的index页面

#### **可能遇到的问题**

TLS连接建立失败，原因是 helper 助手工具没有正常工作，

解决办法：

使用 `auth_param` 参数中使用的相同设置来手动启动助手工具

```bash
/usr/lib64/squid/basic_ldap_auth -b "cn=users,cn=accounts,dc=example,dc=com" -D "uid=proxy_user,cn=users,cn=accounts,dc=example,dc=com" -W /etc/squid/ldap_password -f "(&(objectClass=person)(uid=%s))" -ZZ -H ldap://ldap_server.example.com:389
```

接着在下方输入一个有效的用户名和密码，然后按 **Enter** 键

```bash
user_name password
```

如果帮助程序返回 `OK`，则身份验证成功

#### 3.5 将Squid设置为带有Kerberos身份验证的缓存代理

这部分是squid缓存代理的基本配置，它使用 Kerberos 向 Active Directory(AD)验证用户

只有经过身份验证的用户才可以使用代理

#### 前提

-   &#x20;`/etc/squid/squid.conf` 文件是由 `squid` 软件包提供的默认版本

如果在之前编辑了这个文件，请删除该文件并重新安装该软件包

-   要安装 Squid 的服务器是 AD 域的成员，即需要一个被设置为域成员的服务器

#### 流程

1.安装Kerberos客户端和相关工具

**krb5-workstation**在opencloudos中缺失

安装krb5-client

```bash
yum install squid krb5-clientx86_64
```

2.以AD域管理员的身份进行身份验证

使用kinit命令以AD域管理员的身份进行Kerberos身份验证。

通过提供管理员的凭据，可以获取Kerberos票据并建立安全连接。

```bash
kinit administrator@AD.EXAMPLE.COM
```

3.创建Squid的keytab文件

导出`KRB5_KTNAME`环境变量，设置Squid keytab文件的路径为`/etc/squid/HTTP.keytab`

使用`net ads keytab CREATE -U administrator`命令为Squid创建keytab文件。在执行此命令之前，确保已通过身份验证获取了管理员的Kerberos票据

```bash
export KRB5_KTNAME=FILE:/etc/squid/HTTP.keytab
net ads keytab CREATE -U administrator
```

4.添加HTTP服务主体到keytab中

使用`net ads keytab ADD HTTP -U administrator`命令将HTTP服务主体添加到keytab文件中

```bash
net ads keytab ADD HTTP -U administrator
```

5\. 设置keytab文件所有者

使用`chown`命令将keytab文件`/etc/squid/HTTP.keytab`的所有者设置为squid用户。这确保只有Squid用户可以读取和使用该文件

```bash
chown squid /etc/squid/HTTP.keytab
```

6.(可选)验证keytab 文件是否包含代理服务器的完全限定域名(FQDN)的HTTP服务主体

```bash
klist -k /etc/squid/HTTP.keytab
```

7\. 编辑 `/etc/squid/squid.conf` 文件

在文件顶部添加如下配置条目：

```bash
auth_param negotiate program /usr/lib64/squid/negotiate_kerberos_auth -k /etc/squid/HTTP.keytab -s HTTP/proxy.ad.example.com@AD.EXAMPLE.COM
```

-   `-k /etc/squid/HTTP.keytab`：指定用于Kerberos身份验证的keytab文件的路径。在这里，keytab文件的路径为`/etc/squid/HTTP.keytab`，这是之前创建的包含Squid的Kerberos服务主体密钥的keytab文件。
-   `-s HTTP/proxy.ad.example.com@AD.EXAMPLE.COM`：指定用于身份验证的服务主体（Service Principal Name，SPN）。在这里，指定的SPN是`HTTP/proxy.ad.example.com@AD.EXAMPLE.COM`，其中`proxy.ad.example.com`是Squid代理的主机名
-
-   另外，还有其他参数来帮助程序来启用日志：
-   `-i` 记录信息
-   `-d` 启用调试日志

    Squid 会将助手工具中的调试信息记录到 `/var/log/squid/cache.log` 文件中

然后添加以下 ACL 和规则：

```bash
acl kerb-auth proxy_auth REQUIRED
http_access allow kerb-auth
```

删除下面的规则：

```bash
http_access allow localnet
```

其余配置规则和3.1节相同

#### 验证

```bash
curl -O -L "https://www.redhat.com/index.html" --proxy-negotiate -u : -x "proxy.ad.example.com:3128"
```

当前目录下载index.html证明代理工作正常

也可以手动测试Kerberos 身份验证

```bash
kinit user@AD.EXAMPLE.COM

/usr/lib64/squid/negotiate_kerberos_auth_test proxy.ad.example.com


```

如果返回如下令牌，则身份验证成功

```bash
Token: YIIFtAYGKwYBBQUCoIIFqDC...
```
