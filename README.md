# SIG简介
OpenCloudOS SIG-Stream（L1 Stream SIG）的主要职责是从上游社区选型软件包并完成打包、集成，
发布并维护不依赖其他社区发行版的Stream版本，作为L2、L3 OpenCloudOS的基础。


# 邮件列表
- 组列表 
- 组长专用
- 秘书专用

# 成员 Member
- 王佳 jasperwang@tencent.com （腾讯）
- 龚文 gongwen@nfschina.com   （中科方德）
- 杨科 yangke@redflag-os.com  （红旗）
- 蔡瞿 cai.qu@zte.com.cn      （中兴）

# SIG会议
见[社区日历](https://lists.opencloudos.org/calendar)

# 如何参与
1. 注册gitee账号
2. 加入https://gitee.com/OpenCloudOS-stream 组织
3. 参与sig贡献。
