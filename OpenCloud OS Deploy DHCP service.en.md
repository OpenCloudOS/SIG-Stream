## OpenCloudOS Deploy DHCP service



## 1. Install `dhcp-server` software package

**Prerequisites**

​	You are logged in as the `root` user.

**Steps**

```
# dnf isntall dhcp-server
```

![Snipaste_2023-05-23_14-40-55](https://s2.loli.net/2023/05/23/L8oAySuFtrIm6lM.png)



## 2. Adding network interfaces for the DHCP servers



By default, if we want to deploy the DHCP service, we need the server to listen to the connected network interface. If the DHCP service already exists on the interface, we need to close it or create a new network interface:：

**Steps**

+ Firstly,  Add a virtual network interface and close the default `dhcp` service, and record the subnet `IP` and mask here
  ![image-20230523151408390](https://s2.loli.net/2023/05/23/VwMfk73S9boREtO.png)

+ Add the newly created `VMnet1` network card to the server

![image-20230523151634223](https://s2.loli.net/2023/05/23/CiWYQTvhqgmwZHI.png)

+ Configure the `IP` and other information of the new network card for the server

  + Use the `ifconfig` command to view the newly added network interface, here is `ens36`![image-20230523152008962](https://s2.loli.net/2023/05/23/BuxgpQwNd7RHX8h.png)
    
  + Modify the configuration file of the corresponding interface and add the following content, where the default `IP` is set to  `192.168.131.1` ，`2001:db8:0:1::1/64`
  
    ```
    # vi /etc/sysconfig/network-scripts/ifcfg-ens36
    ```
  
    ```shell
    TYPE=Ethernet
    PROXY_METHOD=none
    BROWSER_ONLY=no
    BOOTPROTO=static
    DEFROUTE=yes
    IPADDR=192.168.131.1      #Customize the IP, consistent with the network segment of the subnet IP
    NETMASK=255.255.255.0
    IPV4_FAILURE_FATAL=no
    IPV6INIT=yes #Whether to enable IPV6 address at boot
    IPV6_AUTOCONF=no #Whether to use automatic configuration of IPv6 addresses
    IPV6_FAILURE_FATAL=no
    IPV6ADDR=2001:db8:0:1::1/64 #Static IPV6 address
    IPV6_DEFAULTGW=2001:db8:0:1::1 #IPV6 address gateway
    NAME=ens36
    DEVICE=ens36    #The device name of the newly added network card
    ONBOOT=yes
    PREFIX=24
    ZONE=trusted
    ```
  
  + Restart the network or device to check whether `IP` is obtained successfully
  
    ```
    # nmcli c reload
    # ip a
    ```
  
    ![image-20230524145943606](https://s2.loli.net/2023/05/24/2MvGlIiqCxgt91r.png)

## 3. Configuring the radvd service for IPv6 routers

The router advertisement daemon (`radvd`) sends router advertisement messages that are required for IPv6 stateless autoconfiguration. This enables users to automatically configure their addresses, settings, routes, and to choose a default router based on these advertisements.

> Note! You can only set `/64` prefixes in the `radvd` service. To use other prefixes, use DHCPv6.

**Prerequisites**

- You are logged in as the `root` user.

**Procedure**

1. Install the `radvd` package:

   ```none
   # dnf install radvd
   ```

2. Edit the `/etc/radvd.conf` file, and add the following configuration:

   ```none
   interface enp1s0
   {
     AdvSendAdvert on;
     AdvManagedFlag on;
     AdvOtherConfigFlag on;
   
     prefix 2001:db8:0:1::/64 {
     };
   };
   ```

   These settings configures `radvd` to send router advertisement messages on the `enp1s0` device for the `2001:db8:0:1::/64` subnet. The `AdvManagedFlag on` setting defines that the client should receive the IP address from a DHCP server, and the `AdvOtherConfigFlag` parameter set to `on` defines that clients should receive non-address information from the DHCP server as well.

3. Optionally, configure that `radvd` automatically starts when the system boots:

   ```none
   # systemctl enable radvd
   ```

4. Start the `radvd` service:

   ```none
   # systemctl start radvd
   ```

5. Optionally, display the content of router advertisement packages and the configured values `radvd` sends:

   ```none
   # radvdump
   ```

## 3. (Optionally)Setting network interfaces for the DHCP servers

By default, the `dhcpd` service processes requests only on network interfaces that have an IP address in the subnet defined in the configuration file of the service.

**Only follow this procedure if** the DHCP server contains multiple network interfaces connected to the same network but the service should listen only on specific interfaces.

Depending on whether you want to provide DHCP for IPv4, IPv6, or both protocols, see the procedure for:

- [IPv4 networks](#IPV4)
- [IPv6 networks](#IPV6)

**Prerequisites**

- You are logged in as the `root` user.
- The `dhcp-server` package is installed.

**Procedure**

- For <a name='IPV4'>IPv4</a> networks:

  1. Copy the `/usr/lib/systemd/system/dhcpd.service` file to the `/etc/systemd/system/` directory:

     ```none
     # cp /usr/lib/systemd/system/dhcpd.service /etc/systemd/system/
     ```

     Do not edit the `/usr/lib/systemd/system/dhcpd.service` file. Future updates of the `dhcp-server` package can override the changes.

  2. Edit the `/etc/systemd/system/dhcpd.service` file, and append the names of the interface, that `dhcpd` should listen on to the command in the `ExecStart` parameter:

     ```none
     ExecStart=/usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid $DHCPDARGS ens36
     ```

     This example configures that `dhcpd` listens only on the `*enp0s1*` and `enp7s0` interfaces.

  3. Reload the `systemd` manager configuration:

     ```none
     # systemctl daemon-reload
     ```

  4. Restart the `dhcpd` service:

     ```none
     # systemctl restart dhcpd.service
     ```

- For <a name='IPV6'>IPv6</a> networks:

  1. Copy the `/usr/lib/systemd/system/dhcpd6.service` file to the `/etc/systemd/system/` directory:

     ```none
     # cp /usr/lib/systemd/system/dhcpd6.service /etc/systemd/system/
     ```

     Do not edit the `/usr/lib/systemd/system/dhcpd6.service` file. Future updates of the `dhcp-server` package can override the changes.

  2. Edit the `/etc/systemd/system/dhcpd6.service` file, and append the names of the interface, that `dhcpd` should listen on to the command in the `ExecStart` parameter:

     ```none
     ExecStart=/usr/sbin/dhcpd -f -6 -cf /etc/dhcp/dhcpd6.conf -user dhcpd -group dhcpd --no-pid $DHCPDARGS ens36
     ```

     This example configures that `dhcpd` listens only on the `*enp0s1*` and `enp7s0` interfaces.

  3. Reload the `systemd` manager configuration:

     ```none
     # systemctl daemon-reload
     ```

  4. Restart the `dhcpd6` service:

     ```none
     # systemctl restart dhcpd6.service
     ```

## 4. Setting up the DHCP service for subnets directly connected to the DHCP server

Use the following procedure if the DHCP server is directly connected to the subnet for which the server should answer DHCP requests. 

Depending on whether you want to provide DHCP for IPv4, IPv6, or both protocols, see the procedure for:

- [IPv4 networks](#ipv4)
- [IPv6 networks](#ipv6)

**Prerequisites**

- You are logged in as the `root` user.
- The `dhcp-server` package is installed.

**Steps**

- For <a name="ipv4">IPv4</a> networks:

  1. Edit the `/etc/dhcp/dhcpd.conf` file:

     1. Optionally, add global parameters that `dhcpd` uses as default if no other directives contain these settings:

        ```none
        option domain-name "example.com";
        default-lease-time 86400;
        ```

        This example sets the default domain name for the connection to `example.com`, and the default lease time to `86400` seconds (1 day).

     2. Add the `authoritative` statement on a new line:

        ```none
        authoritative;
        ```

        > Without the `authoritative` statement, the `dhcpd` service does not answer `DHCPREQUEST` messages with `DHCPNAK` if a client asks for an address that is outside of the pool.

     3. For each IPv4 subnet directly connected to an interface of the server, add a `subnet` declaration:

        ```none
        subnet 192.168.131.0 netmask 255.255.255.0 {
          range 192.168.131.20 192.168.131.100;
          option domain-name-servers 192.168.131.1;
          option routers 192.168.131.1;
          option broadcast-address 192.168.131.255;
          max-lease-time 172800;
        }
        ```

        - This example adds a subnet declaration for the 192.0.2.0/24 network. With this configuration, the DHCP server assigns the following settings to a client that sends a DHCP request from this subnet:
          - A free IPv4 address from the range defined in the `range` parameter
          - IP of the DNS server for this subnet: `192.168.131.1`
          - Default gateway for this subnet: `192.168.131.1`
          - Broadcast address for this subnet: `192.168.131.255`
          - The maximum lease time, after which clients in this subnet release the IP and send a new request to the server: `172800` seconds (2 days)
     
  2. After the configuration is complete, you can use the following command to check the grammar problem. The screenshot shows the output when there is no problem:
  
     ```
     # dhcpd -t
     ```
  
     ![image-20230524143438764](https://s2.loli.net/2023/05/24/dCKnawJUbjRVqW1.png)
  
  3. Optionally, configure that `dhcpd` starts automatically when the system boots:
  
     ```none
     # systemctl enable dhcpd
     ```
  
  4. Start the `dhcpd` service:
  
     ```none
     # systemctl start dhcpd
     ```
  
  5. The client can obtain a new `ip` address through the command `dhclient ens36 -r`
  
- For <a name="ipv6">IPv6</a> networks: 

  1. Edit the `/etc/dhcp/dhcpd6.conf` file:

     1. Optionally, add global parameters that `dhcpd` uses as default if no other directives contain these settings:

        ```none
        option dhcp6.domain-search "example.com";
        default-lease-time 86400;
        ```

        This example sets the default domain name for the connection to `example.com`, and the default lease time to `86400` seconds (1 day).

     2. Add the `authoritative` statement on a new line:

        ```none
        authoritative;
        ```

        > Without the `authoritative` statement, the `dhcpd` service does not answer `DHCPREQUEST` messages with `DHCPNAK` if a client asks for an address that is outside of the pool.

     3. For each IPv6 subnet directly connected to an interface of the server, add a `subnet` declaration:

        ```none
        subnet6 2001:db8:0:1::/64 {
          range6 2001:db8:0:1::20 2001:db8:0:1::100;
          option dhcp6.name-servers 2001:db8:0:1::1;
          max-lease-time 172800;
        }
        ```

        This example adds a subnet declaration for the 2001:db8:0:1::/64 network. With this configuration, the DHCP server assigns the following settings to a client that sends a DHCP request from this subnet:

        - A free IPv6 address from the range defined in the `range6` parameter.

        - The IP of the DNS server for this subnet is `2001:db8:0:1::1`.

        - The maximum lease time, after which clients in this subnet release the IP and send a new request to the server is `172800` seconds (2 days).

          Note that IPv6 requires uses router advertisement messages to identify the default gateway.

  2. After the configuration is complete, you can use the following command to check the grammar problem. The screenshot shows the output when there is no problem:

     ```
     # dhcpd -6 -t
     ```

     ![image-20230524150407465](https://s2.loli.net/2023/05/24/zqpNCmi5Zd3L982.png)

  3. Optionally, configure that `dhcpd6` starts automatically when the system boots:

     ```none
     # systemctl enable dhcpd6
     ```

  4. Start the `dhcpd6` service:

     ```none
     # systemctl start dhcpd6
     ```

  5. The client can obtain a new `ip` address through the command `dhclient ens36 -r`

## 5. (Setting after relay deployed)Setting up the DHCP service for subnets that are not directly connected to the DHCP server

Use the following procedure if the DHCP server is not directly connected to the subnet for which the server should answer DHCP requests. This is the case if a DHCP relay agent forwards requests to the DHCP server, because none of the DHCP server’s interfaces is directly connected to the subnet the server should serve.

Depending on whether you want to provide DHCP for IPv4, IPv6, or both protocols, see the procedure for:

- IPv4 networks
- IPv6 networks

**Prerequisites**

- You are logged in as the `root` user.
- The `dhcp-server` package is installed.

**Procedure**

- For IPv4 networks:

  1. Edit the `/etc/dhcp/dhcpd.conf` file:

     1. Optionally, add global parameters that `dhcpd` uses as default if no other directives contain these settings:

        ```none
        option domain-name "example.com";
        default-lease-time 86400;
        ```

        This example sets the default domain name for the connection to `example.com`, and the default lease time to `86400` seconds (1 day).

     2. Add the `authoritative` statement on a new line:

        ```none
        authoritative;
        ```

        > Without the `authoritative` statement, the `dhcpd` service does not answer `DHCPREQUEST` messages with `DHCPNAK` if a client asks for an address that is outside of the pool.

     3. Add a `shared-network` declaration, such as the following, for IPv4 subnets that are not directly connected to an interface of the server:

        ```none
        shared-network example {
          option domain-name-servers 192.0.2.1;
          ...
        
          subnet 192.0.2.0 netmask 255.255.255.0 {
            range 192.0.2.20 192.0.2.100;
            option routers 192.0.2.1;
          }
        
          subnet 198.51.100.0 netmask 255.255.255.0 {
            range 198.51.100.20 198.51.100.100;
            option routers 198.51.100.1;
          }
          ...
        }
        ```

        This example adds a shared network declaration, that contains a `subnet` declaration for both the 192.0.2.0/24 and 198.51.100.0/24 networks. With this configuration, the DHCP server assigns the following settings to a client that sends a DHCP request from one of these subnets:

        - The IP of the DNS server for clients from both subnets is: `192.0.2.1`.
        - A free IPv4 address from the range defined in the `range` parameter, depending on from which subnet the client sent the request.
        - The default gateway is either `192.0.2.1` or `198.51.100.1` depending on from which subnet the client sent the request.

     4. Add a `subnet` declaration for the subnet the server is directly connected to and that is used to reach the remote subnets specified in `shared-network` above:

        ```none
        subnet 203.0.113.0 netmask 255.255.255.0 {
        }
        ```

        > If the server does not provide DHCP service to this subnet, the `subnet` declaration must be empty as shown in the example. Without a declaration for the directly connected subnet, `dhcpd` does not start.

  2. Optionally, configure that `dhcpd` starts automatically when the system boots:

     ```none
     # systemctl enable dhcpd
     ```

  3. Start the `dhcpd` service:

     ```none
     # systemctl start dhcpd
     ```

- For IPv6 networks:

  1. Edit the `/etc/dhcp/dhcpd6.conf` file:

     1. Optionally, add global parameters that `dhcpd` uses as default if no other directives contain these settings:

        ```none
        option dhcp6.domain-search "example.com";
        default-lease-time 86400;
        ```

        This example sets the default domain name for the connection to `example.com`, and the default lease time to `86400` seconds (1 day).

     2. Add the `authoritative` statement on a new line:

        ```none
        authoritative;
        ```

        > Without the `authoritative` statement, the `dhcpd` service does not answer `DHCPREQUEST` messages with `DHCPNAK` if a client asks for an address that is outside of the pool.

     3. Add a `shared-network` declaration, such as the following, for IPv6 subnets that are not directly connected to an interface of the server:

        ```none
        shared-network example {
          option domain-name-servers 2001:db8:0:1::1:1
          ...
        
          subnet6 2001:db8:0:1::1:0/120 {
            range6 2001:db8:0:1::1:20 2001:db8:0:1::1:100
          }
        
          subnet6 2001:db8:0:1::2:0/120 {
            range6 2001:db8:0:1::2:20 2001:db8:0:1::2:100
          }
          ...
        }
        ```

        This example adds a shared network declaration that contains a `subnet6` declaration for both the 2001:db8:0:1::1:0/120 and 2001:db8:0:1::2:0/120 networks. With this configuration, the DHCP server assigns the following settings to a client that sends a DHCP request from one of these subnets:

        - The IP of the DNS server for clients from both subnets is `2001:db8:0:1::1:1`.

        - A free IPv6 address from the range defined in the `range6` parameter, depending on from which subnet the client sent the request.

          Note that IPv6 requires uses router advertisement messages to identify the default gateway.

     4. Add a `subnet6` declaration for the subnet the server is directly connected to and that is used to reach the remote subnets specified in `shared-network` above:

        ```none
        subnet6 2001:db8:0:1::50:0/120 {
        }
        ```

        > If the server does not provide DHCP service to this subnet, the `subnet6` declaration must be empty as shown in the example. Without a declaration for the directly connected subnet, `dhcpd` does not start.

  2. Optionally, configure that `dhcpd6` starts automatically when the system boots:

     ```none
     # systemctl enable dhcpd6
     ```

  3. Start the `dhcpd6` service:

     ```none
     # systemctl start dhcpd6
     ```

## 6. (Optionally)Setting up a DHCP relay agent

The DHCP Relay Agent (`dhcrelay`) enables the relay of DHCP and BOOTP requests from a subnet with no DHCP server on it to one or more DHCP servers on other subnets. When a DHCP client requests information, the DHCP Relay Agent forwards the request to the list of DHCP servers specified. When a DHCP server returns a reply, the DHCP Relay Agent forwards this request to the client.

Depending on whether you want to set up a DHCP relay for IPv4, IPv6, or both protocols, see the procedure for:

- IPv4 networks
- IPv6 networks

**Prerequisites**

- You are logged in as the `root` user.

**Procedure**

- For IPv4 networks:

  1. Install the `dhcp-relay` package:

     ```none
     # dnf install dhcp-relay
     ```

  2. Copy the `/lib/systemd/system/dhcrelay.service` file to the `/etc/systemd/system/` directory:

     ```none
     # cp /lib/systemd/system/dhcrelay.service /etc/systemd/system/
     ```

     Do not edit the `/usr/lib/systemd/system/dhcrelay.service` file. Future updates of the `dhcp-relay` package can override the changes.

  3. Edit the `/etc/systemd/system/dhcrelay.service` file, and append the `-i *interface*` parameter, together with a list of IP addresses of DHCPv4 servers that are responsible for the subnet:

     ```none
     ExecStart=/usr/sbin/dhcrelay -d --no-pid -i enp1s0 192.168.131.1
     ```

     With these additional parameters, `dhcrelay` listens for DHCPv4 requests on the `enp1s0` interface and forwards them to the DHCP server with the IP `192.168.131.1`.

  4. Reload the `systemd` manager configuration:

     ```none
     # systemctl daemon-reload
     ```

  5. Optionally, configure that the `dhcrelay` service starts when the system boots:

     ```none
     # systemctl enable dhcrelay.service
     ```

  6. Start the `dhcrelay` service:

     ```none
     # systemctl start dhcrelay.service
     ```

- For IPv6 networks:

  1. Install the `dhcp-relay` package:

     ```none
     # dnf install dhcp-relay
     ```

  2. Copy the `/lib/systemd/system/dhcrelay.service` file to the `/etc/systemd/system/` directory and name the file `dhcrelay6.service`:

     ```none
     # cp /lib/systemd/system/dhcrelay.service /etc/systemd/system/dhcrelay6.service
     ```

     Do not edit the `/usr/lib/systemd/system/dhcrelay.service` file. Future updates of the `dhcp-relay` package can override the changes.

  3. Edit the `/etc/systemd/system/dhcrelay6.service` file, and append the `-l *receiving_interface*` and `-u *outgoing_interface*` parameters:

     ```
     ExecStart=/usr/sbin/dhcrelay -d --no-pid -l ens33 -u ens36
     ```

     With these additional parameters, `dhcrelay` listens for DHCPv6 requests on the `ens33` interface and forwards them to the network connected to the `ens36` interface.

  4. Reload the `systemd` manager configuration:

     ```none
     # systemctl daemon-reload
     ```

  5. Optionally, configure that the `dhcrelay6` service starts when the system boots:

     ```none
     # systemctl enable dhcrelay6.service
     ```

  6. Start the `dhcrelay6` service:

     ```none
     # systemctl start dhcrelay6.service
     ```

## 7. (Optionally)Assigning a static address to a host using DHCP

Using a `host` declaration, you can configure the DHCP server to assign a fixed IP address to a media access control (MAC) address of a host. For example, use this method to always assign the same IP address to a server or network device.

Depending on whether you want to configure fixed addresses for IPv4, IPv6, or both protocols, see the procedure for:

- IPv4 networks
- IPv6 networks

**Prerequisites**

- The `dhcpd` service is configured and running.
- You are logged in as the `root` user.

**Procedure**

- For IPv4 networks:

  1. Edit the `/etc/dhcp/dhcpd.conf` file:

     1. Add a `host` declaration:

        ```none
        host server.example.com {
        	hardware ethernet 52:54:00:72:2f:6e;
        	fixed-address 192.168.131.130;
        }
        ```

        This example configures the DHCP server to always assign the `192.168.131.130` IP address to the host with the `52:54:00:72:2f:6e` MAC address.

        The `dhcpd` service identifies systems by the MAC address specified in the `fixed-address` parameter, and not by the name in the `host` declaration. As a consequence, you can set this name to any string that does not match other `host` declarations. To configure the same system for multiple networks, use a different name, otherwise, `dhcpd` fails to start.

     2. Optionally, add further settings to the `host` declaration that are specific for this host.

  2. Restart the `dhcpd` service:

     ```none
     # systemctl start dhcpd
     ```

- For IPv6 networks:

  1. Edit the `/etc/dhcp/dhcpd6.conf` file:

     1. Add a `host` declaration:

        ```none
        host server.example.com {
        	hardware ethernet 52:54:00:72:2f:6e;
        	fixed-address6 2001:db8:0:1::200;
        }
        ```

        This example configures the DHCP server to always assign the `2001:db8:0:1::20` IP address to the host with the `52:54:00:72:2f:6e` MAC address.

        The `dhcpd` service identifies systems by the MAC address specified in the `fixed-address6` parameter, and not by the name in the `host` declaration. As a consequence, you can set this name to any string, provided that it is unique to other `host` declarations. To configure the same system for multiple networks, use a different name because, otherwise, `dhcpd` fails to start.

     2. Optionally, add further settings to the `host` declaration that are specific for this host.

  2. Restart the `dhcpd6` service:

     ```none
     # systemctl start dhcpd6
     ```

## 8. (Optionally)Using a group declaration to apply parameters to multiple hosts, subnets, and shared networks at the same time

Using a `group` declaration, you can apply the same parameters to multiple hosts, subnets, and shared networks.

Note that the procedure describes using a `group` declaration for hosts, but the steps are the same for subnets and shared networks.

Depending on whether you want to configure a group for IPv4, IPv6, or both protocols, see the procedure for:

- IPv4 networks
- IPv6 networks

**Prerequisites**

- The `dhcpd` service is configured and running.
- You are logged in as the `root` user.

**Procedure**

- For IPv4 networks:

  1. Edit the `/etc/dhcp/dhcpd.conf` file:

     1. Add a `group` declaration:

        ```none
        group {
          option domain-name-servers 192.168.131.1;
        
          host server1.example.com {
            hardware ethernet 52:54:00:72:2f:6e;
            fixed-address 192.168.131.130;
          }
        
          host server2.example.com {
            hardware ethernet 52:54:00:1b:f3:cf;
            fixed-address 192.168.131.140;
          }
        }
        ```

        This `group` definition groups two `host` entries. The `dhcpd` service applies the value set in the `option domain-name-servers` parameter to both hosts in the group.

     2. Optionally, add further settings to the `group` declaration that are specific for these hosts.

  2. Restart the `dhcpd` service:

     ```none
     # systemctl start dhcpd
     ```

- For IPv6 networks:

  1. Edit the `/etc/dhcp/dhcpd6.conf` file:

     1. Add a `group` declaration:

        ```none
        group {
          option dhcp6.domain-search "example.com";
        
          host server1.example.com {
            hardware ethernet 52:54:00:72:2f:6e;
            fixed-address 2001:db8:0:1::200;
          }
        
          host server2.example.com {
            hardware ethernet 52:54:00:1b:f3:cf;
            fixed-address 2001:db8:0:1::ba3;
          }
        }
        ```

        This `group` definition groups two `host` entries. The `dhcpd` service applies the value set in the `option dhcp6.domain-search` parameter to both hosts in the group.

     2. Optionally, add further settings to the `group` declaration that are specific for these hosts.

  2. Restart the `dhcpd6` service:

     ```none
     # systemctl start dhcpd6
     ```

## 9. (Optionally)Restoring a corrupt lease database

If the DHCP server logs an error that is related to the lease database, such as `Corrupt lease file - possible data loss!`,you can restore the lease database from the copy the `dhcpd` service created. Note that this copy might not reflect the latest status of the database.

**If you remove the lease database instead of replacing it with a backup, you lose all information about the currently assigned leases. As a consequence, the DHCP server could assign leases to clients that have been previously assigned to other hosts and are not expired yet. This leads to IP conflicts.**

Depending on whether you want to restore the DHCPv4, DHCPv6, or both databases, see the procedure for:

- Restoring the DHCPv4 lease database
- Restoring the DHCPv6 lease database

**Prerequisites**

- You are logged in as the `root` user.
- The lease database is corrupt.

**Procedure**

- Restoring the DHCPv4 lease database:

  1. Stop the `dhcpd` service:

     ```none
     # systemctl stop dhcpd
     ```

  2. Rename the corrupt lease database:

     ```none
     # mv /var/lib/dhcpd/dhcpd.leases /var/lib/dhcpd/dhcpd.leases.corrupt
     ```

  3. Restore the copy of the lease database that the `dhcp` service created when it refreshed the lease database:

     ```none
     # cp -p /var/lib/dhcpd/dhcpd.leases~ /var/lib/dhcpd/dhcpd.leases
     ```

     > If you have a more recent backup of the lease database, restore this backup instead.

  4. Start the `dhcpd` service:

     ```none
     # systemctl start dhcpd
     ```

- Restoring the DHCPv6 lease database:

  1. Stop the `dhcpd6` service:

     ```none
     # systemctl stop dhcpd6
     ```

  2. Rename the corrupt lease database:

     ```none
     # mv /var/lib/dhcpd/dhcpd6.leases /var/lib/dhcpd/dhcpd6.leases.corrupt
     ```

  3. Restore the copy of the lease database that the `dhcp` service created when it refreshed the lease database:

     ```none
     # cp -p /var/lib/dhcpd/dhcpd6.leases~ /var/lib/dhcpd/dhcpd6.leases
     ```

     > If you have a more recent backup of the lease database, restore this backup instead.

  4. Start the `dhcpd6` service:

     ```none
     # systemctl start dhcpd6
     ```
