以下是利用 rrdtool 创建数据库，并对存储、检索、图形展示等功能进行验证的步骤：

**1.安装 rrdtool(可选)**

请使用以下命令在 CentOS 系统上安装 rrdtool：

```
sudo yum install rrdtool
```

OpenCloudOS Stream 2301 已经部署好rrdtool，故此步骤可省略。

![image-20230529161402067](https://gitee.com/ychhenu/typora-img/raw/master/img/image-20230529161402067.png)

**2.创建 RRD 数据库**

使用以下命令创建一个 RRD 数据库：

```
rrdtool create temp.rrd --start now --step 60 DS:temperature:GAUGE:120:-50:50 RRA:AVERAGE:0.5:1:1440
```

这个命令将创建一个名为 temp.rrd 的 RRD 数据库，开始时间为当前时间，每分钟采集一次数据。DS 指标定义了要存储的数据源，RRA 定义了如何存储数据。在本例中，数据源为 temperature，并且将采集 GAUGE 类型的数据，每 120 秒计算一次平均值，并且在 -50 到 50 范围内存储。RRA 用于定义如何存储每个数据点的精度和数量。在本例中，我们使用 AVERAGE 表来存储所有采样点的平均值，并在这个表中存储最近的 1440 个数据点（即一天的数据）。

**3.更新 RRD 数据库**

使用以下命令向 RRD 数据库添加数据：

```
rrdtool update temp.rrd N:30
```

这个命令将在当前时间戳下记录温度为 30 的值。如果您想每分钟自动更新一次数据，请将此命令添加到一个脚本test.sh中并使用 crontab 每分钟执行一次。

做法如下所示：

```
vim test.sh
```

test.sh内容如下所示：

```
#!/bin/bash
rrdtool update temp.rrd N:30
```

添加执行权限

在终端中使用以下命令添加执行权限：

```
chmod +x test.sh
```

1. 添加 crontab 任务

使用以下命令打开 crontab 编辑器：

```
crontab -e
```

在编辑器中输入以下内容以将test.sh脚本每分钟执行一次：

```
* * * * * /path/to/test.sh
```

其中，“/path/to/test.sh” 是 test.sh 脚本所在目录的路径。上述内容意味着该脚本将在每小时的每一分钟执行一次。



![image-20230529155433126](https://gitee.com/ychhenu/typora-img/raw/master/img/image-20230529155433126.png)

**4.绘制 RRD 图表**

使用以下命令绘制 RRD 图表：

```
rrdtool graph temp.png --start -1h --end now DEF:temp=temp.rrd:temperature:AVERAGE LINE2:temp#0000FF:"Temperature (C)"
```

这个命令将创建一个名为 temp.png 的 PNG 图像文件，显示最近一小时的温度数据。DEF 指令定义了要使用的 RRD 数据库和数据源，而 LINE2，则用来画出连接不同采集点的折线图，并且使用颜色为蓝色，将图表标题设置为“Temperature (C)”。

输出图片如下图所示：

![image-20230529155510612](https://gitee.com/ychhenu/typora-img/raw/master/img/image-20230529155510612.png)

**5.版本查询、更新和删除 RRD 文件**

可以使用 `rrdtool -v` 命令查询 rrdtool 的版本。

使用 rrdupdate 命令对 RRD 文件进行更新：

```
rrdupdate temp.rrd N:30
```

使用 rrdinfo 命令查看 RRD 文件的信息：

```
rrdinfo temp.rrd
```

![image-20230529160515334](https://gitee.com/ychhenu/typora-img/raw/master/img/image-20230529160515334.png)

使用 rrdtool tune 命令对 RRD 文件进行调整：

```
rrdtool tune temp.rrd -a temperature
```

使用 rrdtool dump 命令将 RRD 文件转储为 XML 文件：

```
rrdtool dump temp.rrd > temp.xml
```

使用 rrdtool restore 命令将 XML 文件还原成 RRD 文件：

```
rrdtool restore temp.xml temp1.rrd
```

![image-20230529160231193](https://gitee.com/ychhenu/typora-img/raw/master/img/image-20230529160231193.png)

使用 rrdtool info 命令查询 RRD 文件的信息：

```
rrdtool info temp1.rrd
```

![image-20230529160540267](https://gitee.com/ychhenu/typora-img/raw/master/img/image-20230529160540267.png)

**6.删除RRD数据库**

1)确认 RRD 数据库文件名

在删除 RRD 数据库之前，确认该数据库的文件名并确认其所在目录。

```
ls *.rrd
```

2)停止任何使用该 RRD 数据库的进程

使用以下命令检查是否有正在运行的 rrdcached 进程：

```
ps aux | grep rrdcached
```

如果存在 rrdcached 进程，使用以下命令停止：

```
sudo systemctl stop rrdcached
```

3)删除 RRD 数据库文件

使用以下命令删除 RRD 数据库文件：

```
sudo rm /path/to/rrd/file.rrd
```

其中，“/path/to/rrd/file.rrd” 是要删除的 RRD 数据库文件的完整路径。

![image-20230529161428945](https://gitee.com/ychhenu/typora-img/raw/master/img/image-20230529161428945.png)