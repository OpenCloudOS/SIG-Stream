# 部署Hive

## 一丶安装 Hive

1. 根目录下创建soft目录

2. 传输 apache-hive-2.3.6-bin.tar.gz 到 soft 目录下并解压

3. 修改环境变量,添加Hive环境变量

   ```bash
   vi /etc/profile
		
			#hive安装目录
					export HIVE_HOME=/home/hzx/soft/apache-hive-2.3.6-bin    
				 	export PATH=$PATH:$HIVE_HOME/bin
   source /etc/profile   //应用变量更改
   ```

4. 检测Hive是否安装成功
   ```bash
   hive --version   //出现hive版本号即为成功
   ```

## 二丶配置 Hive

1. 在目录 /soft/apache-hive-2.3.6-bin 下创建目录 tmp 来存放hive运行过程中生成的临时文件

2. 启动hadoop
   ```bash
   start-all.sh
   ```

3. 在 HDFS 上创建2个文件夹
   ```bash
   hdfs dfs -mkdir -p /root/hive
   hdfs dfs -mkdir -p /root/hive/warehouse
   ```

4. 进入目录 /soft/apache-hive-2.3.6-bin/conf 创建 文件 hive-site.xml
   （该目录下默认没有 hive-site.xml 文件 ，该文件可以通过复制 hive 自带的文件 hive-default.xml.template 来创建）
   ```bash
   cd /soft/apache-hive-2.3.6-bin/conf
   cp hive-default.xml.template hive-site.xml
   ```

5. 进入目录 /soft/apache-hive-2.3.6-bin/conf 创建 文件 hive-env.sh
   （该目录下默认没有 hive-env.sh 文件 ，该文件可以通过复制 hive 自带的文件 hive-env.sh.template 来创建）
   ```bash
   cd /soft/apache-hive-2.3.6-bin/conf
   cp hive-env.sh.template hive-env.sh
   ```

6. 将 mysql-connector-java-5.1.18-bin.jar 传输到 apache-hive-2.3.6-bin/lib 目录下

7. 启动另一台机器的 MySQL，登录root用户，更改远程权限，并创建名称为 hive 的数据库
   ```bash
   #建立数据库
   create database if not exists hive;
   #设置远程登录的权限
   GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH      GRANT OPTION;
   #刷新配置
   FLUSH PRIVILEGES;
   ```

## 三丶修改 Hive 的配置文件

1. 修改目录 /soft/apache-hive-2.3.6-bin/conf 下的文件 hive-env.sh
   在文件中添加下列语句（注意自己的目录结构）
   ```bash
		# 设置hadoop的安装路径
		export HADOOP_HOME=/home/hzx/soft/hadoop-2.7.3
   
		# 设置hive配置文件所在路径
		export HIVE_CONF_DIR=/home/hzx/soft/apache-hive-2.3.6-bin/conf
   
		# 配置hive依赖包的所在路径
		export HIVE_AUX_JARS_PATH=/home/hzx/soft/apache-hive-2.3.6-bin/lib
   
   ```

2. 修改目录 /soft/apache-hive-2.3.6-bin/conf 下的文件 hive-site.xml 分别修改以下属性
   #MySQL 连接属性
   javax.jdo.option.ConnectionURL
   javax.jdo.option.ConnectionDriverName
   javax.jdo.option.ConnectionUsername
   javax.jdo.option.ConnectionPassword

   #Hive 在 HDFS 上工作路径
   hive.exec.scratchdir
   #Hive 在存储数据时在 HDFS 中的路径
   hive.metastore.warehouse.dir
   #HIve 临时目录路径
   hive.exec.local.scratchdir
   hive.downloaded.resources.dir
   hive.server2.logging.operation.log.location

   修改效果如下：
```bash
   <property>
    <name>javax.jdo.option.ConnectionURL</name>
    <value>jdbc:mysql://192.168.48.130:3306/hive?useUnicode=true&amp;serverTimezone=UTC&amp;useUnicode=true&amp;characterEncoding=utf8&amp;useSSL=false</value>
    ##此处IP地址为MySQL数据库的地址，文中是用的虚拟机乙的IP地址
    ##因为 MySQL 8.0的要求，所以此处已经添加时区；如果你是MySQL5.0等版本，可以只填写  jdbc:mysql://192.168.48.130:3306/hive
   </property>

<property>
    <name>javax.jdo.option.ConnectionDriverName</name>
    <value>com.mysql.cj.jdbc.Driver</value>
    ##MySQL8.0版本驱动名称为 com.mysql.cj.jdbc.Driver ；如果你是MySQL5.0等版本，驱动名称为com.mysql.jdbc.Driver
</property>

<property>
    <name>javax.jdo.option.ConnectionUserName</name>
    <value>root</value>
</property>

<property>
    <name>javax.jdo.option.ConnectionPassword</name>
    <value>123456</value>
    ##此处密码因人而异
</property>
```
```bash
<property>
    <name>hive.exec.scratchdir</name>
    <value>/home/date/soft/apache-hive-2.3.6-bin</value>
</property>

<property>
    <name>hive.metastore.warehouse.dir</name>
    <value>/root/hive/warehouse</value>
</property>
```
```bash
<property>
    <name>hive.exec.local.scratchdir</name>
    <value>/home/date/soft/apache-hive-2.3.6-bin/tmp/${user.name}</value>
</property>

<property>
    <name>hive.downloaded.resources.dir</name>
    <value>/home/date/soft/apache-hive-2.3.6-bin/tmp/${hive.session.id}_resources</value>
</property>

<property>
    <name>hive.server2.logging.operation.log.location</name>
    <value>/home/centos/soft/Hive/apache-hive-2.3.6-bin/tmp/root/operation_logs</value>
</property>
```

3. 执行初始化命令
   ```bash
   schematool -dbType mysql -initSchema
   ```
   ```bash
   ##当显示类似字样表示MySQL数据库连接成功
   SLF4J: Class path contains multiple SLF4J bindings.
   SLF4J: Found binding in [jar:file:/opt/hive/lib/log4j-slf4j-impl-2.4.1.jar!/org/slf4j/impl/StaticLoggerBinder.class]
   SLF4J: Found binding in [jar:file:/opt/soft/hadoop-2.7.3/share/hadoop/common/lib/slf4j-log4j12-1.7.10.jar!/org/slf4j/impl/StaticLoggerBinder.class]
   SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
   SLF4J: Actual binding is of type [org.apache.logging.slf4j.Log4jLoggerFactory]
   Metastore connection URL:        jdbc:mysql://192.168.56.1:3306/hive
   Metastore Connection Driver :    com.mysql.jdbc.Driver
   Metastore connection User:       root
   Starting metastore schema initialization to 2.1.0
   Initialization script hive-schema-2.1.0.mysql.sql
   Initialization script completed
   schemaTool completed
   ```

4. 运行hive进行试验
   ```bash
   hive
   ```
   ```bash
   ##显示此类字样表示运行成功
   which: no hbase in (/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/date/soft/jdk1.8.0_201/bin:/home/date/soft/jdk1.8.0_201/jre/bin:/home/date/soft/hadoop-2.7.3/bin:/home/date/soft/hadoop-2.7.3/sbin:/home/date/soft/apache-hive-2.3.6-bin/bin:/home/date/.local/bin:/home/date/bin)
   SLF4J: Class path contains multiple SLF4J bindings.
   SLF4J: Found binding in [jar:file:/home/date/soft/apache-hive-2.3.6-bin/lib/log4j-slf4j-impl-2.6.2.jar!/org/slf4j/impl/StaticLoggerBinder.class]
   SLF4J: Found binding in [jar:file:/home/date/soft/hadoop-2.7.3/share/hadoop/common/lib/slf4j-log4j12-1.7.10.jar!/org/slf4j/impl/StaticLoggerBinder.class]
   SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
   SLF4J: Actual binding is of type [org.apache.logging.slf4j.Log4jLoggerFactory]
   Logging initialized using configuration in jar:file:/home/date/soft/apache-hive-2.3.6-bin/lib/hive-common-2.3.6.jar!/hive-log4j2.properties Async: true
   Hive-on-MR is deprecated in Hive 2 and may not be available in the future versions. Consider using a different execution engine (i.e. spark, tez) or using Hive 1.X releases.
   hive>
   
   ```