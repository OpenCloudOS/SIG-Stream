## OpenCloud OS 部署DHCP服务



## 1. 安装 `dhcp-server` 软件包

> 要求已用`root`身份登录

**步骤**

```
# dnf isntall dhcp-server
```

![Snipaste_2023-05-23_14-40-55](https://s2.loli.net/2023/05/23/L8oAySuFtrIm6lM.png)



## 2. 为服务器添加网络接口

默认情况下，如果要部署DHCP服务，需要服务器侦听已连接的网络接口，如果该接口已经存在DHCP服务，则需要啊将其关闭或者新建一个网络接口：

> ​	这里我们选择在虚拟机中新建一个网络接口，并关闭默认的dhcp服务，同时为服务器配置IP等信息

**步骤**

+ 首先添加一个虚拟网络接口并关闭默认的`dhcp`服务，并记录这里的子网`IP`以及掩码
  ![image-20230523151408390](https://s2.loli.net/2023/05/23/VwMfk73S9boREtO.png)

+ 为服务器添加刚才新建的 `VMnet1` 网卡

![image-20230523151634223](https://s2.loli.net/2023/05/23/CiWYQTvhqgmwZHI.png)

+ 为服务器配置新网卡的`IP`等信息

  + 使用`ifconfig` 命令查看新添加的网络接口，此处为 `ens36`
    ![image-20230523152008962](https://s2.loli.net/2023/05/23/BuxgpQwNd7RHX8h.png)

  + 修改对应接口的配置文件，添加如下内容，此处默认 `IP` 设置为  `192.168.131.1` ，`2001:db8:0:1::1/64`

    ```
    # vi /etc/sysconfig/network-scripts/ifcfg-ens36
    ```

    ```shell
    TYPE=Ethernet
    PROXY_METHOD=none
    BROWSER_ONLY=no
    BOOTPROTO=static
    DEFROUTE=yes
    IPADDR=192.168.131.1      #自定义IP，与上方要注意的子网IP的网段一致即可
    NETMASK=255.255.255.0
    IPV4_FAILURE_FATAL=no
    IPV6INIT=yes #是否开机启用IPV6地址
    IPV6_AUTOCONF=no #是否使用IPV6地址的自动配置
    IPV6_FAILURE_FATAL=no#如果配置失败，不会关闭网口，保证IPV4的配置还能生效
    IPV6ADDR=2001:db8:0:1::1/64 #静态IPV6地址
    IPV6_DEFAULTGW=2001:db8:0:1::1 #IPV6地址网关
    NAME=ens36
    DEVICE=ens36    #新添加网卡的设备名
    ONBOOT=yes
    PREFIX=24
    ZONE=trusted
    ```
  
  + 重启网络或者设备，查看 `IP` 是否获取成功

    ```
    # nmcli c reload
    # ip a
    ```
  
    ![image-20230524145943606](https://s2.loli.net/2023/05/24/2MvGlIiqCxgt91r.png)

## 3. 为 IPv6 路由器配置 radvd 服务

在 IPv6 网络中，只有路由器广告消息提供有关 IPv6 默认网关的信息。因此，如果您想在需要默认网关设置的子网中使用 DHCPv6，您必须额外配置路由器广告服务，如路由器广告守护进程(`radvd`)。路由器广告守护进程(`radvd`)发送 IPv6 无状态自动配置所需的路由器广告消息。这可让用户根据这些公告自动配置其地址、设置、路由和选择默认路由器。

> 注意, 您只能在 `radvd` 服务中设置 `/64` 前缀。要使用其他前缀，请使用 DHCPv6。

**先决条件**

- 已以 `root` 用户身份登录。

**步骤**

1. 安装 `radvd` 软件包：

   ```none
   # dnf install radvd
   ```

2. 编辑 `/etc/radvd.conf` 文件，并添加以下配置：

   ```none
   interface ens36
   {
     AdvSendAdvert on;
     AdvManagedFlag on;
     AdvOtherConfigFlag on;
   
     prefix 2001:db8:0:1::/64 {
     };
   };
   ```

   这些设置将 `radvd` 配置为对 `2001:db8:0:1::/64` 子网在 `ens36` 设备上发送路由器广告消息。`AdvManagedFlag on` 设置定义了客户端应从 DHCP 服务器接收 IP 地址，而 `AdvOtherConfigFlag` 参数设为 `on`，定义了客户端也应从 DHCP 服务器接收非地址信息。

3. 另外，可以将 `radvd` 配置为在系统启动时自动启动：

   ```none
   # systemctl enable radvd
   ```

4. 启动 `radvd` 服务：

   ```none
   # systemctl start radvd
   ```

5. 另外，可以显示路由器广告数据包的内容，以及 `radvd` 发送的配置值：

   ```none
   # radvdump
   ```

## 3. (可选)为DHCP服务器配置网络接口

默认情况下，`dhcpd` 服务仅处理网络接口上的请求，该接口在在服务的配置文件有一个在子网中定义的 IP 地址。

**只有 DHCP 服务器包含连接到同一网络的多个网络接口，但服务应该只侦听特定的接口时**，才应按照以下步骤操作。

根据您要为 IPv4、IPv6 或两个协议提供 DHCP 的信息，请查看以下操作过程：

- [IPv4 网络](#IPV4)
- [IPv6 网络](#IPV6)

**先决条件**

- 已以 `root` 用户身份登录。
- `dhcp-server` 软件包已安装。

**步骤**

- <a name="IPV4">对于 IPv4 网络</a>：

  1. 将 `/usr/lib/systemd/system/dhcpd.service` 文件复制到 `/etc/systemd/system/` 目录中：

     ```none
     # cp /usr/lib/systemd/system/dhcpd.service /etc/systemd/system/
     ```

     不要编辑 `/usr/lib/systemd/system/dhcpd.service` 文件。`dhcp-server` 软件包的未来更新可以覆盖更改。

  2. 编辑 `/etc/systemd/system/dhcpd.service` 文件，并追加接口名称，`dhcpd` 应该侦听 `ExecStart` 参数中的命令：

     ```none
     ExecStart=/usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid $DHCPDARGS ens36
     ```

     本例配置了 `dhcpd` 仅侦听 `*ens36*` 接口。

  3. 重新加载 `systemd` 管理器配置：

     ```none
     # systemctl daemon-reload
     ```

  4. 重启 `dhcpd` 服务：

     ```none
     # systemctl restart dhcpd.service
     ```

- <a name="IPV6">对于 IPv6 网络</a>：

  1. 将 `/usr/lib/systemd/system/dhcpd6.service` 文件复制到 `/etc/systemd/system/` 目录中：

     ```none
     # cp /usr/lib/systemd/system/dhcpd6.service /etc/systemd/system/
     ```

     不要编辑 `/usr/lib/systemd/system/dhcpd6.service` 文件。`dhcp-server` 软件包的未来更新可以覆盖更改。

  2. 编辑 `/etc/systemd/system/dhcpd6.service` 文件，并追加接口名称，`dhcpd` 应该侦听 `ExecStart` 参数中的命令：

     ```none
     ExecStart=/usr/sbin/dhcpd -f -6 -cf /etc/dhcp/dhcpd6.conf -user dhcpd -group dhcpd --no-pid $DHCPDARGS ens36
     ```

     本例配置了 `dhcpd` 仅侦听 `*ens36*`接口。

  3. 重新加载 `systemd` 管理器配置：

     ```none
     # systemctl daemon-reload
     ```

  4. 重启 `dhcpd6` 服务：

     ```none
     # systemctl restart dhcpd6.service
     ```

## 4. 为直接连接到DHCP服务器的子网设置DHCP服务

如果 DHCP 服务器直接连接到该服务器应响应 DHCP 请求的子网，请使用以下步骤。

根据您要为 IPv4、IPv6 或两个协议提供 DHCP 的信息，请查看以下操作过程：

- [IPv4 网络](#ipv4)
- [IPv6 网络](#ipv6)

**先决条件**

- 已以 `root` 用户身份登录。
- `dhcp-server` 软件包已安装。

**步骤**

- 对于 <a name="ipv4">IPv4</a> 网络：

  1. 编辑 `/etc/dhcp/dhcpd.conf` 文件：

     1. 如果没有单独设置，可以添加 `dhcpd` 用作默认值的全局参数：

        ```none
        option domain-name "example.com";
        default-lease-time 86400;
        ```

        本例将连接 example.com 的默认域名设为 `example.com`，默认的租期时间设为 `86400` 秒（1 天）。

     2. 在新行中添加 `authoritative` 语句：

        ```none
        authoritative;
        ```

        > 没有 `authoritative` 语句，如果客户端询问池外部的地址，则 `dhcpd` 服务不会回答带有 `DHCPNAK` 的 `DHCPREQUEST` 消息。

     3. 对于每个直接连接到服务器接口的 IPv4 子网，都要添加 `subnet` 声明：

        ```none
        subnet 192.168.131.0 netmask 255.255.255.0 {
          range 192.168.131.20 192.168.131.100;
          option domain-name-servers 192.168.131.1;
          option routers 192.168.131.1;
          option broadcast-address 192.168.131.255;
          max-lease-time 172800;
        }
        ```

        这个示例为 192.168.131.0/24 网络添加了 subnet 声明。使用这个配置，DHCP 服务器会为发送这个子网的 DHCP 请求的客户端分配下列设置：

        - `range` 参数定义的范围内空闲的 IPv4 地址
        - 此子网的 DNS 服务器的 IP 为：`192.168.131.1`
        - 此子网的默认网关为：`192.168.131.1`
        - 此子网的广播地址为：`192.168.131.255`
        - 最长租期时间，之后此子网中的客户端释放 IP ，并向服务器发送新请求：`172800` 秒（2 天）

  2. 配置完成之后，可以使用以下命令检查语法问题，截图为没有问题时的输出：

     ```
     # dhcpd -t
     ```

     ![image-20230524143438764](https://s2.loli.net/2023/05/24/dCKnawJUbjRVqW1.png)

  3. （可选）设置在系统引导时自动启动 `dhcpd` ：

     ```none
     # systemctl enable dhcpd
     ```

  4. 启动 `dhcpd` 服务：

     ```none
     # systemctl start dhcpd
     ```

  5. 客户端可以通过命令 `dhclient ens36 -r` 来获取新的 `ip` 地址

- 对于  <a name="ipv6">IPv6</a>网络：

  1. 编辑 `/etc/dhcp/dhcpd6.conf` 文件：

     1. 如果没有单独设置，可以添加 `dhcpd` 用作默认值的全局参数：

        ```none
        option dhcp6.domain-search "example.com";
        default-lease-time 86400;
        ```

        本例将连接 example.com 的默认域名设为 `example.com`，默认的租期时间设为 `86400` 秒（1 天）。

     2. 在新行中添加 `authoritative` 语句：

        ```none
        authoritative;
        ```

        > 没有 `authoritative` 语句，如果客户端询问池外部的地址，则 `dhcpd` 服务不会回答带有 `DHCPNAK` 的 `DHCPREQUEST` 消息。

     3. 对于每个直接连接到服务器接口的 IPv6 子网，请添加 `subnet` 声明：

        ```none
        subnet6 2001:db8:0:1::/64 {
          range6 2001:db8:0:1::20 2001:db8:0:1::100;
          option dhcp6.name-servers 2001:db8:0:1::1;
          max-lease-time 172800;
        }
        ```

        本例为 2001:db8:0:1::/64 网络添加了一个 subnet 声明。使用这个配置，DHCP 服务器会为发送这个子网的 DHCP 请求的客户端分配下列设置：

        - `range6` 参数中定义的范围内可用的 IPv6 地址。

        - 此子网的 DNS 服务器的 IP 地址为 `2001:db8:0:1::1`。

        - 最大租期时间是 `172800` 秒（2 天），之后此子网中的客户端释放 IP ，并向服务器发送新请求。

          请注意： IPv6 需要使用路由器广告信息来识别默认网关。

  2. 配置完成之后，可以使用以下命令检查语法问题，截图为没有问题时的输出：

     ```
     # dhcpd -6 -t
     ```

     ![image-20230524150407465](https://s2.loli.net/2023/05/24/zqpNCmi5Zd3L982.png)

  3. （可选）设置在系统引导时自动启动 `dhcpd6` ：

     ```none
     # systemctl enable dhcpd6
     ```

  4. 启动 `dhcpd6` 服务：

     ```none
     # systemctl start dhcpd6
     ```

  5. 客户端可以通过命令 `dhclient ens36 -r` 来获取新的 `ip` 地址

## 5. (有中继时设置)为没有直接连接到DHCP服务器的子网设置DHCP服务

如果 DHCP 服务器没有直接连接到该服务器应响应 DHCP 请求的子网，请使用以下步骤。如果 DHCP 中继代理将请求转发给 DHCP 服务器，则属于这种情况，因为 DHCP 服务器的接口没有一个直接连接到服务器应服务的子网。与上一步的主要差异在第三、第四步，可以直接查看对应步骤。

根据您要为 IPv4、IPv6 或两个协议提供 DHCP 的信息，请查看以下操作过程：

- IPv4 网络
- IPv6 网络

**先决条件**

- 已以 `root` 用户身份登录。
- `dhcp-server` 软件包已安装。

**步骤**

- 对于 IPv4 网络：

  1. 编辑 `/etc/dhcp/dhcpd.conf` 文件：

     1. 另外，如果没有其他指令包含这些设置，则添加 `dhcpd` 用作默认值的全局参数：

        ```none
        option domain-name "example.com";
        default-lease-time 86400;
        ```

        本例将连接 example.com 的默认域名设为 `example.com`，默认的租期时间设为 `86400` 秒（1 天）。

     2. 在新行中添加 `authoritative` 语句：

        ```none
        authoritative;
        ```

        > 没有 `authoritative` 语句，如果客户端询问池外部的地址，则 `dhcpd` 服务不会回答带有 `DHCPNAK` 的 `DHCPREQUEST` 消息。

     3. **为没有直接连接到服务器接口的 IPv4 子网添加 `shared-network` 声明**，如下所示：

        ```none
        shared-network example {
          option domain-name-servers 192.0.2.1;
          ...
        
          subnet 192.0.2.0 netmask 255.255.255.0 {
            range 192.0.2.20 192.0.2.100;
            option routers 192.0.2.1;
          }
        
          subnet 198.51.100.0 netmask 255.255.255.0 {
            range 198.51.100.20 198.51.100.100;
            option routers 198.51.100.1;
          }
          ...
        }
        ```

        这个示例添加了一个共享网络声明，其中包含对 192.0.2.0/24 和 198.51.100.0/24 的 `subnet` 声明。使用这个配置，DHCP 服务器会为发送来自这些子网之一 DHCP 请求的客户端分配下列设置：

        - 两个子网的客户端的 DNS 服务器的 IP 是： `192.0.2.1`。
        - `range` 参数中定义范围内空闲的 IPv4 地址，具体取决于客户端从哪个子网发送请求。
        - 默认网关是 `192.0.2.1` 或 `198.51.100.1`，具体取决于客户端从哪个子网发送请求。

     4. **为服务器直接连接的子网添加 `subnet` 声明，用于访问上面 `shared-network` 中指定的远程子网**：

        ```none
        subnet 203.0.113.0 netmask 255.255.255.0 {
        }
        ```

        > 如果服务器没有向这个子网提供 DHCP 服务，则 `subnet` 声明必须为空，如示例中所示。没有对直接连接的子网的声明，`dhcpd` 不会启动。

  2. 另外，配置在系统引导时自动启动 `dhcpd` ：

     ```none
     # systemctl enable dhcpd
     ```

  3. 启动 `dhcpd` 服务：

     ```none
     # systemctl start dhcpd
     ```

- 对于 IPv6 网络：

  1. 编辑 `/etc/dhcp/dhcpd6.conf` 文件：

     1. 另外，如果没有其他指令包含这些设置，则添加 `dhcpd` 用作默认值的全局参数：

        ```none
        option dhcp6.domain-search "example.com";
        default-lease-time 86400;
        ```

        本例将连接 example.com 的默认域名设为 `example.com`，默认的租期时间设为 `86400` 秒（1 天）。

     2. 在新行中添加 `authoritative` 语句：

        ```none
        authoritative;
        ```

        > 没有 `authoritative` 语句，如果客户端询问池外部的地址，则 `dhcpd` 服务不会回答带有 `DHCPNAK` 的 `DHCPREQUEST` 消息。

     3. **为没有直接连接到服务器接口的 IPv6 子网添加 `shared-network` 声明**，如下所示：

        ```none
        shared-network example {
          option domain-name-servers 2001:db8:0:1::1:1
          ...
        
          subnet6 2001:db8:0:1::1:0/120 {
            range6 2001:db8:0:1::1:20 2001:db8:0:1::1:100
          }
        
          subnet6 2001:db8:0:1::2:0/120 {
            range6 2001:db8:0:1::2:20 2001:db8:0:1::2:100
          }
          ...
        }
        ```

        本例添加了一个共享网络声明，其中包含对 2001:db8:0:1::1:0/120 和 2001:db8:0:1::2:0/120 的 `subnet6` 声明。使用这个配置，DHCP 服务器会为发送来自这些子网之一 DHCP 请求的客户端分配下列设置：

        - 两个子网的客户端的 DNS 服务器的 IP 是 `2001:db8:0:1::1:1`。

        - `range6` 参数中定义的空闲的 IPv6 地址，具体取决于客户端从哪个子网发送请求。

          请注意： IPv6 需要使用路由器广告信息来识别默认网关。

     4. **为服务器直接连接的子网添加 `subnet6` 声明，用于访问上面 `shared-network` 中指定的远程子网**：

        ```none
        subnet6 2001:db8:0:1::50:0/120 {
        }
        ```

        > 如果服务器没有向这个子网提供 DHCP 服务，则 `subnet6` 声明必须为空，如示例中所示。没有对直接连接的子网的声明，`dhcpd` 不会启动。

  2. 另外，配置在系统引导时自动启动 `dhcpd6` ：

     ```none
     # systemctl enable dhcpd6
     ```

  3. 启动 `dhcpd6` 服务：

     ```none
     # systemctl start dhcpd6
     ```

## 6. (可选)设置DHCP转发代理(中继)

DHCP 中继代理(`dhcrelay`)可以将来自没有 DHCP 服务器的子网的 DHCP 和 BOOTP 请求中继到其他子网上的一个或多个 DHCP 服务器。当 DHCP 客户端请求信息时，DHCP 转发代理会将该请求转发到指定的 DHCP 服务器列表。当 DHCP 服务器返回一个回复时，DHCP 转发代理会将此请求转发给客户端。

根据您要为 IPv4、IPv6 或两个协议设置 DHCP 转发，请查看以下操作过程：

- IPv4 网络
- IPv6 网络

**先决条件**

- 已以 `root` 用户身份登录。

**步骤**

- 对于 IPv4 网络：

  1. 安装 `dhcp-relay` 软件包：

     ```none
     # dnf install dhcp-relay
     ```

  2. 将 `/lib/systemd/system/dhcrelay.service` 文件复制到 `/etc/systemd/system/` 目录中：

     ```none
     # cp /lib/systemd/system/dhcrelay.service /etc/systemd/system/
     ```

     不要编辑 `/usr/lib/systemd/system/dhcrelay.service` 文件。`dhcp-relay` 软件包的未来更新可能会覆盖更改。

  3. 编辑 `/etc/systemd/system/dhcrelay.service` 文件，并追加 `-i interface` 参数以及负责该子网的 DHCPv4 服务器的 IP 地址列表：

     ```none
     ExecStart=/usr/sbin/dhcrelay -d --no-pid -i ens33 192.168.131.1
     ```

     使用这些额外的参数，`dhcrelay` 会侦听 `ens36` 接口上的 DHCPv4 请求，并将它们转发到 IP 为 `192.168.131.1` 的 DHCP 服务器。

  4. 重新加载 `systemd` 管理器配置：

     ```none
     # systemctl daemon-reload
     ```

  5. （可选）配置在系统引导时启动 `dhcrelay` 服务：

     ```none
     # systemctl enable dhcrelay.service
     ```

  6. 启动 `dhcrelay` 服务：

     ```none
     # systemctl start dhcrelay.service
     ```

- 对于 IPv6 网络：

  1. 安装 `dhcp-relay` 软件包：

     ```none
     # dnf install dhcp-relay
     ```

  2. 将 `/lib/systemd/system/dhcrelay.service` 文件复制到 `/etc/systemd/system/` 目录中，并将其命名为 `dhcrelay6.service` ：

     ```none
     # cp /lib/systemd/system/dhcrelay.service /etc/systemd/system/dhcrelay6.service
     ```

     不要编辑 `/usr/lib/systemd/system/dhcrelay.service` 文件。`dhcp-relay` 软件包的未来更新可能会覆盖更改。

  3. 编辑 `/etc/systemd/system/dhcrelay6.service` 文件，并追加 `-l receiving_interface` 和 `-u outgoing_interface` 参数：

     ```none
     ExecStart=/usr/sbin/dhcrelay -d --no-pid -l ens33 -u ens36
     ```

     使用这些额外的参数，`dhcrelay` 会侦听 `ens33` 接口上的 DHCPv6 请求，并将它们转发给连接到 `ens36` 接口的网络。

  4. 重新加载 `systemd` 管理器配置：

     ```none
     # systemctl daemon-reload
     ```

  5. （可选）配置在系统引导时启动 `dhcrelay6` 服务：

     ```none
     # systemctl enable dhcrelay6.service
     ```

  6. 启动 `dhcrelay6` 服务：

     ```none
     # systemctl start dhcrelay6.service
     ```

## 7. (可选)使用DHCP服务为特定主机分配静态IP

使用 `host` 声明，您可以配置 DHCP 服务器，来为主机的媒体访问控制（MAC）地址分配固定 IP 地址。例如：使用这个方法总是为服务器或者网络设备分配相同的 IP 地址。

根据您要为 IPv4、IPv6 或这两个协议配置固定地址，请查看以下操作过程：

- IPv4 网络
- IPv6 网络

**前提条件**

- `dhcpd` 服务已配置且正在运行。
- 已以 `root` 用户身份登录。

**步骤**

- 对于 IPv4 网络：

  1. 编辑 `/etc/dhcp/dhcpd.conf` 文件：

     1. 添加一个 `host` 声明：

        ```none
        host server.example.com {
        	hardware ethernet 52:54:00:72:2f:6e;
        	fixed-address 192.168.131.130;
        }
        ```

        这个示例将 DHCP 服务器配置为始终将 `192.168.131.130` IP 地址分配给具有 `52:54:00:72:2f:6e` MAC 地址的主机。

        `dhcpd` 服务根据 `fixed-address` 参数中指定的 MAC 地址识别系统，而不是根据 `host` 声明中的名称。因此，您可以将此名称设置为不与其它 `host` 声明匹配的任何字符串。要为多个网络配置相同的系统，请使用不同的名称，否则 `dhcpd` 无法启动。

     2. 另外，对针对此主机的特定的 `host` 声明添加其他设置。

  2. 重启 `dhcpd` 服务：

     ```none
     # systemctl start dhcpd
     ```

- 对于 IPv6 网络：

  1. 编辑 `/etc/dhcp/dhcpd6.conf` 文件：

     1. 添加一个 `host` 声明：

        ```none
        host server.example.com {
        	hardware ethernet 52:54:00:72:2f:6e;
        	fixed-address6 2001:db8:0:1::200;
        }
        ```

        这个示例将 DHCP 服务器配置为始终将 `2001:db8:0:1::20` IP 地址分配给 MAC 地址为 `52:54:00:72:2f:6e` 的主机。

        `dhcpd` 服务根据 `fixed-address6` 参数中指定的 MAC 地址识别系统，而不是根据 `host` 声明中的名称。因此，您可以将此名称设置为任何字符串，只要它对其它 `host` 声明是唯一的。要为多个网络配置相同的系统，请使用不同的名称，因为，否则的话 `dhcpd` 无法启动。

     2. 另外，对针对此主机的特定的 `host` 声明添加其他设置。

  2. 重启 `dhcpd6` 服务：

     ```none
     # systemctl start dhcpd6
     ```

## 8. (可选)使用group声明同时将参数应用到多个主机、子网和共享网络

使用 `group` 声明，您可以将同样的参数应用到多个主机、子网和共享网络。

请注意，该流程描述了对主机使用 `group` 声明，但步骤与子网和共享网络相同。

根据您要为 IPv4、IPv6 或两个协议配置组，请查看以下操作过程：

- IPv4 网络
- IPv6 网络

**先决条件**

- `dhcpd` 服务已配置且正在运行。
- 已以 `root` 用户身份登录。

**步骤**

- 对于 IPv4 网络：

  1. 编辑 `/etc/dhcp/dhcpd.conf` 文件：

     1. 添加一个 `group` 声明：

        ```none
        group {
          option domain-name-servers 192.168.131.1;
        
          host server1.example.com {
            hardware ethernet 52:54:00:72:2f:6e;
            fixed-address 192.168.131.130;
          }
        
          host server2.example.com {
            hardware ethernet 52:54:00:1b:f3:cf;
            fixed-address 192.168.131.140;
          }
        }
        ```

        这个 `group` 定义对两个 `host` 条目进行分组。`dhcpd` 服务将 `option domain-name-servers` 参数中设置的值应用到组中的两个主机。**此外，还可以将其他参数设置也加入进去，示例仅供参考**。

  2. 重启 `dhcpd` 服务：

     ```none
     # systemctl start dhcpd
     ```

- 对于 IPv6 网络：

  1. 编辑 `/etc/dhcp/dhcpd6.conf` 文件：

     1. 添加一个 `group` 声明：

        ```none
        group {
          option dhcp6.domain-search "example.com";
        
          host server1.example.com {
            hardware ethernet 52:54:00:72:2f:6e;
            fixed-address 2001:db8:0:1::200;
          }
        
          host server2.example.com {
            hardware ethernet 52:54:00:1b:f3:cf;
            fixed-address 2001:db8:0:1::ba3;
          }
        }
        ```

        这个 `group` 定义对两个 `host` 条目进行分组。`dhcpd` 服务将 `option dhcp6.domain-search` 参数中设置的值应用到组中的两个主机。**此外，还可以将其他参数设置也加入进去，示例仅供参考**。

  2. 重启 `dhcpd6` 服务：

     ```none
     # systemctl start dhcpd6
     ```

## 9. (可选)恢复损坏的租期数据库

如果 DHCP 服务器记录了一个与租期数据库有关的错误，如 `Corrupt lease file - possible data loss!`，则您可以从 `dhcpd` 服务创建的的副本中恢复租期数据库。请注意，这个副本可能没有反映数据库的最新状态。

如果您删除了租期数据库而不是用备份替换它，则丢失了当前分配的租期的所有信息。这样的话，DHCP 服务器可以为之前分配给其它主机但还没有过期的客户端分配租期。这会导致 IP 冲突。

根据您要恢复 DHCPv4、DHCPv6 还是两个数据库，请查看：

- 恢复 DHCPv4 租期数据库
- 恢复 DHCPv6 租期数据库

**先决条件**

- 已以 `root` 用户身份登录。
- 租期数据库被损坏。

**流程**

- 恢复 DHCPv4 租期数据库：

  1. 停止 `dhcpd` 服务：

     ```none
     # systemctl stop dhcpd
     ```

  2. 重命名损坏租期数据库：

     ```none
     # mv /var/lib/dhcpd/dhcpd.leases /var/lib/dhcpd/dhcpd.leases.corrupt
     ```

  3. 恢复 `dhcp` 服务在刷新租期数据库时创建的租期数据库的副本：

     ```none
     # cp -p /var/lib/dhcpd/dhcpd.leases~ /var/lib/dhcpd/dhcpd.leases
     ```

     > 如果您有租期数据库的最新备份，则恢复这个备份。

  4. 启动 `dhcpd` 服务：

     ```none
     # systemctl start dhcpd
     ```

- 恢复 DHCPv6 租期数据库：

  1. 停止 `dhcpd6` 服务：

     ```none
     # systemctl stop dhcpd6
     ```

  2. 重命名损坏租期数据库：

     ```none
     # mv /var/lib/dhcpd/dhcpd6.leases /var/lib/dhcpd/dhcpd6.leases.corrupt
     ```

  3. 恢复 `dhcp` 服务在刷新租期数据库时创建的租期数据库的副本：

     ```none
     # cp -p /var/lib/dhcpd/dhcpd6.leases~ /var/lib/dhcpd/dhcpd6.leases
     ```

     > 如果您有租期数据库的最新备份，则恢复这个备份。

  4. 启动 `dhcpd6` 服务：

     ```none
     # systemctl start dhcpd6
     ```


