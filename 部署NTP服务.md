# OpenClousOS 2301部署NTP服务

## NTP 简介

NTP（Network Time Protocol，网络时间协议）是用来使网络中的各个计算机时间同步的一种协议。它的用途是把计算机的时钟同步到世界协调时UTC，其精度在局域网内可达0.1ms，在互联网上绝大多数的地方其精度可以达到1-50ms。它可以使计算机对其服务器或时钟源（如石英钟，GPS等等）进行时间同步，它可以提供高精准度的时间校正，而且可以使用加密确认的方式来防止病毒的协议攻击。

**但是在官方的仓库中移除了ntp服务软件**， 这里我们用基于centos7的其他系统来演示流程

## 环境：

NTP Server 服务器 IP：`192.168.131.1`

NTP Client 客户端 IP：`192.168.131.13`



## 1、搭建NTP服务器

### **1.1、查看服务器是否安装ntp，系统默认安装ntpdate**

```
[root@localhost ~]# rpm -qa |grep ntp
ntpdate-4.2.6p5-28.el7.centos.x86_64
ntp-4.2.6p5-28.el7.centos.x86_64
```

### **1.2、安装ntp(ntpdate已经系统默认安装过了)**

```
[root@localhost ~]# yum install -y ntp
```

### **1.3、修改ntp配置文件**

```
[root@localhost ~]# vim /etc/ntp.conf

把配置文件下面四行注释掉：
server 0.centos.pool.ntp.org iburst
server 1.centos.pool.ntp.org iburst
server 2.centos.pool.ntp.org iburst
server 3.centos.pool.ntp.org iburst

然后在下面添加这几行：
server 0.cn.pool.ntp.org iburst
server 1.cn.pool.ntp.org iburst
server 2.cn.pool.ntp.org iburst
server 3.cn.pool.ntp.org iburst
```

### **1.4、启动ntp服务，并开机自启动**

```
[root@localhost ~]# systemctl start ntpd
[root@localhost ~]# systemctl enable ntpd
```

### 1.5、查询ntp是否同步

```
[root@localhost ~]# ntpq -p
     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
*119.28.206.193  100.122.36.196   2 u  128  128  377   19.711   -0.468   5.363
```

### 1.6、开启防火墙ntp默认端口udp123

```
[root@localhost ~]# firewall-cmd --permanent --zone=public --add-port=123/udp
success
[root@localhost ~]# firewall-cmd --reload
success
```

 

## 2、NTP客户端配置

安装的NTP跟上面的步骤一样

### 2.1、修改ntp配置文件，将上面的NTP服务器作为客户端同步NTP时间服务器

```
[root@localhost ~]# vim /etc/ntp.conf
#配置允许NTP Server时间服务器主动修改本机的时间
restrict 192.168.0.15 nomodify notrap noquery
#注释掉其他时间服务器
#server 0.centos.pool.ntp.org iburst
#server 1.centos.pool.ntp.org iburst
#server 2.centos.pool.ntp.org iburst
#server 3.centos.pool.ntp.org iburst
#配置时间服务器为本地搭建的NTP Server服务器
server 192.168.131.1
```

### 2.2、与NTP server服务器同步一下时间：

```
[root@localhost ~]# ntpdate -u 192.168.131.1
```

### 2.3、查看ntp同步状态

能看到已经成功同步，要记得开启ntpd这个服务器

```
[root@localhost ~]# ntpq -p
     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
 192.168.131.1  119.28.206.193   3 u    7   64    1    0.217  -288085   0.000
```