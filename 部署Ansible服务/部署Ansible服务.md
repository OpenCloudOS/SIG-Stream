# 部署Ansible服务

以下是在 OpenCloudOS 上部署 Ansible 服务的详细安装和配置过程，包括服务端和客户端的设置。

## 环境设置

| 主机   | IP             |
| ------ | -------------- |
| Server | 192.168.54.149 |
| Client | 192.168.54.148 |

## 服务端配置

### 步骤 1：安装 Ansible

1. 使用 root 用户登录到 OpenCloudOS 服务器。

2. 执行以下命令以安装 Ansible：

   ```
   yum install ansible -y
   ```

### 步骤 2：生成 SSH 密钥对

1. 在服务端上执行以下命令以生成 SSH 密钥对：

   ```
   ssh-keygen
   ```

   按照提示一路回车，不设置密码。

   ![image-20230528165151016](assets/image-20230528165151016.png)

### 步骤 3：将 SSH 公钥分发到客户端

1. 执行以下命令将 SSH 公钥分发到所有客户端主机（替换 `<client_host>` 为客户端主机的 IP 地址或主机名）：

   ```
   ssh-copy-id <client_host>
   ```

   你可能需要输入客户端主机的用户名和密码，此处将SSH公钥分发给了客户机的root用户。

   ![image-20230528165335116](assets/image-20230528165335116.png)

### 步骤 4：配置 Ansible 主机清单

1. 使用文本编辑器打开 Ansible 主机清单文件 `/etc/ansible/hosts`：

   ```
   vi /etc/ansible/hosts
   ```

2. 在文件中添加客户端主机的信息。每行一个主机，格式为 `<hostname_or_ip> ansible_user=<remote_user>`。例如：

   ```
   192.168.54.148 ansible_user=root
   ```

   替换 `<hostname_or_ip>` 为客户端主机的 IP 地址或主机名，`<remote_user>` 为在客户端上用于连接的用户名。

### 步骤 5：测试 Ansible 连接

1. 执行以下命令测试 Ansible 是否能够成功连接到客户端主机：

   ```
   ansible all -m ping
   ```

   如果一切设置正确，你应该能够看到类似以下输出：

   ![image-20230528170045475](assets/image-20230528170045475.png)

   这表示 Ansible 已经成功连接到客户端主机。

### 步骤 6：编写 Ansible Playbook

1. 使用文本编辑器创建一个 Ansible Playbook 文件，例如 `demo.yml`：

   ```
   vi demo.yml
   ```

2. 在 Playbook 文件中编写你要执行的任务，例如：

   ```yaml
   ---
   - name: Example Playbook
     hosts: all
     tasks:
       - name: Print message
         debug:
           msg: "Executing task on {{ inventory_hostname }}"
   
       - name: Create file and write content
         copy:
           content: "success"
           dest: ansible_test.txt
   
       - name: Print success message
         debug:
           msg: "Task completed successfully on {{ inventory_hostname }}"
   ```

   这个 Playbook 将在所有客户端主机上创建一个名为 "ansible_test.txt" 的文件，并将内容设置为 "success\n"。

### 步骤 7：运行 Ansible Playbook

1. 执行以下命令来运行 Ansible Playbook：

   ```
   ansible-playbook demo.yml
   ```

   Ansible 将会在所有客户端主机上执行 Playbook 中定义的任务，如下图：

   ![image-20230528172321634](assets/image-20230528172321634.png)

   客户端查看`ansible_test.txt`文件内容：

   ![image-20230528172406061](assets/image-20230528172406061.png)

## 客户端配置

客户端不需要进行额外的配置。只需确保客户端主机可以通过 SSH 连接，并且服务端上的 SSH 公钥已经分发到客户端上。

通过以上步骤，你应该已经成功在 OpenCloudOS 服务器上部署了 Ansible 服务，并能够远程批量控制客户端主机。根据你的需求，你可以编写不同的 Playbooks 来执行各种任务。请记得根据实际情况进行适当的调整和修改。
