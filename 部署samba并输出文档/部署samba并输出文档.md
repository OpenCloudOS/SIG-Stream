# 部署samba并输出文档
## 部署环境

在 OpenCloudOS Stream 23版本上安装部署 samba

https://mirrors.opencloudos.org/opencloudos-stream/releases/23/images/x86_64/

## 部署记录

1、配置虚拟机网络，虚拟机接入互联网（并设置静态IP）

设置静态IP

```bash
[root@localhost ~]# vi /etc/sysconfig/network-scripts/ifcfg-ens33
```

![](.\Image\01.png)

重启network服务

```bash
[root@localhost ~]# systemctl restart network
```
2、安装基本工具

```bash
[root@localhost ~]# yum install -y vim samba samba-client
```
3、关闭防火墙
关闭防火墙，并设置开机禁止启动
```bash
关闭防火墙：systemctl stop firewalld
查看状态：systemctl status firewalld
开机禁用：systemctl disable firewalld
```
4、禁用 selinux
临时（当前连接下有效）
```bash
[root@localhost ~]# setenforce 0
```
5、查看配置文件、备份配置文件
```bash
[root@localhost ~]# ll /etc/samba/
[root@localhost ~]# cp /etc/samba/smb.conf /etc/samba/smb.conf-bak
```

![](.\Image\02.png)

6、添加系统用户，对添加的用户设置密码
```bash
[root@localhost ~]# useradd smbtest
[root@localhost ~]# passwd smbtest
```

![](.\Image\03.jpg)

7、设置 smb 用户（Samba的管理账号）的密码
```bash
[root@localhost ~]# smbpasswd -a smbtest
```

![](.\Image\04.jpg)
8、创建共享目录

```bash
[root@localhost ~]# mkdir /opt/smbstore
```
9、根据需要赋予共享目录权限

```bash
[root@localhost ~]# chmod 755 /opt/smbstore/
或
[root@localhost ~]# chmod 777 /opt/smbstore/
或
[root@localhost ~]# chown smbtest:root /opt/smbstore/
或
[root@localhost ~]# chown smbtest:smbtest /opt/smbstore/
```
10、修改配置文件
```bash
[root@localhost ~]# vim /etc/samba/smb.conf
```

![](.\Image\05.jpg)

11、启动服务，并设置开机启动、关闭、重启
```bash
#启动
[root@localhost ~]# systemctl start smb
#开机启动 
[root@localhost ~]# systemctl enable smb
#关闭  
[root@localhost ~]# systemctl stop smb
#重启  
[root@localhost ~]# systemctl restart smb
```

## 测试记录
1、计算机地址查看

![](.\Image\06.png)

![](.\Image\07.jpg)

![](.\Image\08.jpg)

2、测试
```bash
[root@localhost ~]# touch /opt/smbstore/test.txt
```

![](.\Image\09.jpg)