# Fortran编译器部署并输出文档

在OpenCloudOS上部署Fortran编译器可以使用GNU Fortran编译器（gfortran）。以下是详细的安装、部署和配置指导文档：

## 1. 安装gfortran编译器

打开终端，运行以下命令：

```
sudo apt-get update
sudo yum install gfortran
```

**验证安装：**在终端中运行以下命令来检查是否成功安装：

```
gfortran --version
```

如果安装成功，将显示gfortran的版本信息，如下图：

![image-20230527214113374](assets/image-20230527214113374.png)

## 编写Fortran源代码示例

 创建一个新的文本文件，例如`hello.f90`，并使用文本编辑器打开该文件。在文件中输入以下简单的Fortran代码示例：

```fortran
program hello
  print *, "Hello, Fortran!"
end program hello
```

编译Fortran源代码： 在终端中，使用以下命令编译Fortran源代码：

```
gfortran hello.f90 -o hello
```

此命令将会将源代码编译成可执行文件`hello`，如下图：

![image-20230527214347566](assets/image-20230527214347566.png)

运行可执行文件： 在终端中，运行以下命令来执行可执行文件：

```
./hello
```

输出应该是`Hello, Fortran!`，如下：

![image-20230527214436832](assets/image-20230527214436832.png)

这样，你就成功地在OpenCloudOS上部署了Fortran编译器，并进行了简单的编译和运行。

