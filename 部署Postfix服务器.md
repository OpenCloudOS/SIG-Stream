## 准备工作
### 1. 关闭防火墙并设置开机不自启
```
systemctl stop firewalld
systemctl disable firewalld
```
![输入图片说明](Postfix%20Picture/firewalld.png)   
### 2. 禁用SELinux
修改`/etc/sysconfig/selinux`路径下的配置文件，修改`SELINUX`为`disable`，保存并退出。
```
vim /etc/sysconfig/selinux
```
![输入图片说明](Postfix%20Picture/selinux1.png)  
![输入图片说明](Postfix%20Picture/selinux2.png)  
`reboot`重启系统，使配置生效。`getenforce`查看当前SELinux的应用模式。  
![输入图片说明](Postfix%20Picture/selinux3.png)  
## 部署、配置Postfix
### 1. 检查是否安装postfix
```
rpm -q postfix
```
![输入图片说明](Postfix%20Picture/postfix1.png)  
若未安装，则执行`yum install postfix`进行安装。  
### 2. 设置系统完整主机名称
使用`hostname`或`hostnamectl`命令可查出系统的主机名称，完整的主机名称由主机本身的名称和网域名称两部分构成，例如`mail.good.com`中，`mail`为主机本身名称，`good.com`为网域名称。  

使用`hostnamectl set-hostname 主机名称`命令可对主机名称进行设置。这里我们将主机名称设置为`mail.good.com`。  
```
hostnamectl set-hostname mail.good.com
hostnamectl
```
![输入图片说明](Postfix%20Picture/postfix2.png)  
修改完成后，`su root`再次切换到root用户，所显示的主机名称则会更新。   
### 3. 修改postfix配置文件main.cf
Postfix的配置文件全部集中在`/etc/postfix`目录下，其中最重要的两个文件是`master.cf`与`main.cf`，当改变配置文件的内容后，都必须执行`systemctl reload postfix`重新加载Postfix使改变生效，但如果修改`inet_interfaces`参数，则需要重新启动(restart)Postfix，而不可只重新加载(reload)。因此，建议修改配置文件后，执行`systemctl restart postfix`重新启动Postfix。  

现在，进入`/etc/postfix`目录下修改`main.cf`。  
```
cd /etc/postfix
ll
vim main.cf
```
![输入图片说明](Postfix%20Picture/postfix3.png)   
![输入图片说明](Postfix%20Picture/postfix4.png)   
`main.cf`中需要修改的内容如下：
```
//修改98行，设置邮件系统的主机名称
myhostname = main.good.com
//修改106行，设置邮件系统的网域名称
mydomain = good.com
//将122行取消注释
myorigin = $mydomain
//将136行取消注释，并注释掉139行
inet_interfaces = all
#inet_interfaces = localhost
//将188行取消注释，并注释掉187行
#mydestination = $myhostname, localhost.$mydomain, localhost
mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain
//修改290行，列出可通过本邮件系统寄出邮件的IP地址
mynetworks = 192.168.180.134/24, 127.0.0.0/8
//修改447行，启用maildir格式的邮件存放目录
home_mailbox = Maildir/
```
修改完成后，保存并退出。使用`postfix check`命令检查是否有语法错误。执行`systemctl restart postfix`重新启动Postfix。至此，Postfix配置结束。   
![输入图片说明](Postfix%20Picture/postfix5.png)   
## 邮件收发示例
### 1. 创建测试用户
执行以下命令，创建一个测试用户`user1`用来在本例中接收邮件。   
```
useradd user1
passwd user1
```
![输入图片说明](Postfix%20Picture/test1.png)   
![输入图片说明](Postfix%20Picture/test2.png)   
### 2. 发送邮件
本例中，由`root`向`user1`发送一封邮件正文为`This is a test.`的邮件。

执行`telnet`命令连接服务器端的25端口。若未安装telnet，则先执行`yum install telnet`安装telnet。  
```
telnet mail.good.com 25
helo mail.good.com
mail from:root@good.com
rcpt to:user1@good.com
data      //开始输入邮件正文
This is a test.
.         //输入"."结束邮件正文
quit
```
![输入图片说明](Postfix%20Picture/test3.png)   
### 3. 查收邮件
进入`user1`的maildir邮箱目录，在new子目录下的邮件文件是已送达但尚未被用户阅读的邮件。使用`cat`命令可查看目录下的邮件文件。   
```
su user1
cd /home/user1/Maildir
cd new
ls
```
![输入图片说明](Postfix%20Picture/test4.png)   