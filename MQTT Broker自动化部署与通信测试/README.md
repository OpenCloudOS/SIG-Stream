# 部署MQTT broker服务，并完成通信测试

## 部署环境

OpenCloudOS Stream 2301  Docker版本

https://mirrors.opencloudos.tech/opencloudos-stream/releases/2301/images/OpenCloudOS-Stream-2301-x86_64.tar

## MQTT RPM打包

> 我们选择eclipse mosquitto为MQTT broker对象，原因是该mosquitto历史最为悠久、使用广泛。
> 在本教程中，我们选择目前最新发行版本mosquitto 2.0.15版本进行打包部署与测试。

首先预先安装相关依赖：

```bash
dnf install -y rpmdevtools openssl-devel gcc rpm-build pcre-devel 
```

下面使用rpm打包工具开始打包：

```bash
rpmdev-setuptree
```

执行指令后可以看到当前位置生成了一个rpmbuild文件夹

![image-20230528150849646](README.assets/image-20230528150849646-16852577305651.png)

1. BUILD目录（宏：%_builddir）：用于存放解压后的源码，并且源码的编译在该目录完成。

2. RPMS目录（宏：%_rpmdir）：存放生成的二进制rpm包。

3. SOURCES目录（宏：%_sourcedir）：存放源代码压缩包和补丁。

4. SPECS目录（宏：%_specdir）：存放.spec文件。

5. SRPMS目录（宏：%_srcrpmdir）：存放生成的源码rpm包。

我们需要将mosquitto代码放入SOURCES目录下：

```bash
wget https://github.com/eclipse/mosquitto/archive/refs/tags/v2.0.15.zip
unzip v2.0.15.zip
cd mosquitto-2.0.15
rm -rf ./.git*
cd ..
tar -zcf mosquitto-2.0.15.tar.gz ./mosquitto-2.0.15/
mv mosquitto-2.0.15.tar.gz SOURCES/
```

我们要实现自动化打包，需要编写spec文件，故首先在当前目录下创建一个.spec文件模板：

```
rpmdev-newspec mosquitto-2.0.15
```

这会产生一个模板文件，我们对其进行编写：

```
Summary: Mosquitto Broker build
Name: mosquitto
Version: 2.0.15
Release: 1
License: GPL
Source: https://github.com/eclipse/mosquitto/archive/refs/tags/v2.0.15.zip
Source1: init.tar.gz

BuildRequires: openssl-devel cmake c-ares-devel libuuid-devel

%description
Mosquitto MQTT Broker v2.0.15

%prep
rm -rf $RPM_BUILD_DIR/mosquitto-2.0.15
zcat $RPM_SOURCE_DIR/mosquitto-2.0.15.tar.gz | tar -xvf -

%build
cd mosquitto-2.0.15
make WITH_CJSON=no WITH_DOCS=no RPM_OPT_FLAGS="$RPM_OPT_FLAGS"

%install
make WITH_CJSON=no WITH_DOCS=no install DESTDIR="$RPM_BUILD_ROOT" prefix=/usr

%clean
[ $RPM_BUILD_ROOT != "/" ] && rm -rf $RPM_BUILD_ROOT

%files
%defattr (-,root,root,0755) 
%attr(644, root, root)	
%attr(755, root, root) /usr/bin/mosquitto_pub 
%attr(755, root, root) /usr/bin/mosquitto_sub 
%attr(755, root, root) /usr/sbin/mosquitto 
%attr(755, root, root) /usr/bin/mosquitto_passwd 
%attr(755, root, root) /usr/bin/mosquitto_rr
/usr/lib/libmosquitto.so.1
/usr/lib/libmosquitto.so
/usr/include/mosquitto.h
/usr/lib/libmosquittopp.so.1
/usr/lib/libmosquittopp.so
/usr/include/mosquittopp.h
/usr/include/mosquitto_broker.h
/usr/include/mqtt_protocol.h
/usr/lib/pkgconfig/libmosquitto.pc
/usr/lib/pkgconfig/libmosquittopp.pc
/usr/include/mosquitto_plugin.h
/etc/mosquitto/aclfile.example
/etc/mosquitto/mosquitto.conf.example
/etc/mosquitto/pskfile.example
/etc/mosquitto/pwfile.example

%post -p /sbin/ldconfig
%preun
/sbin/service mosquitto stop
/sbin/chkconfig --del mosquitto

%postun -p /sbin/ldconfig
```

完成后我们开始制作rpm包：

```bash
cd SPECS
rpmbuild -bb mosquitto-2.0.15.spec
```

执行成功将如下输出：

![image-20230528174241036](README.assets/image-20230528174241036-168526696194211.png)

同时在路径`./rpmbuild/RPMS/x86_64`中生成对应的rpm包

![image-20230528174352228](README.assets/image-20230528174352228-168526703308413.png)

至此RPM打包结束

## MQTT部署与测试

为了验证rpm包是否正确构建，我们重新启动一个相同docker容器，在其中进行测试。

手动将mosquitto-2.0.15-1.x86_64.rpm移动进去，然后安装：

```bash
rpm -ivh mosquitto-2.0.15-1.x86_64.rpm
```

如下所示，mosquitto已经被正确安装且运行正常。

![image-20230528170425018](README.assets/image-20230528170425018-16852646657423.png)

进一步的，我们利用mosquitto提供的sub和pub两个客户端进行通信测试：

0. 开启mosquitto：mosquitto

   ![image-20230528172812797](README.assets/image-20230528172812797-16852660935885.png)

1. 开启一个终端用于订阅消息：mosquitto_sub -d -t 'test'

   ![image-20230528172826079](README.assets/image-20230528172826079-16852661067497.png)

2. 开启另一个终端用于发布消息：mosquitto_pub -d -t 'test' -m 'hello from opencloudos' 

​		![image-20230528172833728](README.assets/image-20230528172833728-16852661144759.png)

可以看到我们发送了一条消息“hello from opencloudos”，在上图2中也可以对应看到这则消息。

至此通信测试结束，可以验证MQTT broker（包括客户端组件）均部署成功，可以正常运行。

## 附件

* **mosquitto_rpm.tar.gz**

  > 打包过程生成的rpm文件，其中包括rpm打包脚本和mosquitto源码

* **mosquitto-2.0.15-1.x86_64.rpm**
