测试环境：
	server (服务器)：虚拟机 OpenCloudOS Stream 23 aarch 64 DVD  仅主机模式连接 （作服务器）
	client  (客户端)：虚拟机 Ubuntu 1604                                             仅主机模式连接 （作客户端）

测试步骤：
	1、在server端下载DHCP服务器，并为虚拟网络设置参数。
	2、使用client端查看DHCP的运行情况。

## 一、server端的操作

### 1、使用**root用户**登录，首先以 **NAT 模式**打开server端虚拟机，下载DHCP服务器。

	yum  install dhcp* -y

![a86f52e854f8434cf3db1d158b425e7.png](https://s2.loli.net/2023/05/24/RHPzogGawkIsZOV.png)

### 2、关闭server端虚拟机，将模式切换为 **仅主机连接**。

![3d8f59ed69849c9f18ee5ed92875df6 1.png](https://s2.loli.net/2023/05/24/RpUG6T9etnXQb3E.png)

### 3、打开虚拟网络编辑器，点击更改设置。

![85e6221818df0b46b7b9fd84b134a3b.png](https://s2.loli.net/2023/05/25/sEK4b6I1Q2Lg8UH.png)

### 4、将使用本地DHCP服务选项**去掉**。

![3fc121c0ae63c7650eb7df4a2014402 1.png](https://s2.loli.net/2023/05/24/jUIa1yhRnHbB8xt.png)

### 5、再次以root用户登入server端虚拟机，使用 **ifconfig** 命令查看IP，可以看到没有分配IP。

![5ca5ed1fef62dcde8f73ec53ab311dd.png](https://s2.loli.net/2023/05/24/zIiTgty8N9esSQa.png)

### 6、此时需要手动为server端虚拟机**手动设置静态IP**，打开配置文件文件，不同设备的网卡名称可能不同，可以通过**ifconfig命令**查看，此处为**ens32**。
（修改BOOTPROTO为static 静态模式，添加IP、掩码、网关，IP需要和仅主机模式在同一个网段）

	vi /etc/sysconfig/network-scripts/ifcfg-ens33

![c4e2306dd1e4bba642a7cf2637babd1.png](https://s2.loli.net/2023/05/24/EbKr3ZUVYiCmSQd.png)

### 7、再次查看server端服务器的IP信息，可以看到IP与之前设置相同。

![1791dcb59c331442bc38ebfbd0fdd83.png](https://s2.loli.net/2023/05/24/jdstLPRZ3frKWw7.png)

### 8、对 dhcpd.conf 文件内容进行编辑，首先将配置模板拷贝到 dhcpd.conf 文件中。

	cp /usr/share/doc/dhcp-4.2.5/dhcpd.conf.example /etc/dhcp/dhcpd.conf

### 9、打开 dhcpd.conf 文件，配置信息，设置**动态IP的范围**。

![8facad190652aa227c43276c66c55c2.png](https://s2.loli.net/2023/05/24/zX5Yb9jaJZVrq1u.png)

修改为下图内容：

![79306bcc4b5f15a4fb4a9e3eeff2165.png](https://s2.loli.net/2023/05/24/SMsO8QutwNVUe3x.png)

### 10、在server端虚拟机开启dhcpd服务，并查看服务状态。

	systemctl start dhcpd
	systemctl status dhcpd

![025a44a3841bde3888efaac18b347a0.png](https://s2.loli.net/2023/05/24/jRkiqTAma6seY4X.png)

## 二、client端的操作

### 1、保持**server端虚拟机**开机。
### 2、以仅主机连接的模式打开**client端虚拟机**，输入ifconfig指令查看IP信息。

![15a1393ef670e1cc5743c28ca095711.png](https://s2.loli.net/2023/05/24/udc2v6JiGF9pDtC.png)

可以看到 此时**client端的IP地址在server端划分的IP 192.168.156.60~192.168.156.80范围内