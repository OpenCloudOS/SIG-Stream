# 部署多路径服务

## 1.软件安装

* 使用以下命令检查系统是否已安装 `device-mapper-multipath` 软件包

  ```
  rpm -qa | grep device-mapper-multipath
  ```

* 如果该软件包没有安装，则需要安装它。可以使用以下命令安装它

  ```
  sudo yum install device-mapper-multipath -y
  ```

* 安装后，请确保加载 dm-multipath 内核模块。可以使用以下命令加载内核模块（若无输出则成功）

  ```
  modprobe dm-multipath
  ```

* 可以使用 fdisk 命令查看磁盘设备信息

  ```
  sudo fdisk -l
  ```

## 2.配置多路径参数

* 打开 `/etc/multipath.conf` 文件进行编辑

  ```
  sudo vi /etc/multipath.conf
  ```

  添加以下内容：

  ```
  defaults {
      user_friendly_names yes
      find_multipaths yes
      path_grouping_policy multibus
      path_checker readsector0
      prio const
      failback immediate
      rr_min_io 100
      no_path_retry 18
  }
  ```

## 3.启动服务

* 设置在系统启动时自动启用 `multipathd` 服务，以便系统能够自动管理多路径设备

  ```
  sudo systemctl enable multipathd.service
  ```

* 启动 multipathd 服务

  ```
  sudo systemctl start multipathd.service
  ```

* 查看 multipathd 服务状态

  ```
  sudo systemctl status multipathd.service
  ```

## 4.测试多路径设置

* 使用以下命令检查设备是否已添加到多路径设备列表中

  ```
  sudo multipath -ll
  ```

* 显示的输出应包括相关设备的多个路径、文件大小、vendor 和 model 信息等

* 如果以上指令没有输出，请检查系统中是否正确配置了多路径存储设备