---

---

# 部署Apache服务器

## 问题描述：

在stream 2301版本上部署Linux+Apache+Mysql+Python 这样的LAMP服务

## 环境

虚拟机：VMware Workstation 16

系统：OpenCloudOS stream 2301

内核版本: Kernel 5.4.119-20.0009.20 on x86_64

## 流程

Note: 为了保证流程的安全性，我们全程使用普通用户进行部署

首先，我们确保yum已经更新到最新

```
sudo yum update -y
```

![image-20230525134338303](img/image-20230525134338303.png)

### 1.部署Apache

安装Apache

```
sudo yum install httpd -y
```

![image-20230525134538487](img/image-20230525134538487.png)

启动Apache，并检查运行状态

```
sudo systemctl start httpd
```

![image-20230525134611706](img/image-20230525134611706.png)

如果需要Apache随系统启动，设置如下

```
sudo systemctl enable httpd
```

### 2.部署Mysql

由于yum默认仓库中没有Mysql，所以我们需要首先下载安装 MySQL YUM 仓库，首先我们需要安装wget

```
sudo yum install wget -y
```

![image-20230525141714337](img/image-20230525141714337.png)

![image-20230525141733236](img/image-20230525141733236.png)

然后下载安装 MySQL YUM 仓库

```
wget http://repo.mysql.com/mysql57-community-release-el7-10.noarch.rpm
sudo rpm -Uvh mysql57-community-release-el7-10.noarch.rpm
```

![image-20230525140514265](img/image-20230525140514265.png)

随后，使用yum 安装 Mysql

```
sudo yum install -y mysql-community-server
```

![image-20230525141803224](img\image-20230525141803224.png)

报错，我们选择暂时跳过GPG检查，重新安装成功

```
sudo yum install -y mysql-community-server --nogpgcheck
```

![image-20230525141913078](img\image-20230525141913078.png)

启动Mysql

```
systemctl start mysqld.service
```

![image-20230525141945657](img\image-20230525141945657.png)

检查启动是否成功

```
systemctl status mysqld.service
```

![image-20230525142013378](img\image-20230525142013378.png)

### 3.安装python

OpenOS已经预装Python

![image-20230525142102726](img\image-20230525142102726.png)

至此，LAMP服务部署完成。