# 部署xmake

## 环境

OpenCloudOS Stream 2301  Docker版本

https://mirrors.opencloudos.tech/opencloudos-stream/releases/2301/images/OpenCloudOS-Stream-2301-x86_64.tar

## 部署过程

预先安装相关软件依赖

```bash
dnf -y install glibc-headers gcc-c++  kernel-headers gcc
```

开始构建和安装xmake

```bash
curl -fsSL https://xmake.io/shget.text | bash
```

![image-20230527213954801](README.assets/image-20230527213954801-16851947955871.png)

执行：

```
xmake --version
```

**这里会提示不支持用root执行，但由于我们测试，所以暂时允许并直接以root身份执行：**

```
export XMAKE_ROOT=y
xmake --version
```

至此构建成功

![image-20230527215105753](README.assets/image-20230527215105753-16851954666353.png)

## xmake使用测试

现在我们创建一个基于xmake的demo工程进行构建。

```bash
 xmake create test
```

![image-20230527215700178](README.assets/image-20230527215700178-16851958208915.png)

这里默认生成hello world程序

![image-20230527215725341](README.assets/image-20230527215725341-16851958460297.png)

进入到test目录下执行编译：

```bash
cd test
xmake 
```

完成编译并输出对应的内容

![image-20230527215839542](README.assets/image-20230527215839542-16851959203969.png)

至此xmake构建完毕，并且xmake功能正常。

