# 部署Django

## 1. 安装Python

1. 首先使用以下命令更新系统软件包列表:

   ````
   sudo yum update
   
2. 安装Python，使用以下命令：

   ````
   sudo yum install python3
   ````
   ![image-20230526145310044](assets/image-20230526145310044.png)
   
   OpenCloudOS已经预装了python3，安装完成后，可以使用以下命令检查Python是否成功安装：
   
   ```
   python3 --version
   ```
   
   输出的结果应该是Python的版本号，如下图：
   
   ![image-20230526145416056](assets/image-20230526145416056.png)

## 安装Django

1. 安装pip，使用以下命令：

   ````shell
   sudo yum install python3-pip
   ````
   
   ![image-20230526145356175](assets/image-20230526145356175.png)
   
2. 使用以下命令安装Django：

   ````
   pip3 install django
   ````
   ![image-20230526145546033](assets/image-20230526145546033.png)
   
   安装完成后，可以使用以下命令检查Django是否成功安装：
   
   ```
   django-admin --version
   ```
   
   输出的结果应该是Django的版本号，如下图：
   
   ![image-20230526145610163](assets/image-20230526145610163.png)

## 配置Django

1. 创建Django项目，使用以下命令：

   ````
   django-admin startproject myproject
   ````

   这将在当前目录下创建一个名为myproject的Django项目。

   ![image-20230526145711185](assets/image-20230526145711185.png)

2. 创建Django应用，使用以下命令：

   ````
   cd myproject
   python3 manage.py startapp myapp
   ````

   这将在myproject目录下创建一个名为myapp的Django应用。

   ![image-20230526145759143](assets/image-20230526145759143.png)

3. 更改Django项目的配置文件，使用以下命令：

   ````
   vi myproject/settings.py
   ````

   打开文件后，进行以下配置：

   - 将`ALLOWED_HOSTS`设置为`['*']`，以允许任何主机访问项目。
   - 
   - 将`TIME_ZONE`设置为您所在的时区。
   - 将`DATABASES`设置为您的数据库配置。

   保存并关闭文件。

4. 在Django应用中创建视图，使用以下命令：

   ````
   vi myapp/views.py
   ````
   打开文件后，输入以下代码：

   ```python
   from django.http import HttpResponse
   
   def index(request):
       return HttpResponse("Hello, world!\n")
   ```

   保存并关闭文件。

5. 在Django应用中创建URL配置，使用以下命令：

   ````
   vi myapp/urls.py
   ````
   打开文件后，输入以下代码：

   ```python
   from django.urls import path
   
   from . import views
   
   urlpatterns = [
       path('', views.index,name='index'),
   ]
   ```

   保存并关闭文件。

6. 将Django应用的URL配置包含在Django项目的URL配置中，使用以下命令：

   ````
   vi myproject/urls.py
   ````
   打开文件后，输入以下代码：
   
   ```python
   from django.contrib import admin
   from django.urls import include, path
      
   urlpatterns = [
       path('', include('myapp.urls')),
       path('admin/', admin.site.urls),
   ]
   ```
   
   保存并关闭文件。

## 测试Django

1. 在Django项目目录下，运行以下命令启动Django开发服务器：

   ````
   python3 manage.py runserver 0.0.0.0:8000
   ````

   这将启动Django的开发服务器，并使其监听所有可用的网络接口，端口为8000。

   ![image-20230526150508519](assets/image-20230526150508519.png)

2. 使用curl访问Django的demo页面，在终端中输入以下命令

   ````
   curl http://服务器IP地址:8000/
   ````

   如果一切正常，您应该能够看到“Hello, world!”的响应。

   ![image-20230526152241395](assets/image-20230526152241395.png)

   同时，在后台能看到Django接收到了一个请求，状态码为200：

   ![image-20230526152557958](assets/image-20230526152557958.png)如何

3. 外网访问，需配置防火墙开放对应的端口，命令如下：

   ```shell
   sudo firewall-cmd --zone=public --add-port=8000/tcp --permanent
   sudo firewall-cmd --reload
   ```

   ![image-20230526182320081](assets/image-20230526182320081.png)

   此时通过外网主机的浏览器即可访问demo页面：

   ![image-20230526182501096](assets/image-20230526182501096.png)

4. 关闭Django开发服务器，使用`CTRL-C`终止运行。

以上是在OpenCLoudOS上使用yum部署Python的Django Web框架的详细部署文档。需要注意的是，具体的配置和测试步骤可能会因为不同的系统环境、Django版本或者其他因素而有所不同。在实际部署过程中，可以根据具体情况进行调整和修改。

